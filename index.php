<?php
// error_reporting(E_ALL);
global $settings, $dispatcher, $member;

NeuronCMS::DispatchName('action','store');
NeuronCMS::DispatchName('shortname','category');
NeuronCMS::DispatchName('control','product');
NeuronCMS::DispatchName(3,'product_action');
NeuronCMS::DispatchName(4,'category_action');

require_once('functions.inc.php');
require_once('Store.class.php');
require_once('StoreProduct.class.php');
require_once(NEURON_ROOT."includes/billing/ShoppingProcess.class.php");

//SECURE is set in config.inc.php on local site folder, use for site that defaults to https
if (!defined('SECURE')) {
include_once('secure_switch.php');
}
// unset($_SESSION[$dispatcher['store']]);

$STORE_ID = 1; //set to 1 for now - can be > 1 for multiple stores in future.
$store = new Store($STORE_ID);

if($store->configured) {
#	echo 9;
	if($store->enabled || $dispatcher['category'] == 'configure') {
#		echo 10;
		if(NeuronCMS::DispatcherHasValue('category')) {
			#echo 11;
       
			if($dispatcher['category'] == 'addproduct') {

				if(!in_array($member->admin, array('1','3'))) {

					$message = urlencode("You do not have sufficient privileges for this operation.");
					Header("Location:/static/login?message=".$message);
					die();
				} else {

					$store->addProduct();
				}
			} 
            else if($dispatcher['category'] == 'deleteproducts') {
				if(!in_array($member->admin, array('1','3'))) {

					$message = urlencode("You do not have sufficient privileges for this operation.");
					Header("Location:/static/login?message=".$message);
					die();
				} else {

					$store->deleteProducts($_POST['delete']);
				}
			}
			else if ($dispatcher['category'] == 'validate_secret') {
				$json_str = file_get_contents('php://input');
				$json_obj = json_decode($json_str);
				if (!is_null($json_obj)) {
					if (!empty($json_obj->client_secret)) {
						$shop->retrievePaymentIntent();
						echo json_encode(array('client_secret' => $shop->client_secret)); exit;
					}
				}
				
			}
            else if($dispatcher['category'] == 'checkout') {

				$shop = new ShoppingProcess($store);
                
				switch($dispatcher['product']) {
					case 'billing':
						$shop->Billing();

						//Facebook pixel
						if ($settings->facebook_pixel_id) {
							$shop->fbq = "fbq('track', 'InitiateCheckout', {value: ".$_SESSION['idev_cart']['amount'].",currency: '".$shop->store->currency."',});";
							//$tpl2->assign('fb_initiate_co', $fbq);
						}

						$otherstatetax = (float) $store->otherstatetax;
						$includeStateTaxCountry = $_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" ? 1 : 0;
						$shippingrate_id =  $_SESSION[$dispatcher['store']]['cart']['shippingrateid'];
						$matchBillingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == $store->country || $_SESSION[$dispatcher['store']]['shipping_country'] == $store->country) ? 1 : 0;
						$matchCountryAndState = ($_SESSION[$dispatcher['store']]['billing_country'] == $store->country && $_SESSION[$dispatcher['store']]['billing_state'] == $store->state) || ($_SESSION[$dispatcher['store']]['shipping_country'] == $store->country && $_SESSION[$dispatcher['store']]['shipping_state'] == $store->state) ? 1 : 0;
						$billingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['billing_country'] == "AU" || $_SESSION[$dispatcher['store']]['billing_country'] == "CA"
						|| $_SESSION[$dispatcher['store']]['shipping_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "AU" || $_SESSION[$dispatcher['store']]['shipping_country'] == "CA") ? 1 : 0;
				
						$store->isOtherCountryStateTax = (($includeStateTaxCountry && $otherstatetax) && $matchBillingCountry && !$matchCountryAndState ) ? 1 : 0;
						// ORIG $carttotals = $store->calculateCartTotals($_SESSION[$dispatcher['store']]['cart']['shippingrateid'], 0);
						if ($billingCountry) {
							if (($matchCountryAndState) || ($otherstatetax > 0 && $includeStateTaxCountry) ) {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 1);
							} else {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 0);
							}
						} else {
							if ($matchBillingCountry) {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 1);
							} else {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 0);
							}
						}
						$_SESSION['store']['total'] = $carttotals['total'];
						
						$tpl2->assign_by_ref('carttotals', $carttotals);
						
						$tpl2->assign_by_ref('content',$shop);
						$tpl2->assign('dispatcher',$dispatcher);
						$tpl2->display("co_details.tpl");
						break;
					case 'updatebilling-order-summary':
						
						parse_str($_POST['value'], $billing_shipping_data);
						$otherstatetax = (float) $store->otherstatetax;
						$includeStateTaxCountry = $billing_shipping_data['billing_country'] == "US" || $billing_shipping_data['shipping_country'] == "US" ? 1 : 0;
						$shippingrate_id =  $_SESSION[$dispatcher['store']]['cart']['shippingrateid'];
						$matchBillingCountry = ($billing_shipping_data['billing_country'] == $store->country || $billing_shipping_data['shipping_country'] == $store->country) ? 1 : 0;
						$matchCountryAndState = ($billing_shipping_data['billing_country'] == $store->country && $billing_shipping_data['billing_state'] == $store->state) || ($billing_shipping_data['shipping_country'] == $store->country && $billing_shipping_data['shipping_state'] == $store->state) ? 1 : 0;
						$billingCountry = ($billing_shipping_data['billing_country'] == "US" || $billing_shipping_data['billing_country'] == "AU" || $billing_shipping_data['billing_country'] == "CA"
											|| $billing_shipping_data['shipping_country'] == "US" || $billing_shipping_data['shipping_country'] == "AU" || $billing_shipping_data['shipping_country'] == "CA") ? 1 : 0;
				
						$store->isOtherCountryStateTax = (($includeStateTaxCountry && $otherstatetax) && $matchBillingCountry && !$matchCountryAndState ) ? 1 : 0;
						if ($billingCountry) {
							if (($matchCountryAndState) || ($otherstatetax > 0 && $includeStateTaxCountry)) {
								$return = $store->calculateCartTotals($shippingrate_id, 1);
							} else {
								$return = $store->calculateCartTotals($shippingrate_id, 0);
							}
						} else {
							if ($matchBillingCountry) {
								$return = $store->calculateCartTotals($shippingrate_id, 1);
							} else {
								$return = $store->calculateCartTotals($shippingrate_id, 0);
							}
						}
						
						echo json_encode($return);
						exit;
					break;
					case 'payment':
						 //error_reporting(E_ALL);
						 $otherstatetax = (float) $store->otherstatetax;
						 $includeStateTaxCountry = $_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" ? 1 : 0;

						 $shippingrate_id =  $_SESSION[$dispatcher['store']]['cart']['shippingrateid'];
						 $matchBillingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == $store->country || $_SESSION[$dispatcher['store']]['shipping_country'] == $store->country) ? 1 : 0;
						 $matchCountryAndState = ($_SESSION[$dispatcher['store']]['billing_country'] == $store->country && $_SESSION[$dispatcher['store']]['billing_state'] == $store->state) || ($_SESSION[$dispatcher['store']]['shipping_country'] == $store->country && $_SESSION[$dispatcher['store']]['shipping_state'] == $store->state) ? 1 : 0;
						 $billingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['billing_country'] == "AU" || $_SESSION[$dispatcher['store']]['billing_country'] == "CA"
						 || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "AU" || $_SESSION[$dispatcher['store']]['shipping_country'] == "CA") ? 1 : 0;
				 
						 $store->isOtherCountryStateTax = (($includeStateTaxCountry && $otherstatetax) && $matchBillingCountry && !$matchCountryAndState ) ? 1 : 0;

						 if ($billingCountry) {
							if (($matchCountryAndState) || ($otherstatetax > 0 && $includeStateTaxCountry) ) {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 1);
							} else {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 0);
							}
						} else {
							if ($matchBillingCountry) {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 1);
							} else {
								$carttotals = $store->calculateCartTotals($shippingrate_id, 0);
							}
						}
						// orig $carttotals = $store->calculateCartTotals($_SESSION[$dispatcher['store']]['cart']['shippingrateid'], 1);
						$_SESSION['store']['total'] = $carttotals['total'];
						if ($carttotals['total'] < $store->minimum_cart_total) {
							$message = "Sorry you cannot checkout yet there is a minimum shopping cart total of $".$store->minimum_cart_total;
							$_SESSION[$dispatcher['store']]['messages'] = $message;                     
							Header("Location:/store/cart");  break;
						}

						if (isset($_SESSION[$dispatcher['store']]) && in_array($_SESSION[$dispatcher['store']]['paymentprovider'], array('stripe', 'stripe-paypal')) ) {
							//Create the payment intnt
							$stripeSecretKey = $db->getOne("SELECT value FROM `jol_payments_settings` WHERE name='onsite_provider_id'"); 
							$stripePubKey = $db->getOne("SELECT value FROM `jol_payments_settings` WHERE name='stripe_publishable_key'"); 
							$_SESSION['store']['stripeSecretKey'] = $stripeSecretKey;
							$_SESSION['store']['currency'] = $shop->store->currency;

							if (is_null($_SESSION[$dispatcher['store']]['client_secret']) && is_null($_SESSION[$dispatcher['store']]['payment_intent_id'])) {
								$shop->createPaymentIntent();
							}
							elseif (!is_null($_SESSION[$dispatcher['store']]['payment_intent_id'])) {
								//check the payment intent and if the amount didn't change
								$shop->retrievePaymentIntent();
							}
						}
						
						//display the card fields and process the initial(not final) checkout
						$shop->Payment($carttotals['total'], "STORE PAYMENT");
						
						$tpl2->assign_by_ref('content', $shop);
						$tpl2->assign('message', $message); 
						
						$tpl2->assign('stripe_publishable_key', $stripePubKey);
						$tpl2->assign('dispatcher', $dispatcher);
						$tpl2->display("co_payment.tpl");
						break;
					case 'success':	
						if (isset($_SESSION[$dispatcher['store']]) && !is_null($_SESSION[$dispatcher['store']]['payment_intent_id'])) {	
							//stripe: once the payment is confirmed 
							$shop->processSuccesfullStorePayment();
							echo json_encode(['success'=>true]); exit;
						}
						break;
					case 'callback':
						$shop->Callback();
						die();
						break;
					case 'complete':
						global $settings;
						$shop->Complete();
						if ($settings->facebook_pixel_id) {
							//Facebook pixel tracking
							$shop->fbqPurchase = "fbq('track', 'Purchase', {value: ".$_SESSION['idev_cart']['amount'].",currency: '".$shop->store->currency."'});";
						}

						$tpl2->assign_by_ref('content',$shop);
						$tpl2->assign_by_ref('settings',$settings);
						$tpl2->assign('dispatcher',$dispatcher);
						$tpl2->display("co_complete.tpl");
						break;
					case '':
					default:
						if ( $store->checkMinimumCartTotal() ) {
							$message = "Sorry you cannot checkout yet there is a minimum shopping cart total of $".$store->minimum_cart_total;
							$_SESSION[$dispatcher['store']]['messages'] = $message;                     
							Header("Location:/store/cart");  break;
						}
						
						$carttotals = $store->calculateCartTotals($_SESSION[$dispatcher['store']]['cart']['shippingrateid'], 1);
						
						$shop->HowToPay();

						$tpl2->assign_by_ref('carttotals', $carttotals);
						$tpl2->assign_by_ref('content',$shop); 
						$tpl2->assign('dispatcher',$dispatcher);
						$tpl2->display('co_htp.tpl');
						break;
					}
					return;
				}
            else if($dispatcher['category'] == 'cart') {                
                if ( !$store->checkMinimumCartTotal() ) {
                    if (substr($_SESSION[$dispatcher['store']]['messages'], 41, 27) == 'minimum shopping cart total') {
                      $_SESSION[$dispatcher['store']]['messages'] = '';
                    }
                }
				#echo 17;
                if (!empty($_SESSION[$dispatcher['store']]['messages'])) {
                  $store->message = $_SESSION[$dispatcher['store']]['messages'];
				}           
				
                $store->showCart();                
            }
            elseif ($dispatcher['category'] == 'update-cart') {
				$return = $store->ajaxShowCart();
				echo json_encode($return);exit;
            
			} 
            else if($dispatcher['category'] == 'configure') {
				if(!in_array($member->admin, array('1','3'))) {
					$message = urlencode("You do not have sufficient privileges for this operation.");
					Header("Location:/static/login?message=".$message);
					die();
				}
				if($dispatcher['product'] == 'categories') {

					$store->configureCategories($STORE_ID);

				} else if($dispatcher['product'] == 'shippingrates') {

					$store->configureShippingRates($STORE_ID);

				} else if($dispatcher['product'] == 'manage_products') {
					$cat = null;
					$filter = !empty($_POST['filter']) ? $_POST['filter'] : '';
					if($filter != '') {
						$cat = filter_var($_POST['filter'], FILTER_SANITIZE_STRING);
					}
					$searchp = !empty($_POST['search']) ? $_POST['search'] : '';
					$search = '';
					if(!empty($searchp)) {
						$search = filter_var($_POST['search'], FILTER_SANITIZE_STRING);
					}               
					$store->manageProducts($STORE_ID,$cat,$search);
					#$tpl->assign('prods',$store->products);
				}else if($dispatcher['product'] == 'reorder_products') {
					#echo "blah";
					#$products = filter_var($_POST['serial'], FILTER_SANITIZE_STRING);
					#global $fp;
					#$fp->log($_POST);
					$store->reorderProducts($_POST['serial'],$_POST['filter']);
					#header("Location:/static/login?message=AJAX");
				} else {

					$store->configureStore($STORE_ID);

				}

			} 
			else if($dispatcher['category'] == 'orders') {

				if(!in_array($member->admin, array('1','3'))) {
					$message = urlencode("You do not have sufficient privileges for this operation.");
					Header("Location:/static/login?message=".$message);
					die();
				}

				if(NeuronCMS::DispatcherHasValue('product')) {

					if($dispatcher['product'] == 'export') $store->exportOrders($STORE_ID);
					$store->viewOrder($dispatcher['product']);
				} else {

					$store->viewOrders($STORE_ID);
				}

			} 
			else if($dispatcher['category'] == 'download') {

				$store->fetchDownload($dispatcher['product']);
			} 
			else {
				if(NeuronCMS::DispatcherHasValue('product')) {

					$product = getProduct($dispatcher['product']);

					if($dispatcher['product_action'] == 'edit') {

						if(!in_array($member->admin, array('1','3'))) {
							$message = urlencode("You do not have sufficient privileges for this operation.");
							Header("Location:/static/login?message=".$message);
							die();
						} else {
							if(isset($_POST['preview'])) {
								getPreview($product['id']);
							} elseif(isset($_POST['showdownloaddata'])) {
								getDownloadFile($product['id']);
							} else {
								$store->editProduct($product);
								$tpl2->assign('filedownload', $store->GetFileDownloads($product['id']));
								$tpl2->assign('filepreviewdownload', $store->GetPreviewDownloads($product['id']));
							}
						}
					} else if($dispatcher['product_action'] == 'delete') {

						if(!in_array($member->admin, array('1','3'))) {

							$message = urlencode("You do not have sufficient privileges for this operation.");
							Header("Location:/static/login?message=".$message);
							die();
						} else {

							$store->deleteProduct($product);
						}
					} else {
                        
                        $store->showProduct($product);
                        if ($store->product['taxexempt'] || !$store->forceaddtax) {
                            if ($member) {
                                if ($store->product['memberdiscount'] == 0 && $store->product['memberdiscount'] != '') {
                                    $pinterest_product_rich_pins_price = $store->product['price'];
                                } else if ($store->product['memberdiscount'] > 0) {
                                    $pinterest_product_rich_pins_price = $store->product['price'] - ($store->product['price'] /100 * $store->product['memberdiscount']);
                                } else if ($store->memberdiscount > 0) {
                                    $pinterest_product_rich_pins_price = $store->product['price'] - ($store->product['price'] /100 * $store->memberdiscount);
                                } else {
                                    $pinterest_product_rich_pins_price = $store->product['price'];
                                }
                            } else {
                                $pinterest_product_rich_pins_price = $store->product['price'];
                            }
                        } else {
                            if ($member) {
                                if ($store->product['memberdiscount'] == 0 && $store->product['memberdiscount'] != '') {
                                    $pinterest_product_rich_pins_price = $store->product['price'] + ($store->product['price'] / 100 * $content->tax);
                                }else if ($content->product['memberdiscount'] > 0) {
                                    $pinterest_product_rich_pins_price = ($store->product['price'] - ($store->product['price'] / 100 * $store->product['memberdiscount'])) + (($store->product['price'] - ($store->product['price'] / 100 * $store->product['memberdiscount'])) / 100 * $content->tax);
                                } else if ($content->memberdiscount > 0) {
                                    $pinterest_product_rich_pins_price = ($store->product['price'] - ($store->product['price'] / 100 * $store->memberdiscount)) + (($store->product['price'] - ($store->product['price'] / 100 * $store->memberdiscount)) / 100 * $store->tax);
                                } else {
                                    $pinterest_product_rich_pins_price = $store->product['price'];
                                }
                            } else {
                                $pinterest_product_rich_pins_price = $store->product['price'];
                            }
                        }
                        
                        $tpl2->assign('product_rich_pins', true);
                        $tpl2->assign('pinterest_product_rich_pins_price', $pinterest_product_rich_pins_price);
					}
				} else {

					$store->showProducts($STORE_ID,$dispatcher['category'],$_GET['p']);
				}
			}
		} 
		else {
			$_GET['p'] = !empty($_GET['p']) ? $_GET['p'] : '';
			$store->showProducts($STORE_ID,0,$_GET['p']);

		}
	} else {

		$store->template = "storeclosed.tpl";
	}
} else {

	if(!in_array($member->admin, array('1','3'))) {

		$message = urlencode("You must log in as an administrator to configure the store first.");
		Header("Location:/static/login?message=".$message);
		die();
	}

	if($dispatcher['product'] == 'categories') {

		$store->configureCategories($STORE_ID);
	} else if($dispatcher['product'] == 'shippingrates') {

		$store->configureShippingRates($STORE_ID);
	} else {

		if($dispatcher['category'] != 'configure') {
			Header("Location:/".$dispatcher['store']."/configure"); die();
		} else {

			$store->configureStore($STORE_ID);
		}
	}
}
$tpl2->assign('productstoreplanlimit',$store->GetRecordLimitForProductStorePlan());
$tpl2->assign('message',$store->message);
$tpl2->assign_by_ref('content',$store);
$tpl2->assign_by_ref('member',$member);
$tpl2->assign('dispatcher',$dispatcher);
$tpl2->display($store->template);
?>

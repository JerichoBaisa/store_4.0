<?PHP

function getProducts($STORE_ID,$categoryid,$includesubcategories=false)
{
	global $db;
	global $member;

	if($member->admin == '1')
	{
		$sql = "SELECT * FROM store_products where discontinued=0 and storeid='".$STORE_ID."'";
	}
	else if($member->admin == '0')
	{
		$sql = "SELECT * FROM store_products where discontinued=0 and storeid='".$STORE_ID.
				"' AND (visibility='PUBLIC' OR visibility='MEMBERS')";
	}
	else
	{
		$sql = "SELECT * FROM store_products where discontinued=0 and storeid='".$STORE_ID.
				"' AND visibility='PUBLIC'";
	}

	if($categoryid)
	{
		if($includesubcategories)
		{
			$subcategories = getSubCategoriesByCategory($categoryid,'FLAT',$categoryarray = null);  // null variable needed for recursive call

			if(count($subcategories))
			{
				$subcategories = implode(", ",$subcategories);
				$sql .= " AND categoryid IN (".$subcategories.")";

			}
			else
			{
				$sql .= sprintf(" AND categoryid = %s".$db->qstr($categoryid));
			}
		}
		else
		{
			$sql .= sprintf(" AND categoryid = %s", $db->qstr($categoryid));
		}
	}

	$prods = $db->getAll($sql);

	if(!$categoryid)
		$categoryid = 0;

	$sql2 = sprintf("SELECT * FROM store_products_categories WHERE category_id = %s ORDER BY store_products_categories.order",$categoryid);
	$res2 = $db->getAll($sql2);

	if($res2) {

       foreach($prods as $k2=>$p) {
         $prods[$k2]['order'] = 0;
	       foreach($res2 AS $k=>$o) {
                if($p['id'] == $o['product_id']) {
                    $prods[$k2]['order'] = $o['order'];
                }
            }
        }

        $products = array_sort($prods, "order", "SORT_ASC");

	} else {
		$products = $prods;
	}

	return $products;
}

function getProduct($productid)
{
	global $db;
	global $member;
  $productid = filter_var($productid, FILTER_SANITIZE_NUMBER_INT) ;
  
	if($member->admin == '1')
	{
		$sql = sprintf("SELECT * FROM store_products where discontinued=0 AND id = %d", $productid);
	}
	else if($member)
	{
		$sql = sprintf("SELECT * FROM store_products where discontinued=0 AND id = %d AND (visibility='PUBLIC' OR visibility='MEMBERS')", $productid);
	}
	else
	{
		$sql = sprintf("SELECT * FROM store_products where discontinued=0 AND id = %d AND visibility='PUBLIC'", $productid);
	}

	$product = $db->getRow($sql);
	
	return $product;
}

function getProductsByCategory($STORE_ID,$categoryid,$includesubcategories)
{
	return getProducts($STORE_ID, $categoryid,$includesubcategories);
}

function getCategoryLevel($cat,$l=0) {
	$l++;
	$cats = getCategories();
	foreach($cats as $k => $c) {
		if($c['id'] == $cat['parent']) {
			$level = getCategoryLevel($c,$l);
			if($level) {
				return($level);
				break;
			} else {
				return($l);
				break;
			}
		}
	}
	return($l);
}

function spaceCategoryName($cat) {
	$l = getCategoryLevel($cat);
	while($idx < $l) {
		$cat['name'] = "&nbsp;&nbsp;".$cat['name'];
		$idx ++;
	}
	return($cat['name']);
}

function getCategories()
{
	global $db;
	global $member;

	if($member->admin == '1')
	{
		$sql = "SELECT * FROM store_categories ORDER BY name ASC";
	}
	else if($member)
	{
		$sql = "SELECT * FROM store_categories WHERE (visibility='PUBLIC' OR visibility='MEMBERS') ORDER BY name ASC";
	}
	else
	{
		$sql = "SELECT * FROM store_categories WHERE visibility='PUBLIC' ORDER BY name ASC";
	}
	#vd::dump($sql);
	$results = $db->GetAll($sql);
	return $results;
}


function getSubCategoriesByCategory($categoryid,$mode,&$subcategories)
{
	global $db, $member;
  $categoryid = filter_var($categoryid, FILTER_SANITIZE_NUMBER_INT);
	if(!isset($subcategories)) $subcategories = array();

	if($member->admin == '1')
	{
		$sql = sprintf("SELECT id from store_categories where parent=%d", $categoryid);
	}
	else if($member)
	{
		$sql = sprintf("SELECT id from store_categories where parent=%d AND  (visibility='PUBLIC' OR visibility='MEMBERS')", $categoryid);
	}
	else
	{
		$sql = sprintf("SELECT id from store_categories where parent=%d AND visibility='PUBLIC'", $categoryid);
	}

	$categories = $db->getAll($sql);

	if($mode == 'FLAT')
	{
		if($categoryid)
		{
			$subcategories[$categoryid] = $categoryid;
		}

		foreach($categories as $category)
		{
			getSubCategoriesByCategory($category['id'],$mode,$subcategories);
		}

	}
	else if($mode == 'ARRAY')
	{
		$subs = array();
		foreach($categories as $category)
		{
			if($category['id'])	$subs = getSubCategoriesByCategory($category['id'],$mode,$subs);
		}

		$subcategories[$categoryid] = $subs;
	}

	return $subcategories;
}

function categoriesToDisplayArray(&$categorytree,$count,&$categoryflat)
{
	//create a flat array of placeholders if the array doesn't exist
	if(!isset($categoryflat)) $categoryflat = getSubCategoriesByCategory(0,'FLAT',$subcats = null);

	$count++;

	foreach($categorytree as $key => $category)
	{
		if(count($category)) categoriesToDisplayArray($category,$count,$categoryflat);
		$categoryflat[$key] = $count;
	}

	$count--;

	return $categoryflat;
}

function getCategoryHistory($categoryid,&$path)
{
	global $db;

	//find the root of the category
	$sql = sprintf("SELECT id,parent,name from store_categories where id=%d",$db->qstr($categoryid));

	$category = $db->getRow($sql);

	if(!empty($category['parent']))	$path = getCategoryHistory($category['parent'],$path);
	if($category)$path[] = array('id'=>$categoryid,'name'=>$category['name']);
	return $path;
}

function getChildCategories($STORE_ID,$categoryid)
{
	$childcategories = array();
	foreach(getCategories() as $key => $category)
	{

		if($category['parent'] == $categoryid)
		{

			$category['itemcount'] = count(getProductsByCategory($STORE_ID, $category['id'],true));  //true as we want the products in sub categories

			$childcategories[] = $category;
		}
	}

	return $childcategories;
}

function getShippingRates()
{
	global $db;
	$sql = "SELECT * FROM store_shipping_rates ORDER BY id ASC";
	$results = $db->GetAll($sql);
	return $results;
}

function getShippingRate($rateid)
{
	global $db;
	$sql = sprintf("SELECT * FROM store_shipping_rates WHERE id=%d",  filter_var($rateid, FILTER_SANITIZE_NUMBER_INT));
	$results = $db->GetRow($sql);
	return $results;
}


function getShippingRatesForProduct($productid)
{
	global $db;
	$sql = sprintf("SELECT * FROM store_shipping_rates_product
					LEFT JOIN store_shipping_rates ON store_shipping_rates_product.shippingrateid = store_shipping_rates.id
					WHERE productid = %d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	$rates = $db->getAll($sql);
	return $rates;
}

function getShippingRateForProductByRateId($productid,$rateid)
{
	global $db;

	$sql = sprintf("SELECT cost FROM store_shipping_rates_product WHERE productid = %d AND shippingrateid = %d",
      filter_var($productid, FILTER_SANITIZE_NUMBER_INT), $rateid);
	$rate = $db->getRow($sql);
	return $rate['cost'];
}

function isStoreConfigured($id)
{
	global $db;
	$result = array();
	$sql = sprintf("SELECT configured FROM store WHERE id=%d",filter_var($id, FILTER_SANITIZE_NUMBER_INT));
	$result = $db->GetRow($sql);
	return $result['configured'];
}

function getStoreConfig($id)
{
	global $db;
	$result = array();
	$sql = sprintf("SELECT * FROM store WHERE id=%s", filter_var($id, FILTER_SANITIZE_NUMBER_INT));
	$result = $db->GetRow($sql);

	return $result;
}

function deleteDownloadPreview($productid)
{
	global $db;
	$sql = sprintf("UPDATE store_downloads SET preview=NULL, previewsize=NULL, previewtype=NULL, previewname=NULL
					WHERE productid=%d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	$db->Execute($sql);
}

function deleteProductImage($productid)
{
	global $db, $dispatcher;

	@unlink(dirname($_SERVER['SCRIPT_FILENAME']).'/custom/'.$dispatcher['store'].'/images/full/'.$productid.'_full.jpg');
	@unlink(dirname($_SERVER['SCRIPT_FILENAME']).'/custom/'.$dispatcher['store'].'/images/thumb/'.$productid.'_thumb.jpg');

	$sql = sprintf("UPDATE store_products SET hasimage='0' WHERE id=%d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	$db->Execute($sql);
}


function reduceStockForProduct($productid, $quantity)
{
	global $db;
	$product = getProduct($productid);

	$remainingstock = $product['stock'] - $quantity;
	if($remainingstock < 0)	$remainingstock = 0;

	$sql = sprintf("UPDATE store_products SET stock=%d WHERE id=%d",$remainingstock,$productid);

	$db->Execute($sql);

}

function activateDownload($orderid,$productid)
{
	global $db, $settings;

    if (SITE_NAME == 'loveprayteach' || SITE_NAME == 'oscarg') {
		$ACTIVE_DAYS = (empty($settings->product_validity_days) ? 90 : $settings->product_validity_days);
	} else if (SITE_NAME == 'youniverse') {
		$ACTIVE_DAYS = (empty($settings->product_validity_days) ? 2 : $settings->product_validity_days);
	} else {
		$ACTIVE_DAYS = (empty($settings->product_validity_days) ? 1 : $settings->product_validity_days);
	}

	$downloadcode = md5(microtime());
  $productid = filter_var($productid, FILTER_SANITIZE_NUMBER_INT);
	$sql = sprintf("INSERT INTO store_downloads_active(id, orderid, downloadid, downloadcode, expires, downloadcount, productid)
			VALUES (NULL, '%d',%s,'%s',NOW() + INTERVAL '%d' DAY, '0', '%d')",
        $orderid,$productid,$downloadcode, $ACTIVE_DAYS, $productid);

	if($db->Execute($sql))
	{
		return $downloadcode;
	}
	else
	{
		return false;
	}

}

function activateArticle($orderid,$productid)
{
	global $db;

	$article = getArticleForProduct($productid);

	if(!$article['accesstime']) $article['accesstime'] = 1000;  // 3 years for no limit


	$articlecode = md5(microtime());

	$sql = sprintf("INSERT INTO store_articles_active(id, orderid, articleid, articlecode, expires, accessed)
			VALUES (NULL, '%d','%s','%s',NOW() + INTERVAL '%d' DAY,'0')",$orderid, $article['articleid'],$articlecode,
			$article['accesstime']);

	if($db->Execute($sql))
	{
		return $articlecode;
	}
	else
	{
		return false;
	}

}


function checkProductHasDownload($productid)
{
	global $db;
	$sql = sprintf("SELECT id from store_downloads where productid=%d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	return count($db->getRow($sql));
}

function checkProductHasPreview($productid)
{
	global $db;
	$sql = sprintf("SELECT previewsize from store_downloads where productid=%d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	$result = $db->getRow($sql);
	if ($result) {
		return $result['previewsize'];
	}
}

function checkDownloadIsValid($downloadcode)
{
	global $db, $settings;

	//$settings->product_download_limit
    if (SITE_NAME == 'loveprayteach' || SITE_NAME == 'youniverse') {
		$MAX_DOWNLOAD_COUNT = (empty($settings->product_download_limit) ? 5 : $settings->product_download_limit);
	}
    else {
		$MAX_DOWNLOAD_COUNT = (empty($settings->product_download_limit) ? 2 : $settings->product_download_limit);
	}

	$sql = sprintf("SELECT * FROM store_downloads_active WHERE downloadcode = %s AND expires > NOW() AND downloadcount < %d",
					$db->qstr($downloadcode), $MAX_DOWNLOAD_COUNT);

	return count($db->getRow($sql));
}

function checkArticleIsValid($articleid,$articlecode)
{
	global $db, $tpl;
  $articleid = filter_var($articleid, FILTER_SANITIZE_STRING);
	$sql = sprintf("SELECT *, UNIX_TIMESTAMP(`expires`) AS utime FROM store_articles_active WHERE articlecode = %s
					AND articleid=%d",$db->qstr($articlecode), $articleid);

	$active = $db->getRow($sql);

	if (!count($active)) return 'NOCODE';

	$sql = sprintf("SELECT * from store_articles WHERE
				   	articleid=%s",$db->qstr($active['articleid']));
	$article = $db->getRow($sql);

	if ($active['accessed']>=$article['accesslimit'] && $article['accesslimit'] != 0)return 'EXCEEDED';
	if ($active['utime']<time())return 'EXPIRED';

	if (($active['utime']-time()) < 86400 ){
		$tpl->assign('message','Your access to this article expires in less than 24 hours');
	}

	if ($article['accesslimit'] && ($article['accesslimit'] - $active['accessed']) < 3 ){
	   $left = $article['accesslimit'] - $active['accessed'] - 1;
	   $tpl->assign('message','You are entitled to view this article '.$left .' more times');
	}
	return 'OK';


}

function incrementArticleViews($articlecode)
{
	global $db;

	$sql = sprintf("UPDATE store_articles_active SET accessed = accessed +1  WHERE articlecode = '%s'",
				   $articlecode);

	$db->Execute($sql);

}



function getDownloadData($downloadcode)
{
	global $db;

	$sql = sprintf("SELECT downloadid,downloadcount,productid FROM store_downloads_active WHERE downloadcode = %s",$db->qstr($downloadcode));

	$download = $db->getRow($sql);

	$sql = sprintf("SELECT data,datasize,datatype, replace(dataname, ',', '') as dataname FROM store_downloads WHERE productid = '%d'",$download['productid']);

	$downloaddata = $db->getRow($sql);

	Header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	Header(sprintf("Content-type: %s",$downloaddata['datatype']));
	Header(sprintf("Content-length: %s", $downloaddata['datasize']));
	Header(sprintf("Content-Disposition: attachment; filename=%s",$downloaddata['dataname']));
	Header("Content-Transfer-Encoding: binary");
	Header("Content-Description: PHP Generated Data");
	if ($downloaddata['data']=='FS'){

		$filedir = dirname($_SERVER['SCRIPT_FILENAME']);
		$path = $filedir.'/storefiles/'.$download['downloadid'];
		echo file_get_contents($path);
	}else{
		echo $downloaddata['data'];
	}

	$downloadcount = $download['downloadcount'] + 1;

	$sql = sprintf("UPDATE store_downloads_active SET downloadcount = '%d' WHERE downloadcode = '%s'", $downloadcount,$downloadcode);

	$db->Execute($sql);

	die();

}

function getPreview($productid)
{
	global $db;

	$sql = sprintf("SELECT preview,previewsize,previewtype,previewname FROM store_downloads WHERE productid = %d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	$previewdata = $db->getRow($sql);

	Header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	Header(sprintf("Content-type: %s",$previewdata['previewtype']));
	Header(sprintf("Content-length: %s", $previewdata['previewsize']));
	Header(sprintf('Content-Disposition: attachment; filename="%s"',$previewdata['previewname']));
	Header("Content-Transfer-Encoding: binary");
	Header("Content-Description: PHP Generated Data");
	if ($previewdata['preview']=='FS'){

		$filedir = dirname($_SERVER['SCRIPT_FILENAME']);
		$path = $filedir.'/storefiles/'.$productid.'_preview';
		echo file_get_contents($path);
	}else{
		echo $previewdata['preview'];
	}
	die();
}

//Jericho downloadfile preview
function getDownloadFile($productid) {
	global $db;

	$sql = sprintf("SELECT `data`, datasize, datatype, replace(dataname, ',', '') as dataname FROM store_downloads WHERE productid = %d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	$previewdata = $db->getRow($sql);

	Header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	Header(sprintf("Content-type: %s",$previewdata['datatype']));
	Header(sprintf("Content-length: %s", $previewdata['datasize']));
	Header(sprintf('Content-Disposition: attachment; filename="%s"',$previewdata['dataname']));
	Header("Content-Transfer-Encoding: binary");
	Header("Content-Description: PHP Generated Data");
	if ($previewdata['data']=='FS'){

		$filedir = dirname($_SERVER['SCRIPT_FILENAME']);
		$path = $filedir.'/storefiles/'.$productid;
		echo file_get_contents($path);
	}else{
		echo $previewdata['data'];
	}
	die();
}

function getArticleForProduct($productid)
{
	global $db;
	$sql = sprintf("SELECT * from store_articles where productid=%d", filter_var($productid, FILTER_SANITIZE_NUMBER_INT));
	return $db->getRow($sql);
}

function getProductForArticle($articleid)
{
	global $db;
    $articleid = filter_var($articleid, FILTER_SANITIZE_STRING);
	$sql = sprintf("SELECT * from store_articles where articleid='%s'", $articleid);
	return $db->getRow($sql);
}


function getOrders($STORE_ID, $from='', $to='')
{
	global $db;
    $qry = $limit = '';
    if (!empty($from)) {
        $to = empty($to) ? date('Y-m-d') : $to;
        $qry = sprintf(" WHERE placed >= '%s' and placed <= '%s' ", $from, $to);
    }
    else {
        $limit = ' LIMIT 2000';
    }
	$sql = sprintf("SELECT * FROM store_orders %s ORDER BY placed DESC %s", $qry, $limit);
	return $db->getAll($sql);
}
function getOrder($orderid)
{
	global $db;

	$sql = sprintf("SELECT * FROM store_orders WHERE id=%d", filter_var($orderid, FILTER_SANITIZE_NUMBER_INT) );
	return $db->getRow($sql);
}

function updateOrderStatus($orderid,$status)
{
	global $db;
	$sql = sprintf("UPDATE store_orders SET status=%s WHERE id=%d",$db->qstr($status), filter_var($orderid, FILTER_SANITIZE_NUMBER_INT) );
	return $db->Execute($sql);

}
function array_sort($array, $on, $order='SORT_DESC')
    {
      $new_array = array();
      $sortable_array = array();

      if (count($array) > 0) {
          foreach ($array as $k => $v) {
              if (is_array($v)) {
                  foreach ($v as $k2 => $v2) {
                      if ($k2 == $on) {
                          $sortable_array[$k] = $v2;
                      }
                  }
              } else {
                  $sortable_array[$k] = $v;
              }
          }

          switch($order)
          {
              case 'SORT_ASC':
                  asort($sortable_array);
              break;
              case 'SORT_DESC':
                  arsort($sortable_array);
              break;
          }

          foreach($sortable_array as $k => $v) {
              $new_array[] = $array[$k];
          }
      }
      return $new_array;
    }

?>

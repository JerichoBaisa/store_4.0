{if $content->settings.plan_header}<h1>{$content->settings.plan_header}</h1>{/if}
{if $content->settings.plan_body}<p>{$content->settings.plan_body}</p>{/if}
{if $content->message}{foreach from=$content->message item=reg_message}
<div class="alert alert-success" role="alert">{$reg_message}</div>
{/foreach}{/if}
{$content->form.promocode.label}
<form {$content->form.attributes}>
{$content->form.hidden}
<table class="table_with_padding" width="100%"cellspacing="0" cellpadding="1">

   <tr>
 <td width="20%" valign="top">{$content->form.paymentplan.label}</td>
 <td><div class="warning">{$content->form.paymentplan.error}</div>{$content->form.paymentplan.html}
 </td>
 </tr>

    <tr>
 <td width="20%" valign="top">{$co$_SESSION[$dispatcher['action']]ntent->form.paymentprovider.label}</td>
 <td valign="middle"><div class="warning">{$content->form.paymentprovider.error}</div>{$content->form.paymentprovider.html}
 </td>
 </tr>

<tr>
<td valign="top"></td>
<td>{$content->form.goback.html}&nbsp;{$content->form.gonext.html}</td>
</tr>

</table>

</form>

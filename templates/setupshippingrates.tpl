<br>
<form {$content->form.attributes}>
{$content->form.hidden}
<h2>Shipping Rates</h2>
<br>
<table id="shippingrate_table" width="100%" border="0" cellpadding="2" cellspacing="2">
	<tr>
    <td bgcolor="#dddddd">Rate Name</td>
    {if $content->shippingcharge eq 'PC'}
    <td bgcolor="#dddddd">Item Cost</td>
    <td bgcolor="#dddddd">Multiple Item % Discount</td>
    {/if}
    {if $content->shippingcharge eq 'PB'}
    <td bgcolor="#dddddd">Basket Cost</td>
    {/if}

		
		<td  bgcolor="#dddddd">Remove</td>
	</tr>
	
	{foreach from=$content->shippingratesholder item=shippingrate}
		<tr bgcolor="{cycle values="#aaaaaa,#bbbbbb"}">
		{foreach from=$shippingrate item=shippingitem}
			<td>{$content->form.$shippingitem.html}</td>
		{/foreach}
		</tr>
	{/foreach}

	<tr bgcolor="#dddddd">
		<td colspan="5">{$content->form.addshippingrate.html}</td>
	</tr>
	<tr><td><br>{$content->form.cancel.html}</td><td colspan="4" align="right"><br>{$content->form.submit.html}</td></tr>
</table>
</form>

{literal}
<script type="text/javascript">

$( document ).ready(function() {
	$('#shippingrate_table').find('input[type=text]').each(function() {
   
    let textid = $(this).attr('name').toString();
    
		if(textid.includes("shipping_defaultitemcost") || textid.includes("shipping_basketcost") || textid.includes("shipping_percentdiscount") ) {
        setInputFilter(document.getElementById(textid), function(value) {
        return /^-?\d*$/.test(value); 
      });
    }

  });
	   
});

function setInputFilter(textbox, inputFilter) {
  console.log('textbox', textbox);
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

</script>
{/literal}

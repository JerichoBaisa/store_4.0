{literal}
<!-- DataTables CSS -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">


<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" >
  $(document).ready( function () {
    
    $("#date_from").datepicker();
    $("#date_to").datepicker();
    
    $('#clinical-table').DataTable(
      {
        "lengthMenu": [[100, -1], [100, "All"]],
        "ordering": true,
        "order": [[ 3, "desc" ]],
        "columns": [
          { "orderable": false },
          { "orderable": false },
		  { "orderable": true },
          { "orderable": false },
          { "orderable": true },
          { "orderable": false },
          { "orderable": false },
        ],
        "info":     false,
        "bFilter": true
      }
    );   
  });
</script>
{/literal}
<div class="well">
<h2>Store Orders</h2>
<p>Enter a pair of Date Ranges using the pop-out calendars. Click Show Result to see order details.</p>
<a href="https://support.subhub.com/hc/en-us/articles/360047046312-How-to-View-Your-Store-Orders" target="_blank" ><i class="fa fa-file-text" aria-hidden="true" style="margin-right: 5px;"></i>Support Guide</a><br />
</div>
<form {$content->form.attributes}>
  
  <table class="table">
    <tr><td colspan="2"><h4>Date Range</h4></td></tr>
    <tr> 
      <td style="width: 40%;">From : <input type="text" name="date_from" id="date_from" value="{if (isset($date_from) && !empty($date_from) ) } {$date_from} {else} {/if}" required autocomplete="off"></td>
      <td >To : <input type="text" name="date_to" id="date_to" value="{if (isset($date_to) && !empty($date_to)) } {$date_to} {else}{/if}" required autocomplete="off"></td>
    </tr>    
    <tr>
      <td colspan="2">
        <input type="submit" id="search" name="search" value="Show Result" class="btn btn-default"> &nbsp; &nbsp; 
        <input type="submit" id="export" name="export" value="Export as Excel" class="btn btn-default" />
      </td>
    </tr>
  </table>
   
  <table id="clinical-table" class="table table-striped table-bordered dataTable no-footer">
	<thead>
      <tr>
		<th>Order #</th>
		<th>Name</th>
		<th>Product</th>
		<th>Amount</th>
		<th>Placed</th>
		<th>Status</th>
		<th>Details</th>
      </tr>
    </thead>
	<tbody> 
	{foreach from=$content->orders item=order}
      
	
		{if $order.status eq 'NEW'}
			{assign var='fontcolour' value='#FF0000'}
		{elseif $order.status eq 'COMPLETED'}
			{assign var='fontcolour' value='#00AA00'}
		{elseif $order.status eq 'CANCELLED'}
			{assign var='fontcolour' value='#BBBBBB'}
		{else}
			{assign var='fontcolour' value='#000000'}
		{/if}
		
      
		<tr bgcolor="{cycle values="#dddddd,#eeeeee"}">            
			<td><font color="{$fontcolour}">{$order.id}</font></td>
			
            {if $order.type eq 'member'}
              <td><font color="{$fontcolour}">{$order.member_details.billing_title}&nbsp;{$order.member_details.billing_name_first}&nbsp;{$order.member_details.billing_name_last}</font></td>
			  <td><font color="{$fontcolour}">
				{foreach from=$order.details.cartitems item=item}
					{$item.name}
				{/foreach}
				  </font></td>
              <td><font color="{$fontcolour}">{$content->currencysymbol}{$order.member_details.carttotals.total}</font></td>
              <td><font color="{$fontcolour}">{$order.placed}</font></td>
              <td>
                <font color="{$fontcolour}">Subscription</font>
              </td>
              <td>
                <font color="{$fontcolour}"><a href="/members/{$order.id}/edit">Details</a></font>
              </td>
            {else}
              <td><font color="{$fontcolour}">{$order.details.billing_title}&nbsp;{$order.details.billing_name_first}&nbsp;{$order.details.billing_name_last}</font></td>
			  <td><font color="{$fontcolour}">
				{foreach from=$order.details.cartitems item=item}
					{$item.name}
				{/foreach}
				  </font></td>
              <td><font color="{$fontcolour}">{$content->currencysymbol}{$order.details.carttotals.total}</font></td>
              <td><font color="{$fontcolour}">{$order.placed}</font></td>
              <td>
                <font color="{$fontcolour}">
                  {foreach from=$order.statusfields item=statusfield}
                      {$content->form.$statusfield.html}
                  {/foreach}
                </font>
              </td>
              <td>
                <font color="{$fontcolour}"><a href="/{$dispatcher.store}/orders/{$order.id}" target="_blank">Details</a></font>
              </td>
            {/if}
		</tr>
        

	{/foreach}
    </tbody> 

	<tr><td colspan="7" align="right">{$content->form.submit.html}</td></tr>
</table>

</form>
<br>
<strong><a href="/{$dispatcher.store}/">Back to store</a><strong>
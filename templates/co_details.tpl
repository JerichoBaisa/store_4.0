{literal}
<link rel="stylesheet" type="text/css" href="/css/spinner.css">
<script>
function toggleSame(){
	var state = document.form.shipping_same.checked;
	if(state){
		
		document.form.shipping_name_first.value = document.form.billing_name_first.value;
		document.form.shipping_name_last.value = document.form.billing_name_last.value;
		document.form.shipping_address.value = document.form.billing_address.value;
		document.form.shipping_city.value = document.form.billing_city.value;
		document.form.shipping_zip.value = document.form.billing_zip.value;
		document.form.shipping_state.value = document.form.billing_state.value;
		document.form.shipping_country.value = document.form.billing_country.value;
		
	}
}
</script>
{/literal}

{if $settings->facebook_pixel_id neq ''}
    {literal}
        <script>
    {/literal}
        
    {$content->fbq}

    {literal}
        </script>
    {/literal}
{/if}

<div class="confirm_progress_holder" style="display: none;">
    <div id="progress_popup_box">
      <!-- <h2 class="fs-title">Please wait while we set up<br>your Subhub Editor</h2> -->
      <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
    </div>
  </div>

<div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12 offset-sm-0 col-xs-12 offset-xs-0">
    {if $content->message}
        {foreach from=$content->message item=reg_message}
            <div class="alert alert-success" role="alert">{$reg_message}</div>
        {/foreach}
    {/if}
      <div class="row">
         <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 mb-4">
            
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

               
                    {if $content->settings.sandb_header}
                        <h1>{$content->settings.sandb_header}</h1>
                    {/if}

                    {if $content->settings.sandb_body}
                        <p>{$content->settings.sandb_body}</p>
                    {/if}

                    <form {$content->form.attributes}>
                        {$content->form.hidden}
                        {foreach from=$content->additional_elements key=k item=v}

                            {if $k NEQ '#'}
                                <legend>{$k}</legend>
                            {/if}

                            
                            {foreach item=element from=$v}
                                <!-- elements with alternative layout in external template file-->
                                {if $content->form.$element.style}
                                    <div class="form-group">
                                        {include file="smarty-dynamic-`$content->form.$element.style`.tpl"} 
                                    </div>
                                <!-- submit or reset button (don't display on frozen forms) -->
                                {elseif $content->form.$element.type eq "submit" or $content->form.$element.type eq "reset"}
                                    {if not $content->form.frozen}
                                        <div class="form-group">
                                            {$content->form.$element.html}
                                        </div>
                                    {/if}
                                    
                                <!-- normal elements -->
                                {else}
                                    <div class="form-group">
                                        <label>
                                            {if $content->form.$element.required}
                                                <span class="text-danger">*</span>
                                            {/if}
                                            {$content->form.$element.label}
                                        </label>

                                        {if $content->form.$element.type eq "group"}
                                            {foreach key=gkey item=gitem from=$content->form.$element.elements}
                                                {$gitem.label}
                                                {$gitem.html}

                                                {if $gitem.required}
                                                    <span class="text-danger">*</span>
                                                {/if}                  

                                                {if $content->form.$element.separator}
                                                    {cycle values=$content->form.$element.separator}
                                                {/if}

                                            {/foreach}
                                        {else}
                                            {$content->form.$element.html}
                                        {/if}

                                        {if $content->form.$element.error}
                                            <span class="text-danger warning">{$content->form.$element.error}</span>
                                        {/if}

                                        <small class="form-text text-muted">
                                            <p class="text-danger">{$content->form.$element.label_note}</p>
                                        </small>
                                    </div>
                                {/if}
                            {/foreach}   

                        {/foreach}
                        <!-- /Form Fields -->

                    
                        <!-- Buttons -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {$content->form.goback.html}
                                {$content->form.gonext.html}
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {$content->form.requirednote}
                            
                            </div>
                            
                        </div>
                        <!-- /Buttons -->
                    </form>
                </div>
            </div>

        </div>
         <!-- Order Summary -->
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
            <div id="summary" class="bordered-box">
            <h5 class="mb-4 font-weight-bold">Order Summary</h5>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <p class="m-0">
                        Subtotal 
                    </p>
                </div>
                <div class="col-lg-6 col-md-6">
                    <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol}
                        <span id="cart-subtotal"> 
                        {if $content->store->currency eq "JPY"}
                            {$carttotals.subtotal|string_format:"%d"}
                        {else}
                            {$carttotals.subtotal}
                        {/if}
                        </span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <p class="m-0">
                        Shipping Fees
                    </p>
                </div>
                <div class="col-lg-6 col-md-6">
                    <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol} 
                        <span class="amount font-weight-bold" id="cart-shipping-fees">
                        {if $content->store->currency eq "JPY"}
                            {$carttotals.shippingcost|string_format:"%d"}
                        {else}
                            {$carttotals.shippingcost}
                        {/if}
                        </span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <p class="m-0">
                        Tax
                    </p>
                    </div>
                <div class="col-lg-6 col-md-6">
                    <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol}
                        <span class="amount font-weight-bold" id="cart-total-tax">
                        {if $content->store->currency eq "JPY"}
                            {$carttotals.totaltax|string_format:"%d"}
                        {else}
                            {if $carttotals.totaltax eq 0 }
                                -
                            {else}
                                {$carttotals.totaltax}
                            {/if}
                        {/if}
                        </span>
                    </p>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <p class="m-0">
                        Total
                    </p>
                </div>
                <div class="col-lg-6 col-md-6">
                    <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol} 
                        <span class="amount font-weight-bold" id="cart-total">
                            {if $content->store->currency eq "JPY"}
                                {$carttotals.total|string_format:"%d"}
                            {else}
                                {$carttotals.total}
                            {/if}
                        </span>
                    </p>	
                </div>
            </div>
            </div>
        </div>
        <!-- /Order Summary -->

    </div>
</div>

{literal}
<script type="text/javascript">
$( document ).ready(function() {

    $('select[name=billing_country], select[name=billing_state], select[name=shipping_state], select[name=shipping_country]').on('change', function(e) {
        // $(".confirm_progress_holder, #progress_popup_box, .wait").css("display", "block");
        $.ajax({
            url:'/store/checkout/updatebilling-order-summary',
            data:{ 'value':$("form").serialize() },
            type:'POST',
            dataType: "json",
            success: function(json) {
            console.log('jsonData',json);
            
            var totaltax = '-';
            if (json.totaltax.length) {
                totaltax = parseFloat( json.totaltax ).toFixed(2);
            }
            
            var subtotal = '-';
            if (json.subtotal.length) {
                subtotal = parseFloat( json.subtotal ).toFixed(2);
            }
            
            var shipping_cost = '-';
            if (json.shippingcost.length) {
                shipping_cost = parseFloat(json.shippingcost).toFixed(2);
            }
            
            var total = '-';
            if (json.total.length) {
                total = parseFloat(json.total).toFixed(2);
            }
            
            $('#cart-subtotal').text(subtotal);
            $('#cart-shipping-fees').text(shipping_cost);
            $('#totaltax').text(totaltax);
            $('#cart-total-tax').text(totaltax);
            $('#cart-total').text(total);
            // $(".confirm_progress_holder, #progress_popup_box, .wait").css("display", "none");
            return false;        
            },
            error: function() {
                $( "input[name='updatecart']" ).trigger( "click" );
            }
        });
    });
});

</script>
{/literal}
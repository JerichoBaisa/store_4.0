<br>
<form {$content->form.attributes}>
{$content->form.hidden}
<h2>Product Category Configuration</h2>
<br>
<table width="100%" border="0" cellpadding="2" cellspacing="2">
	<tr>
		<td bgcolor="#dddddd">Category Name</td>
		<td bgcolor="#dddddd">Description</td>
		<td bgcolor="#dddddd">Visibility</td>
		<td bgcolor="#dddddd">Parent Category</td>
		<td align="center" bgcolor="#dddddd">Remove</td>
	</tr>
	
	{foreach from=$content->categoriesholder item=category}
		<tr bgcolor="{cycle values="#aaaaaa,#bbbbbb"}">
		{foreach from=$category item=categoryitem}
			<td valign="top">{$content->form.$categoryitem.html}</td>
		{/foreach}
		</tr>
	{/foreach}

	<tr bgcolor="#dddddd">
		<td colspan="5">{$content->form.addcategory.html}</td>
	</tr>
	<tr><td><br>{$content->form.cancel.html}</td><td colspan="4" align="right"><br>{$content->form.submit.html}</td></tr>
</table>
</form>

{literal}
	<script language="javascript">

        function showDownload()
        {
            document.getElementById('download').style.display = "block";
            document.getElementById('article').style.display = "none";
            document.getElementById('shipping').style.display = "none";
        }

        function showProduct()
        {
            document.getElementById('download').style.display = "none";
            document.getElementById('article').style.display = "none";
            document.getElementById('shipping').style.display = "block";
        }

        function showArticle()
        {
            document.getElementById('article').style.display = "block";
            document.getElementById('download').style.display = "none";
            document.getElementById('shipping').style.display = "none";
        }

        function addLoadEvent(func)
        {
            var oldonload = window.onload;
            if (typeof window.onload != 'function')
            {
                window.onload = func;
            } else
            {
                window.onload = function ()
                {
                    if (oldonload)
                    {
                        oldonload();
                    }
                    func();
                }
            }
        }


        addLoadEvent(function ()
        {

            if (document.form.type[0].checked)
            {
                showProduct();
            }

            if (document.form.type[1].checked)
            {
                showDownload();
            }

            if (document.form.type[2].checked)
            {
                showArticle();
            }

        })

	</script>
	<script type="text/javascript" src="/js/tinymce/tinymce.min.js?{$smarty.now}"></script>
        <script type="text/javascript" src="/js/tinymce/init.js?{$smarty.now}"></script>
	<script src="//cdn.jsdelivr.net/npm/mediaelement@4.2.3/build/mediaelement-and-player.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/mediaelement@4.2.3/build/mediaelementplayer.min.css">
	
	<!--New Logo Uploader Dependencies-->
	<link href="/css/kartik-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.
	This must be loaded before fileinput.min.js -->
	<script src="/css/kartik-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
	<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
	This must be loaded before fileinput.min.js -->
	<script src="/css/kartik-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
	<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for HTML files.
	This must be loaded before fileinput.min.js -->
	<script src="/css/kartik-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
	<!-- the main fileinput plugin file -->
	<script src="/css/kartik-fileinput/js/fileinput.min.js"></script>
	<!-- bootstrap.js below is needed if you wish to zoom and view file content 
	in a larger detailed modal dialog -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script> -->
	<!-- optionally if you need a theme like font awesome theme you can include 
	it as mentioned below -->
	<script src="/css/kartik-fileinput/themes/fa/theme.js"></script>
	<!-- optionally if you need translation for your language then include 
	locale file as mentioned below -->
	<!--<script src="/css/kartik-fileinput/js/locales/<lang>.js"></script>-->
	
	<style>
		.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
			margin: 0;
			padding: 0;
			border: none;
			box-shadow: none;
			text-align: center;
		}
		.kv-avatar .file-input {
			display: table-cell;
			max-width: 220px;
		}
	</style>
{/literal}

<div class="row">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="well">
                    <h2> Edit Product</h2><br/>
                    <a href="https://support.subhub.com/hc/en-us/articles/115004837463-Create-a-Store-Download-Product" target="_blank" >SubHub Training Manual : Create a Digital Product</a><br/>
                    <a href="https://support.subhub.com/hc/en-us/articles/115004837583-Create-a-Store-Physical-Product" target="_blank" >SubHub Training Manual : Create a Physical Product</a>
                </div>
				<form {$content->form.attributes}>
					{$content->form.hidden}

					<div class="form-group">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{if $content->form.name.error}<br>{/if}{$content->form.name.label} <font color="#FF0000">*</font></label>
								{if $content->form.name.error}<font color="#FF0000">{$content->form.name.error}</font><br>{/if}
								{$content->form.name.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{if $content->form.categoryid.error}<br>{/if}{$content->form.categoryid.label}</label>
								{if $content->form.categoryid.error}<font color="#FF0000">{$content->form.categoryid.error}</font><br>{/if}
								{$content->form.categoryid.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{$content->form.visibility.label}</label>
								{$content->form.visibility.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<h4>Type</h4>
								{$content->form.type.html}
							</div>
						</div>
						<br>
						<div id="download" style="display:none;">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<label>{$content->form.downloaddata.label} <font color="#FF0000">*</font></label>
									<br>
									{if $filedownload}
								
									<button type="submit" class="btn btn-link" id="showdownloaddata" name="showdownloaddata">{$filedownload}</button>
									{/if}
										
										<div id="kv-avatar-errors-2" class="left-block" style="width:800px;display:none"></div>
										<div class="kv-avatar left-block" style="width:200px">
											<input id="downloaddata" name="downloaddata" type="file" class="file-loading">
										</div>
									
								</div>
							</div>

							<br>

							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<label>{$content->form.downloadpreview.label} <font color="#FF0000">*</font></label>
									<br>
									{if $filepreviewdownload}
									
									<button type="submit" class="btn btn-link" id="preview" name="preview">{$filepreviewdownload}</button>
									
										
										<div id="kv-avatar-errors-3" class="left-block" style="width:800px;display:none"></div>
										<div class="kv-avatar left-block" style="width:200px">
											<input id="downloadpreview" name="downloadpreview" type="file" class="file-loading">
										</div>
									{/if}
								</div>
							</div>

							<br>

							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									{$content->form.deletepreview.html}
								</div>
							</div>
						</div>

						<div id="article" style="display:none;">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>{$content->form.articleid.label}</label>
									{$content->form.articleid.html}
								</div>
							</div>
							<br>

							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>{$content->form.accesstime.label}&nbsp;&nbsp;<small><em>days (0 for unlimited)</em></small></label>
									{$content->form.accesstime.html}
								</div>
							</div>
							<br>

							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>{$content->form.accesslimit.label}&nbsp;&nbsp;<small><em>times (0 for unlimited)</em></small></label>
									{$content->form.accesslimit.html}
								</div>
								<br>
							</div>
							<table class="table_with_padding"  width="100%" border="0" cellpadding="2" cellspacing="2">
								<tr><td width="100"></td><td align="left"></td></tr>
								<tr><td width="100"></td><td align="left"></td></tr>
								<tr><td width="100"></td><td align="left"></td></tr>
							</table>  
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>{$content->form.teaser.label}</label>
								{$content->form.teaser.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>{$content->form.description.label}</label>
								{$content->form.description.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{$content->form.image.label}</label>
								
								<div id="kv-avatar-errors-1" class="left-block" style="width:800px;display:none"></div>
								<div class="kv-avatar left-block" style="width:200px">
									<input id="avatar-1" name="image" type="file" class="file-loading">
								</div>
								<br>
								{if $content->product.hasimage}
									{$content->form.deleteimage.html}
								{/if}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{if $content->form.price.error}<br>{/if}{$content->form.price.label}</label>
								{if $content->form.price.error}<font color="#FF0000">{$content->form.price.error}</font><br>{/if}
								{$content->form.price.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{$content->form.taxexempt.label}</label>
								{$content->form.taxexempt.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{$content->form.memberdiscount.label} (%) <img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Leave this field blank to use the <br/>Default Member Discount (currently {if $content->memberdiscount}{$content->memberdiscount}{else}0{/if}%)', DELAY, 300, CLICKCLOSE, true)"/></label>
									{$content->form.memberdiscount.html}
							</div>
						</div>
						<br>
						<h4>Meta Tags </h4>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{$content->form.meta_title.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover='Tip("Title tags should be a maximum of 70 characters long, including spaces. The text entered here will alter the &lt; title &gt; tag.", DELAY, 300, CLICKCLOSE, true)'/></label>
									{$content->form.meta_title.html}
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>{$content->form.meta_description.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('The description should optimally be between 150-160 characters. This text will be used as description meta information.', DELAY, 300, CLICKCLOSE, true)"/></label>
									{$content->form.meta_description.html}
							</div>
						</div>
						<br>
						{if $content->stocklevels}

							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>{$content->form.stock.label}</label>
									{$content->form.stock.html}
								</div>
							</div>
						{/if}	
					</div>

					<div id="shipping" style="display:block;">
						<table class="table_with_padding"  width="100%" border="0" cellpadding="2" cellspacing="2">

							{if $content->shippingratesholder|@count }
								<tr><td colspan="2"><br><h2>Product Shipping Costs</h2></td></tr>
										{/if}
										{foreach from=$content->shippingratesholder item=shippingrate}
								<tr>
									{foreach from=$shippingrate item=shippingitem}
										<td width="100">{$content->form.$shippingitem.label}</td><td>{$content->form.$shippingitem.html}</td>
										{/foreach}
								</tr>
							{/foreach}
						</table>
					</div>
					<br>
					<table class="table_with_padding"  width="100%" border="0" cellpadding="2" cellspacing="2">
						<tr><td>{$content->form.cancel.html}</td><td align="right">{$content->form.submit.html}</td></tr>
					</table>
				</form>
			</div>
		</div>

	</div>
</div>
					
{literal}
	<script tpye="text/javascript">
		
		function confirmDeleteProductImage() {
			if (confirm("Are you sure you want to delete the Product's image?")) {
				return true;
			} else {
				event.preventDefault();
			}
		}
		
		$(document).ready(function () {
			var product_id = {/literal}{if $content->product.id gt 0}{$content->product.id}{else}0{/if}{literal}
			var hasImage = {/literal}{if $content->product.hasimage}{$content->product.hasimage}{else}0{/if}{literal};
			var height = $("#logo_height").val();
			var width = $("#logo_width").val();
			var ratio = width / height;
			var defaultPreviewLogo = '<img src="/custom/store/images/full/11_full.jpg?' + Date.now() + '" alt="Product Image" style="width:100%">';


			var previewImage =  {/literal}'{$filepreviewdownload}'{literal};
			var previewImageExtension = previewImage.substr((previewImage.lastIndexOf('.') + 1));

			if ( hasImage ) {
				defaultPreviewLogo = '<img src="/custom/store/images/full/'+ product_id +'_full.jpg?' + Date.now() + '" alt="Product Image" style="width:100%">';
			}

			if(isImage(previewImageExtension)) {
				defaultDownloadPreviewLogo = '<img src="/custom/store/images/full/'+ product_id +'_fullpreview.jpg?' + Date.now() + '" alt="Preview Image" style="width:100%">';
			} else {
				defaultDownloadPreviewLogo = '';
			}

console.log(defaultDownloadPreviewLogo);			

			$('input[name="image"]').change(function (e) {
				var _URL = window.URL || window.webkitURL;
				var file, img;
				if ((file = this.files[0])) {
					img = new Image();
					img.onload = function () {
						$('input[name="image_h"]').val(this.height);
						$('input[name="image_w"]').val(this.width);
						$('.imagesize_h strong').text(this.height);
						$('.imagesize_w strong').text(this.width);
						height = this.height;
						width = this.width;
						ratio = width / height;
					};
					img.src = _URL.createObjectURL(file);
				}

			});

			//Scripts for new uploader
			$("#avatar-1").fileinput({
				overwriteInitial: true,
				maxFileSize: 10480000,
				showClose: false,
				showCaption: false,
				browseLabel: 'Upload File',
				removeLabel: '',
				browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
				removeIcon: '',
				removeTitle: '',
				elErrorContainer: '#kv-avatar-errors-1',
				msgErrorClass: 'alert alert-block alert-danger',
				defaultPreviewContent: defaultPreviewLogo,
				layoutTemplates: {main2: '{preview} {browse}'},
				allowedFileExtensions: ["jpeg", "jpg", "png", "gif"]
			});

			$("#downloaddata").fileinput({
				overwriteInitial: true,
				maxFileSize: 10480000,
				showClose: false,
				showCaption: false,
				browseLabel: 'Upload File',
				removeLabel: '',
				browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
				removeIcon: '',
				removeTitle: '',
				elErrorContainer: '#kv-avatar-errors-2',
				msgErrorClass: 'alert alert-block alert-danger',
				// defaultPreviewContent: defaultPreviewLogo,
				layoutTemplates: {main2: '{preview} {browse}'}
				// allowedFileExtensions: ["jpg", "png", "gif"]
			});

			$("#downloadpreview").fileinput({
				overwriteInitial: true,
				maxFileSize: 10480000,
				showClose: false,
				showCaption: false,
				browseLabel: 'Upload File',
				removeLabel: '',
				browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
				removeIcon: '',
				removeTitle: '',
				elErrorContainer: '#kv-avatar-errors-3',
				msgErrorClass: 'alert alert-block alert-danger',
				defaultPreviewContent: defaultDownloadPreviewLogo,
				layoutTemplates: {main2: '{preview} {browse}'}
				//allowedFileExtensions: ["jpg", "png", "gif"]
			});

			//Constrain Propertions

			$("#logo_width").focusout(function () {
				if ($('#contrain_proportions').is(":checked")) {
					$("#logo_height").val(Math.round((height / width) * $("#logo_width").val()));
				}
			});

			$("#logo_height").focusout(function () {
				if ($('#contrain_proportions').is(":checked")) {
					$("#logo_width").val(Math.round((width / height) * $("#logo_height").val()));
				}
			});

			$("#contrain_proportions").change(function () {
				if ($("#contrain_proportions").is(":checked")) {
					$("#logo_height").val(Math.round((height / width) * $("#logo_width").val()));
				}
			});


		function isImage(filename) {
			switch (filename.toLowerCase()) {
				case 'jpg':
				case 'gif':
				case 'png':
				case 'jpeg':
					return true;
			}
			return false;
    	}

	});
	</script>
{/literal}
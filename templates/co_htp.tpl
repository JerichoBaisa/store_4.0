
<form {$content->form.attributes}>
   {$content->form.hidden}
   <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12 offset-sm-0 col-xs-12 offset-xs-0">
      <div class="row">
         <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 mb-5">
            
            
            {if $content->settings.htp_header}
               <h1>
                  {$content->settings.htp_header}
               </h1>
            {/if}

            {if $content->settings.htp_body}   
               <p class="mb-4">
                  {$content->settings.htp_body}
               </p>
            {/if}
            
            {if $content->form.paymentprovider.label}
               <h5 class="font-weight-bold mb-4">
                  {$content->form.paymentprovider.label}
               </h5>
            {/if}
            
            {if $content->settings.num_of_providers NEQ '0'}
               <div class="row">
                  
                  <div div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                     {$content->form.paymentprovider.html}
                  </div>

                  {if $content->form.paymentprovider.error}
                     <div div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        {$content->form.paymentprovider.error}
                     </div>
                  {/if}
               
                  <div div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     {$content->form.gonext.html}
                  </div>
               </div>
            {/if}
         </div>

         <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
            <div id="summary" class="bordered-box">
               <h5 class="mb-4 font-weight-bold">Order Summary</h5>
               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0">
                        Subtotal 
                     </p>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol} 
                        {if $content->store->currency eq "JPY"}
                           {$carttotals.subtotal|string_format:"%d"}
                        {else}
                           {$carttotals.subtotal}
                        {/if}
                     </p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0">
                        Shipping Fees
                     </p>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol} 
                        {if $content->store->currency eq "JPY"}
                           {$carttotals.shippingcost|string_format:"%d"}
                        {else}
                           {$carttotals.shippingcost}
                        {/if}
                     </p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0">
                        Tax
                     </p>
                     </div>
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0 font-weight-bold">
                        {$content->store->currencysymbol} 
                        {if $content->store->currency eq "JPY"}
                           {$carttotals.totaltax|string_format:"%d"}
                        {else}
                           {$carttotals.totaltax}
                        {/if}
                     </p>
                  </div>
               </div>

               <hr>

               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0">
                        Total
                     </p>
                  </div>
                  <div class="col-lg-6 col-md-6">
                     <p class="m-0 font-weight-bold">
                         {$content->store->currencysymbol} 
                         {if $content->store->currency eq "JPY"}
                           {$carttotals.total|string_format:"%d"}
                         {else}
                           {$carttotals.total}
                         {/if}
                        
                     </p>	
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

</form>

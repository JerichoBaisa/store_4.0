

<br>
<h1>Store Order #{$content->order.id} - {$content->order.status}</h1>
<br>

{if $content->carttotals.total|floatval > 0}

<p><b>Address Details</b></p>
<table cellpadding="2" cellspacing="2" border="0" width="100%">
	<tr><td><b>Billing Details</b></td>{if not $content->downloadonly}<td><b>Shipping Details</b></td>{/if}</tr>
	<tr>
		<td>{$content->order.details.billing_title}&nbsp;{$content->order.details.billing_name_first}&nbsp;{$content->order.details.billing_name_last}</td>
		{if not $content->downloadonly}<td>{$content->order.details.shipping_title}&nbsp;{$content->order.details.shipping_name_first}&nbsp;{$content->order.details.shipping_name_last}</td>{/if}
	</tr>
	<tr><td>{$content->order.details.billing_address}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_address}</td></tr>{/if}
	<tr><td>{$content->order.details.billing_city}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_city}</td></tr>{/if}
	<tr><td>{$content->order.details.billing_zip}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_zip}</td></tr>{/if}
	<tr><td>{$content->order.details.billing_state}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_state}</td></tr>{/if}
	<tr><td>{$content->order.details.billing_country}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_country}</td></tr>{/if}
	<tr><td>{$content->order.details.billing_phone}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_phone}</td></tr>{/if}
	<tr><td>{$content->order.details.billing_email}</td>{if not $content->downloadonly}<td></td></tr>{/if}
</table>
<br>

{else} 

	<table cellpadding="2" cellspacing="2" border="0" width="100%">
	<tr><td><b>Member Details</b></td></tr>
	<tr>
		<td>{$content->order.details.billing_title}&nbsp;{$content->order.details.billing_name_first}&nbsp;{$content->order.details.billing_name_last}</td>
		{if not $content->downloadonly}<td>{$content->order.details.shipping_title}&nbsp;{$content->order.details.shipping_name_first}&nbsp;{$content->order.details.shipping_name_last}</td>{/if}
	</tr>
	<tr><td>{$content->order.details.billing_email}</td>{if not $content->downloadonly}<td></td></tr>{/if}
	<tr><td>{$content->order.details.billing_address}</td>{if not $content->downloadonly}<td>{$content->order.details.shipping_address}</td></tr>{/if}
</table>
<br>

{/if}
<p><b>Order Details</b></p>
{include file='staticcart2.tpl'}
<br>
{* <strong><a href="/{$dispatcher.store}/orders/">Back to orders</a><strong> *}


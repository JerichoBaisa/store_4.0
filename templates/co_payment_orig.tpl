<div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12 offset-sm-0 col-xs-12 offset-xs-0">
    {if $content->settings.payment_header}
        <h1>{$content->settings.payment_header}</h1>
    {/if}

    {if $content->settings.payment_body}
        <p>{$content->settings.payment_body}</p>
    {/if}

    {if $content->message}
        {foreach from=$content->message item=reg_message}
            <div class="alert alert-success" role="alert">{$reg_message}</div>
        {/foreach}
    {/if}

    {if $content->errors}
        {$content->errors}
    {/if}

    
    <div class="row mt-5">
      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 mb-4">
        <h5 class="mb-4 font-weight-bold">Coupon Code</h5>
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <form class="mb-5" method="post">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-2">
                  <div class="form-group">
                    {$content->form.coupon_code.html} 
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                  <div class="form-group">
                    {$content->form.addcoupon.html}
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-6 col-md-8 col-xs-12">

            <form {$content->form.attributes}>
                {$content->form.hidden}
        
                {if $content->provider->verify_target}
                    <iframe src="{$content->provider->verify_target}" width="100%" height="500" scrolling="auto" frameborder="0">
                        [Your user agent does not support frames or is currently configured not to display frames. However, you may visit
                        <a href="{$content->provider->verify_target}">the related document.</a>]
                    </iframe>
                {else}
        
                  {foreach from=$content->additional_elements key=k item=v}
                  
                      {if $k NEQ '#'}
                        <h5 class="mb-4 font-weight-bold">{$k}</h5>
                      {/if}
      
                      {foreach item=element from=$v}
                          <!-- elements with alternative layout in external template file-->
                          {if $content->form.$element.style}
                              <div class="form-group">
                                  {include file="smarty-dynamic-`$content->form.$element.style`.tpl"} 
                              </div>
                              
                          <!-- submit or reset button (don't display on frozen forms) -->
                          {elseif $content->form.$element.type eq "submit" or $content->form.$element.type eq "reset"}
                              {if not $content->form.frozen}
                                  <div class="form-group">
                                      <label>To pay with PayPal please click the button below once.</label>
                                      {$content->form.$element.html}
                                  </div>
                              {/if} 
                          
                          {elseif $content->form.$element.type eq "hidden" }
      
                            {$content->form.$element.html}
                          {/if}
                      {/foreach}   
                  {/foreach}
                {/if}
                <input type="hidden" name="csrf_token" id="csrf_token" value="{$token}">
                {if $smarty.session.store.paymentprovider neq 'paypal'}
                  <div class="form-group">
                    <label class="options-heading text-left mx-0 mt-0 mb-2" for="cardholder-name">Card Holder Name</label>
                    <input id="cardholder-name"  class="form-control" type="text" value="{$smarty.session.idev_cart.billing_name_first} {$smarty.session.idev_cart.billing_name_last}" autocomplete="name">
                  </div>
                  
                  <div class="form-group">
                    <label class="options-heading text-left mx-0 mt-0 mb-2" for="cardholder-email" >Card Holder Email</label>
                    <input id="cardholder-email"  class="form-control" type="text" value="{$smarty.session.idev_cart.billing_email}">
                  </div>
                
                  <!-- card form will load here-->
                  <div class="form-group">
                    <label class="options-heading text-left mx-0 mt-0 mb-2" for="card-container"  >Card</label>
                    <div id="card-container"></div>
                    <p id="card-errors" class="text-danger font-weight-bold"></p>
                  </div>
        
                  <span class="token"></span>
                  
                  <div class="w-100">
                    <button type="submit" id="card-button"  class="action-button btn btn-primary" data-secret="{$content->provider->paymentIntent}">Place Order</button>
                  </div>   
                {/if}
                <div id="confirm_progress_holder" style="display: none;">
                    <div id="progress_popup_box">
                        {* <h2 class="fs-title">Validating Card...</h2> *}
                        <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                    </div>
                </div> 
                {* <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        {$content->form.goback.html}
                        {$content->form.gonext.html}
                    </div>
                </div> *}
        
            </form>
          </div>
        </div>
      </div>

      {$content->staticCart}
    </div>

</div>
{literal}
<style>
#confirm_progress_holder .fa {
    font-size: 60px;
    margin-top: 15px;
    color: #ff7e46
}
#confirm_progress_holder {
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    padding: 0;
    text-align: center;
    background: #fff;
    position: fixed;
    z-index: 99
}
</style>
{/literal}
{if $settings->facebook_pixel_id neq ''}
{literal}<script>{/literal}fbq('track', 'AddPaymentInfo');{literal}</script>{/literal}
{/if}
<script> 
var pubKey = "{$stripe_publishable_key}"
</script>
{literal}
<script>
  var stripe = Stripe(pubKey);
  var elements = stripe.elements();
  var style = {
    base: {
      iconColor: "#ccc",
      color: "#000",
      fontWeight: 400,
      fontFamily: "Helvetica Neue, Helvetica, Arial, sans-serif",
      fontSize: "16px",
      fontSmoothing: "antialiased",

      "::placeholder": {
        color: "#BFAEF6"
      },
      ":-webkit-autofill": {
        color: "#fce883"
      }
    },
    invalid: {
      iconColor: "#FFC7EE",
      color: "#FFC7EE"
    }
  };

  var card = elements.create("card", {
    iconStyle: "solid",
    style: style
  });

  card.mount("#card-container");

  card.addEventListener('change', function (event) {
    var displayError = document.getElementById('card-errors');
    console.log("displayError ", displayError);
    if (event.error) {
      displayError.textContent = event.error.message;
    } else {
      displayError.textContent = '';
    }
  });


  // Create a token or display an error when the form is submitted.
  var form = document.getElementById('form');
  form.addEventListener('submit', function (event) {
    
    sessionStorage.removeItem('current_fieldset_index');
 
    event.preventDefault();

    
    var errorElement = document.getElementById('card-errors');
    var name = form.querySelector('#cardholder-name');
    var email = form.querySelector('#cardholder-email');
    var theToken = form.querySelector('#csrf_token').value.trim();


  document.getElementById('confirm_progress_holder').setAttribute("style", "display:block;"); 

    if (name.value.trim() === '') {
      errorElement.textContent = 'Card holder name is required.';
      //$('.progress_holder, #progress_popup_box, .wait').css('display', 'none');
      return false;
    }

    stripe.createPaymentMethod('card', card, {
      billing_details: {
        name: name.value.trim(),
        email: email.value.trim()
      }
    }).then(function (result) {

      console.log('Create payment method logging the result => ', result);

      if (result.error) {
        console.log('error ', result.error.message);
        errorElement.textContent = result.error.message;
        //$('.progress_holder, #progress_popup_box, .wait').css('display', 'none');
        // Show error in payment form
      }
      else {
        //$('.progress_holder, #progress_popup_box, .wait').css('display', 'none');
        // Otherwise send paymentMethod.id to your server (see Step 2)
        

        fetch('/store/checkout/payment', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ payment_method_id:result.paymentMethod.id, token:theToken,collectpayment:''})
        }).then(function (result) {
          console.log('Handle server response (see Step 3)');
          // Handle server response (see Step 3)
          result.json().then(function (json) {

            console.log('result json1 ', json);
            var paymentError = false;
            if (json.error) {
              
              Object.keys(json.error).forEach(key => {
                console.log("key => " + key);
                //jsonBody object is an error object from stripe
                if (key.toLowerCase() == 'jsonbody') {
                  var jsonError = json.error[key].error;
                  //console.log("value of current key =>  "+jsonError); // the value of the current key.
                  //console.log(typeof jsonError);
                  Object.keys(jsonError).forEach(jsonBodyErrKey => {
                    console.log("jsonBodyErrKey  =>  " + jsonBodyErrKey.toLowerCase());
                    console.log("value of jsonBodyErrKey  =>  " + jsonError[jsonBodyErrKey]);
                    //display the message
                    if (jsonBodyErrKey.toLowerCase() === 'message') {
                      alert(jsonError[jsonBodyErrKey]);
                      errorElement.textContent = jsonError[jsonBodyErrKey];
                      document.getElementById('confirm_progress_holder').setAttribute("style", "display:none;"); 
                      paymentError = true;
                    }
                  })
                }
              });
            }

            if (paymentError != true) {
              //console.log(" not jsonBODY ");
              if (json.error) {
                errorElement.textContent = json.error;
                document.getElementById('confirm_progress_holder').setAttribute("style", "display:none;"); 
              }
              else {
                //console.log('call handleServerResponse ');
                if (json.success === true) {
                  var url = window.location.protocol + '//' + window.location.hostname + '/store/checkout/complete';
                  window.location = url;
                }
                else {
                  result = handleServerResponse(json);
                }
              }
            }


          })
        });
      }
    });

  });

  //for recurring automatic processing
  function handleServerResponse(response) {
    console.log('inside handleServerResponse ', response.requires_action);
    
    if (response.error) {
      // Show error from server on payment form
      document.getElementById('card-errors').textContent = response.error;
    }
    else if (response.requires_action) {
      if (response.confirmation_method == 'manual') {
        confirmServerResponse(response);
      }
      else if (response.confirmation_method == 'automatic') {
        stripe.handleCardPayment(response.payment_intent_client_secret)
          .then(function (response) {
            if (response.error) {
              // Handle error here
              document.getElementById('card-errors').textContent = response.error.message;
              document.getElementById('confirm_progress_holder').setAttribute("style", "display:none;"); 


            } else if (response.paymentIntent && response.paymentIntent.status === 'succeeded') {
              // Handle successful payment here
              console.log('handleCardPayment success ', response);
              // Show success message 
              var url = window.location.protocol + '//' + window.location.hostname + '/store/checkout/complete';
              console.log('handleCardPayment success url ', url);
              window.location = url;
            }
            else {
              console.log('handleCardPayment failed ', response);
            }
          });
      }
      //plan have free days - offsite payment
      else { //response.confirmation_method === null
        console.log('confirmation method ', response.confirmation_method);
        stripe.handleCardSetup(response.payment_intent_client_secret,
          {
            payment_method: response.payment_method,
          }
        ).then(function (result) {
          console.log('handleCardSetup success ', result);
          if (result.error) {
            // Handle result.error or result.setupIntent
            document.getElementById('card-errors').textContent = result.error.message;
            //$('.progress_holder, #progress_popup_box, .wait').css('display', 'none');
          }
          else if (result.setupIntent && result.setupIntent.status === 'succeeded') {
            var url = window.location.protocol + '//' + window.location.hostname + '/store/checkout/complete';
            window.location = url;
          }
          else {
            document.getElementById('card-errors').textContent = result.error.message;;
          }
        });
      }

    }
  }


  //manual processing
  function confirmServerResponse(response) {
    var theToken = document.getElementById('csrf_token').value.trim();
    if (response.error) {
      // Show error from server on payment form
      document.getElementById('card-errors').textContent = response.error;
    }
    else if (response.requires_action) {

      // Use Stripe.js to handle required card action for non recurring
      stripe.handleCardAction(
        response.payment_intent_client_secret
      ).then(function (result) {

        console.log('inside handle card action => ', result);

        if (result.error) {
          // Show error in payment form
          document.getElementById('card-errors').textContent = result.error.message;
          document.getElementById('confirm_progress_holder').setAttribute("style", "display:none;"); 

        }
        else {
          // The card action has been handled
          // The PaymentIntent can be confirmed again on the server
          fetch('/store/checkout/payment', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ payment_intent_id: result.paymentIntent.id, token: theToken })

          }).then(function (confirmResult) {

            console.log(' value of  confirmResult => ', confirmResult);

            var result = confirmResult.json();

            if (confirmResult.status == '500') {
              document.getElementById('card-errors').textContent = result.error.message;
              document.getElementById('confirm_progress_holder').setAttribute("style", "display:none;"); 
            }

            console.log('inside handle card action confirmResult json => ', result);
            if (confirmResult.status == 200 && confirmResult.statusText == 'OK') {
              var url = window.location.protocol + '//' + window.location.hostname + '/store/checkout/complete';
              window.location = url;
            }

            return result;
          }).then(handleServerResponse);
        }
      });
    }
    else {
      // Show success message 
      //console.log(' Show success message  ');
      var url = window.location.protocol + '//' + window.location.hostname + '/store/checkout/complete';
      window.location = url;
    }
  }  
</script>
{/literal}
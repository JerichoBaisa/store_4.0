<!--hywel-->
{literal}
<script language="javascript">
	function toggleCurrencyRounding()
	{
		if(document.getElementById('currencyrounding').style.display == "block")
		{
			document.getElementById('currencyrounding').style.display="none";
		}
		else
		{
			document.getElementById('currencyrounding').style.display="block"; 
		}
	}
	
	function addLoadEvent(func) 
	{
		var oldonload = window.onload;
		if (typeof window.onload != 'function') 
		{
			window.onload = func;
		} 
		else 
		{
			window.onload = function() 
			{
				if (oldonload) 
				{
					oldonload();
				}
				func();
			}
		}
	}
	
	
	addLoadEvent(function() 
	{
		if(document.form.multicurrencies.checked)
		{ 
			document.getElementById('currencyrounding').style.display="block"; 
		}

	})

  $(document).ready(function() {
    $('#trigger-save-store-config').on('click', function() {
      $("#save-store-config").click(); 
      return false;
    });

    $('select[name=country]').on('change', function(e) {
      countryStateTaxInit();
    });

    countryStateTaxInit();
  });

  function countryStateTaxInit() {
    var countrystatetax = $('select[name=country]').val().toLowerCase();
    if (countrystatetax === 'us') {
          $('#other-state-tax').css('display','block');
        } else {
          $('#other-state-tax').css('display','none');
          $('#otherstatetax').val('');
    }
  }
	
</script>
<script type="text/javascript" src="/js/tinymce/tinymce.min.js?{$smarty.now}"></script>
<script type="text/javascript" src="/js/tinymce/init.js?{$smarty.now}"></script>
{/literal}
<form {$content->form.attributes} class="form-horizontal">
{$content->form.hidden}
<br>
<div class="well">
    <h2>Store Setup</h2>
    <p>Manage your built-in store. Sell physical, digital products and pay-per-view products.</p>
    <a href="https://support.subhub.com/hc/en-us/articles/115004837703-Setting-Up-Your-Store" target="_blank" ><i class="fa fa-file-text" aria-hidden="true" style="margin-right: 5px;"></i>Support Guide</a><br />
    
</div>
<br>
<fieldset>
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
      <h3 class="panel-title">Storefront</h3>
    </div>
    <div class="panel-body">

      <div class="col-md-12">
        <div class="form-group">
            <label>
              {$content->form.title.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Your store title will appear on your main store page.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.title.html}
          </div>
          <div class="form-group">
            <label>
              {$content->form.welcometext.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('You can add additional purchase instructions.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.welcometext.html}
          </div>
      </div>

        <div class="col-md-12">
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
           <div class="form-group">
             <label>Product Categories</label><br />
             {$content->form.categories.html}
           </div>
           <div class="form-group">      
             {$content->form.showcategories.label}&nbsp;{$content->form.showcategories.html}
           </div>

           <div class="form-group">
            <label>{$content->form.shippingcharge.label}</label>
            {$content->form.shippingcharge.html}
          </div>

          <div class="form-group">
            <label><em>Save shipping method before configuring shipping rate</em></label><br />
             <input class="btn btn-primary" id="trigger-save-store-config" value="Save Shipping Method" type="submit">
           </div>
           <div class="form-group">
             {$content->form.shippingrates.html}
           </div>

         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
           
         </div>

       </div>

     </div>
     <p>&nbsp;</p>
      <div class="col-md-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group"><h2>Taxation</h2></div>
    
          <div class="form-group">
            <label>{$content->form.excludeshippingcosttax.label} &nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Select if products will be taxed.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {$content->form.excludeshippingcosttax.html}
          </div>

          <div class="form-group">
            <label>{$content->form.tax.label} <strong>%</strong> &nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Add your tax rate.',DELAY, 300, CLICKCLOSE,true)"/></label>
           {$content->form.tax.html}
            
          </div>
          <div class="form-group">
            <label>{$content->form.forceaddtax.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Decide when tax will be calculated.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {$content->form.forceaddtax.html}
          </div>
       
          <div class="form-group">
            <label>{$content->form.taxname.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the name of your tax.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {$content->form.taxname.html}
          </div>
        
          <div class="form-group">
            <label>{$content->form.state.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter your state. Select NON-US/Other if not resident in USA.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {$content->form.state.html}
          </div>

          <div class="form-group" id="other-state-tax" style="display:none;">
            <label>{$content->form.otherstatetax.label} <strong>%</strong> &nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter sales tax applied to non-residents.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {$content->form.otherstatetax.html}
          </div>

          <div class="form-group">
            <label>{if $content->form.title.error}<br>{/if}{$content->form.country.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Select your country from the drop down menu.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {if $content->form.country.error}<font color="#FF0000">{$content->form.country.error}</font><br>{/if}{$content->form.country.html}
          </div>

      </div>
          
      
           
    </div>
    <div class="col-md-12">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		  <div class="form-group"><h2>Currency</h2></div>
          <div class="form-group">
            <label>{$content->form.currency.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Set the store default currency.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {$content->form.currency.html}
          </div>
       
          <div class="form-group" style="display: none;">
            <label>{$content->form.multicurrencies.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Here you can select to use a multi currency store.\<BR>\ If you select this option people visiting your store can choose to \<BR>\shop in any of the currencies you have setup on your site.',DELAY, 300, CLICKCLOSE,true)"/></label>
            &nbsp;{$content->form.multicurrencies.html}
          </div>
		  <div class="form-group"><h2>Confirmation Email</h2></div>
          <div class="form-group">
            <label>{if $content->form.orderemail.error}<br>{/if}{$content->form.orderemail.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the email to receive notification of store orders.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {if $content->form.orderemail.error}<font color="#FF0000">{$content->form.orderemail.error}</font><br>{/if}{$content->form.orderemail.html}
          </div>
       
          <div class="form-group">
            <label>{if $content->form.fromemail.error}<br>{/if}{$content->form.fromemail.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the email that buyers can contact about their orders.',DELAY, 300, CLICKCLOSE,true)"/></label>
            {if $content->form.fromemail.error}<font color="#FF0000">{$content->form.fromemail.error}</font><br>{/if}{$content->form.fromemail.html}
          </div>
      
          <div class="form-group">
            <label>
              {$content->form.thankyouemailsubject.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the email subject title of the purchase confirmation.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.thankyouemailsubject.html}
          </div>
          
          <div class="form-group">
            <label>
              {$content->form.thankyouemail.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the purchase confirmation message sent to buyers.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            <br/>{$content->form.thankyouemail.html}
          </div>

          <div class="form-group"><h2>Checkout Order Page Message</h2></div>
          <div class="form-group">
            <label>
              {$content->form.checkoutmessage.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the screen message seen after the purchase is completed.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            <br/>{$content->form.checkoutmessage.html}
          </div>
      </div>
    </div>
          
          
    <div class="col-md-12">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group"><h2>Advanced Settings</h2></div>
          <div class="form-group">
            <label>
              {$content->form.stocklevels.label}<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Tick the box to enable stock levels to be set on your physical product pages. Sold out products will display as Unavailable',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            &nbsp;{$content->form.stocklevels.html}
          </div>
       
          <div class="form-group">
            <label>
              {$content->form.memberdiscount.label} <strong>%</strong><img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Here you can type in a discount which becomes available when members of your site login. \<BR>\ This will the display the non members price and the members price on each product.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.memberdiscount.html}
          </div>
          
            
          <div class="form-group">
            <label>
              {$content->form.productspp.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the number of products to display per store page.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.productspp.html}
          </div>

          <div class="form-group">
            <label>
              {$content->form.minimum_cart_total.label}
            </label>
            {$content->form.minimum_cart_total.html}
          </div>
          
          <div class="form-group">
            <label>
              {$content->form.enabled.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Set the status of your store.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.enabled.html}
          </div>

          <div class="form-group">
            <label>
              {$content->form.productlimitdays.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the number of days a digital product will be available to download after purchase.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.productlimitdays.html}
          </div>

          <div class="form-group">
            <label>
              {$content->form.downloadlimit.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Enter the number of times a buyer can download a digital purchase.',DELAY, 300, CLICKCLOSE,true)"/>
            </label>
            {$content->form.downloadlimit.html}
          </div>
           
      </div>
     
    </div>
    <p>    &nbsp;</p>
   <div class="col-md-4">
      {$content->form.submit.html}
    </div>


          
  </div>
</fieldset>
</form>



<script language="javascript" type="text/javascript" src="/js/jquery-ui.js"></script>
{literal}
	<style>
		.selArrow {
			padding-left: 3% !important;
		}
	</style>
{/literal}
<div class="well">
	<h2>Manage Products</h2>
	<p>Drag and drop the product listings to edit their order in the store.</p>

</div>
{if $member->admin == '1' || $member->admin == '3'}
	<p align="right">
		<b>
			<a href="/{$dispatcher.store}/addproduct" class="btn btn-small btn-success">Add</a>
			<a href="/{$dispatcher.store}/orders" class="btn btn-small btn-primary">Orders</a>
			<a href="/store" class="btn btn-small btn-primary">View Store</a>
            <a href="/emailmanager" class="btn btn-small btn-primary">Email Manager</a>
		</b>
	</p>
{/if}


<form method="POST" action="/{$dispatcher.store}/configure/manage_products">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
			<label>Display products in</label>
			<select id="filter" name="filter" onChange="this.form.submit()" class="form-control">
				<option value="0">All Categories</option>
				{foreach from=$content->categories item=cat}
					<option value="{$cat.id}"{if $cat.id==$content->currentCat} selected=true{/if}>{$cat.name}</option>
				{/foreach}
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6">
			<label>Search for</label>
			<input type="text" size="10" class="form-control" name="search" value="{$smarty.post.search}"/>
			<br>
			<input type="submit" value="Search" class="btn btn-small btn-primary"/>
		</div>
	</div>
</form>
{if $content->products|@count > 0}
	<form method="POST" action="/{$dispatcher.store}/deleteproducts">
		<div id="productTableArea" class="table-responsive">
			<table class="table table-striped table-hover" id="productTable"  cellpadding="0" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Delete</th>
						<th>Action</th>
						<th>Title</th>
						<th>Re-Order</th>
						<th>Type</th>
						<th>Viewable by</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{foreach from=$content->products item=product}
						<tr id={$product.id}>
							<td><input type="checkbox" name="delete[]" value="{$product.id}"></td>
							<td><a href="/store/products/{$product.id}/edit" class="btn btn-small btn-primary">Edit</a></td>
							<td><a href="/store/products/{$product.id}">{$product.name}</a>&nbsp;</td>
							<td class="selArrow" title="To reorder your products, click on the arrow, to drag and drop them into position.">
								<i class="fa fa-arrows-v" aria-hidden="true"></i>
							</td>
							<td>{$product.type}</td>
							<td>{$product.visibility}</td>
							<td>{$product.price}</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
		<input type="submit" class="btn btn-small btn-danger" name="dodelete" value="Delete Selected" onClick="return confirm('Whoa there! Are you sure you are okay to permanently delete this product?');"/>

	</form>
{/if}

<script type="text/javascript">
	{literal}

    function forceHelper(e, ui) {
        $(".productTable-selected").html("<td colspan=90%></td>");
        //$(".productTable-selected").show();
    }

    var fixHelper = function (e, ui) {
        //$(".productTable-selected").html("<td colspan=100%></td>");	
        ui.children().each(function () {
            $(this).width($(this).width());
            //$(this).height(10);
        });
        return ui;
    };


    $(function () {
        $("#productTable tbody").sortable({
            helper: fixHelper,
            tolerance: 'intersect',
            containment: '#productTableArea',
            placeholder: 'productTable-selected',
            //axis: 'y',
            change: function (e, ui) {
                forceHelper(e, ui);
            },
            update: function () {
                var serial = new Array();
                var filter = $("#filter").val();
                $("#productTable tr").each(function (i) {
                    serial[i] = this.id;
                });
                $.ajax({
                    url: "/store/configure/reorder_products",
                    type: "POST",
                    data: "serial=" + serial + "&filter=" + filter
                });
            }
        });

        //$("#productTable tbody").disableSelection();
    });

	{/literal}
</script>

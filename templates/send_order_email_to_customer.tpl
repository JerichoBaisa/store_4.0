<html>
<body>
<p><b>Your order from {$domain}</b></p>
<p>{$content->thankyouemail}</p>
<br>

{if $content->carttotals.total gt 0 }
<p><b>Address Details</b></p>
<table cellpadding='0' cellspacing='2' border='0' width='60%'>
	<tr><td><b>Billing Details</b></td>{$shipping_details}</tr>
	<tr>
		<td>{$content->storedetails.billing_title}&nbsp;{$content->storedetails.billing_name_first}&nbsp;{$content->storedetails.billing_name_last}</td>
		{$download_only}
	</tr>
	<tr><td>{$content->storedetails.billing_address}</td><td>{$shipping_address}</td></tr>
	<tr><td>{$content->storedetails.billing_city}</td><td>{$shipping_city}</td></tr>
	<tr><td>{$content->storedetails.billing_zip}</td><td>{$shipping_zip}</td></tr>
	<tr><td>{$content->storedetails.billing_state}</td><td>{$shipping_state}</td></tr>
	<tr><td>{$content->storedetails.billing_country}</td><td>{$shipping_country}</td></tr>
	<tr><td>{$content->storedetails.billing_phone}</td><td>{$shipping_phone}</td></tr>
	<tr><td>{$content->storedetails.billing_email}</td><td></td></tr>
</table>

{else}
<table cellpadding='0' cellspacing='2' border='0' width='60%'>
	<tr><td><b>Member Details</b></td></tr>
	<tr>
		<td>{$member->title}&nbsp;{$member->name_first}&nbsp;{$member->name_last}</td>
		{$download_only}
    </tr>
    <tr><td>{$member->email}</td></tr>
	<tr><td>{$member->address1}<td></tr>
</table>

{/if}

<p>&nbsp;</p>
<p><b>Order Summary</b></p>
{literal}
<style>
#shoppingcart {
  width: 100%;
  border-collapse: collapse;
  border-right: none;
  border-bottom: none;
}

#shoppingcart th {
  border: none;
  padding: 5px;
}
#shoppingcart td {
  border-bottom: 1px solid black;
  padding: 5px;
}
</style>
{/literal}
<table id='shoppingcart' width="{$cart_width}" cellpadding='4' cellspacing='2'>
	<tr>
		<td style='border-bottom: none; height: 30px;' bgcolor='#dddddd'></td>
		<td style='border-bottom: none;' bgcolor='#dddddd'>Product</td>
		<td style='border-bottom: none;' bgcolor='#dddddd'>Quantity</td>
		<td style='border-bottom: none;' bgcolor='#dddddd' align='center'>Price Each</td>
		<td style='border-bottom: none;' bgcolor='#dddddd' align='center'>Total {$tax}</td>
	</tr>
  {$cart_items}
  {$cart_discount}
	<tr>
		<td style='border-bottom: none;' colspan='1' rowspan={$rowspan}></td>
		<td style='border-bottom: none;' colspan='3'>Subtotal</td>
		<td style='border-bottom: none;' colspan='1' align='right'>          
    <b>{$cart_subtotal}<b>
    </td>
	</tr>
  {$cart_shipping}	
	{$total_tax}
	<tr>
		<td style='border-bottom: none;' colspan='3'>Total</td>
		<td style='border-bottom: none;' colspan='1' align='right'><b>{$cart_total}</td>		
	</tr>
	<tr>
		<td></td>
		<td style='border-bottom: none;' colspan='1'>Order Notes</td>
		<td style='border-bottom: none;' colspan='4' align='right'><b>{$content->storedetails.cart.ordernotes}</td>		
	</tr>
	</table>
	{$notices}
	<br>
	{$order_notes}
  <br><br>
</body>
</html>
{literal}
<script type="text/javascript" src="/js/tinymce/tinymce.min.js?{$smarty.now}"></script>
<script type="text/javascript" src="/js/tinymce/init.js?{$smarty.now}"></script>
<script src="//cdn.jsdelivr.net/npm/mediaelement@4.2.3/build/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/mediaelement@4.2.3/build/mediaelementplayer.min.css">
<script language="javascript">
	
	function showDownload()
	{
		document.getElementById('download').style.display="block";
		document.getElementById('article').style.display="none";
		document.getElementById('shipping').style.display="none";
	}
	
	function showProduct()
	{
		document.getElementById('download').style.display="none";
		document.getElementById('article').style.display="none";
		document.getElementById('shipping').style.display="block";
	}
	
	function showArticle()
	{
		document.getElementById('article').style.display="block";
		document.getElementById('download').style.display="none";
		document.getElementById('shipping').style.display="none";
	}
	
	function addLoadEvent(func) 
	{
		var oldonload = window.onload;
		if (typeof window.onload != 'function') 
		{
			window.onload = func;
		} 
		else 
		{
			window.onload = function() 
			{
				if (oldonload) 
				{
					oldonload();
				}
				func();
			}
		}
	}
	
	
	addLoadEvent(function() 
	{
		if(document.form.type[0].checked)
		{ 
			showProduct();
		}
		
		if(document.form.type[1].checked)
		{
			showDownload();
		}

		if(document.form.type[2].checked)
		{
			showArticle();
		}

	})
	
</script>
{/literal}

<br>
<div class="well">
<h2> Add Product</h2><br/>
<a href="https://support.subhub.com/hc/en-us/articles/115004837463-Create-a-Store-Download-Product" target="_blank" ><i class="fa fa-file-text" aria-hidden="true" style="margin-right: 5px;"></i>Support Guide : Create a Digital Product</a>
<br>
<a href="https://support.subhub.com/hc/en-us/articles/115004837583-Create-a-Store-Physical-Product" target="_blank" ><i class="fa fa-file-text" aria-hidden="true" style="margin-right: 5px;"></i>Support Guide : Create a Physical Product</a>
<br>
<a href="https://support.subhub.com/hc/en-us/articles/115004837663-Pay-Per-View" target="_blank" ><i class="fa fa-file-text" aria-hidden="true" style="margin-right: 5px;"></i>Support Guide : Create a Pay Per View Product</a>
</div>

<form {$content->form.attributes} class="form-horizontal">
{$content->form.hidden}
<div class="col-md-12">
    <div class="form-group">
        <label>
        {if $content->form.name.error}<br>{/if}{$content->form.name.label} <font color="#FF0000">*</font>
        </label>
        {if $content->form.name.error}<font color="#FF0000">{$content->form.name.error}</font><br>{/if}
        {$content->form.name.html}
    </div>
    <div class="form-group">
        <label>
        {if $content->form.categoryid.error}<br>{/if}{$content->form.categoryid.label}
        </label>
        {if $content->form.categoryid.error}<font color="#FF0000">{$content->form.categoryid.error}</font><br>{/if}
			{$content->form.categoryid.html}
    </div>
    <div class="form-group">
       <label>
       {$content->form.visibility.label}
       </label>
       {$content->form.visibility.html}
    </div>
    <div class="form-group">
        <label>Type</label><br />
        {$content->form.type.html}
    </div>
    
    <div id="download" style="display:none;" class="form-group">
       <label>
       {$content->form.downloaddata.label} <font color="#FF0000">*</font>
       </label>
       {$content->form.downloaddata.html} 
       <br />
       <label>{$content->form.downloadpreview.label}</label>
       {$content->form.downloadpreview.html}
    </div>    
    <div id="article" style="display:none;" class="form-group">
        <label>{$content->form.articleid.label}</label>
        {$content->form.articleid.html}
        <br />
        <label>{$content->form.accesstime.label}</label>
        {$content->form.accesstime.html} <label>days (0 for unlimited)</label>
        <br /><br />
        <label>{$content->form.accesslimit.label} </label>
        {$content->form.accesslimit.html} <label>times (0 for unlimited)</label>
    </div>
    <div class="form-group">
        <label>{$content->form.teaser.label}</label>
        {$content->form.teaser.html}
    </div>
    <div class="form-group">
        <label>{$content->form.description.label}</label>
        {$content->form.description.html}
    </div>
    <div class="form-group">
        <label>{$content->form.image.label}</label>
        {$content->form.image.html}
    </div>
    <div class="form-group">
        <label>
            {if $content->form.price.error}<br>{/if}{$content->form.price.label}
            {if $content->form.price.error}<font color="#FF0000">{$content->form.price.error}</font><br>{/if}
        </label>
        {$content->form.price.html}
    </div>
    <div class="form-group">
        <label>{$content->form.taxexempt.label}</label>
        {$content->form.taxexempt.html}
    </div>
    <div class="form-group">
        <label>
            {$content->form.memberdiscount.label} (%) &nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('Leave this field blank to use the <br/>Default Member Discount (currently {if $content->memberdiscount}{$content->memberdiscount}{else}0{/if}%)',DELAY, 300, CLICKCLOSE,true)"/>
        </label>
        {$content->form.memberdiscount.html}
    </div>

     <div class="form-group">
        <div class="dividingHeader">Add Meta Tags </div>
    </div>
    <div class="form-group">
        <label>
            {$content->form.meta_title.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover='Tip("Title tags should be a maximum of 70 characters long, including spaces. The text entered here will alter the &lt; title &gt; tag.", DELAY, 300, CLICKCLOSE, true)'/></label>
        </label>
        {$content->form.meta_title.html}
    </div>

     <div class="form-group">
        <label>
            {$content->form.meta_description.label}&nbsp;<img src="/images/admin_icons/questionmark.png" onmouseover="Tip('The description should optimally be between 150-160 characters. This text will be used as description meta information.', DELAY, 300, CLICKCLOSE, true)"/></label>
        </label>
        {$content->form.meta_description.html}
    </div>

    {if $content->stocklevels}
    <div class="form-group">
        <label>{$content->form.stock.label}</label>
        {$content->form.stock.html}
    </div>
    {/if}
    <div id="shipping" style="display:block;" class="form-group">
    {if $content->shippingratesholder|@count }
        <h2>Product Shipping Costs</h2>
        {foreach from=$content->shippingratesholder item=shippingrate}
			{foreach from=$shippingrate item=shippingitem}
                <label>{$content->form.$shippingitem.label}</label>
                {$content->form.$shippingitem.html}
			{/foreach}
		{/foreach}
    {/if}
    </div>
    <div class="form-group">
         {$content->form.cancel.html} &nbsp;&nbsp;{if $settings->store_limit GT $productstoreplanlimit || $settings->store_limit EQ ''  }{$content->form.submit.html }{/if}
    </div>
     
</div>



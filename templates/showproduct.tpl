<script type="text/javascript" src="/js/ekko-lightbox.min.js"></script>
{literal}
<script>
  $(function(){
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox();
    });
  });
</script>
{/literal}

{if $content->product}
	<div class="container-fluid store-product">
		<div class="row">
			<div class="col-lg-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb bg-transparent px-0">
						<li class="breadcrumb-item">
							<a href="/{$dispatcher.store}/">All Products</a>
						</li>
						{foreach from=$content->categoryhistory item=category}
							<li class="breadcrumb-item">
								<a href="/{$dispatcher.store}/{$category.id}">{$category.name}</a>
							</li>
						{/foreach}
						<li class="breadcrumb-item">
							<a href="/{$dispatcher.store}/products/{$content->product.id}">{$content->product.name}</a>
						</li>
					</ol>
				</nav>
			</div>
			
			<div class="col-lg-12 text-right">
				{if $member->admin == '1' || $member->admin == '3'}
					<a href="/{$dispatcher.store}/products/{$content->product.id}/edit" class="btn btn-primary my-1 mr-2" role="button" >Edit</a>
					<a href="/{$dispatcher.store}/products/{$content->product.id}/delete" class="btn btn-primary my-1" role="button" >Delete</a>
				{/if}
			</div>
		
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
				<div id="productimage">
					{if $content->product.hasimage eq '0'}
						<a href="/custom/{$dispatcher.store}/images/full/11_full.jpg?{$smarty.now}" data-toggle="lightbox" title="{$content->product.name}">
							<img src="/custom/{$dispatcher.store}/images/full/11_full.jpg?{$smarty.now}" class="w-100 mb-2 rounded">
						</a>
					{elseif $content->product.haslargeimage eq '1'}
						<a href="/custom/{$dispatcher.store}/images/full/{$content->product.id}_large.jpg?{$smarty.now}" data-toggle="lightbox" title="{$content->product.name}">
							<img src="/custom/{$dispatcher.store}/images/full/{$content->product.id}_full.jpg?{$smarty.now}" class="w-100 mb-2 rounded">
						</a>
					{elseif $content->product.hasimage eq '1'}
						<a href="/custom/{$dispatcher.store}/images/full/{$content->product.id}_full.jpg?{$smarty.now}" data-toggle="lightbox" title="{$content->product.name}">
							<img src="/custom/{$dispatcher.store}/images/full/{$content->product.id}_full.jpg?{$smarty.now}" class="w-100 mb-2 rounded">
						</a>
					{/if}
				</div>
			</div>
		
			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
				<h1 class="product-header">{$content->product.name}</h1>
				<form {$content->form.attributes}>
					<h5 class="product-price-holder">
						{if $content->product.taxexempt || !$content->forceaddtax}
							{if $member}
								{if $content->product.memberdiscount eq 0 && $content->product.memberdiscount neq ''}
									{$content->currencysymbol}
									{if $content->currency eq "JPY"}
										{$content->product.price|string_format:"%d"} 
									{else}
										{$content->product.price format="%.2f"} 
									{/if}
								{elseif $content->product.memberdiscount gt 0}
									{$content->currencysymbol}
									{if $content->currency eq "JPY"}
										{math equation="x - (x /100 * y)" x=$content->product.price y=$content->product.memberdiscount|string_format:"%d"} 
										<strike class="text-muted">{$content->currencysymbol}{$content->product.price|string_format:"%d"}</strike>
									{else}
										{math equation="x - (x /100 * y)" x=$content->product.price y=$content->product.memberdiscount format="%.2f"} 
										<strike class="text-muted">{$content->currencysymbol}{$content->product.price format="%.2f"}</strike>
									{/if}
								{elseif $content->memberdiscount gt 0}
									{$content->currencysymbol}
									{if $content->currency eq "JPY"}
										{math equation="x - (x /100 * y)" x=$content->product.price y=$content->memberdiscount|string_format:"%d"} 
										<strike class="text-muted">{$content->currencysymbol}{$content->product.price|string_format:"%d"}</strike>
									{else}
										{math equation="x - (x /100 * y)" x=$content->product.price y=$content->memberdiscount format="%.2f"} 
										<strike class="text-muted">{$content->currencysymbol}{$content->product.price format="%.2f"}</strike>
									{/if}
								{else}
									{$content->currencysymbol}
									{if $content->currency eq "JPY"}
										{$content->product.price|string_format:"%d"} 
									{else}
										{$content->product.price format="%.2f"} 
									{/if}
								{/if}
							{else}
								{if $content->currency eq "JPY"}
									{$content->currencysymbol}
									{$content->product.price|string_format:"%d"}
								{else}
									{$content->currencysymbol}
									{$content->product.price format="%.2f"}
								{/if}
							{/if}
						{else}


						{if $member}
							{if $content->product.memberdiscount eq 0 && $content->product.memberdiscount neq '' }
								{if $content->currency eq "JPY"}
									{$content->currencysymbol}{$content->product.price|string_format:"%d"}
								{else}
									{$content->currencysymbol}{$content->product.price format="%.2f"}
								{/if}
								<!-- {math equation="x + (x / 100 * y)" x=$content->product.price y=$content->tax format="%.2f"} -->
							{elseif $content->product.memberdiscount gt 0}
								{$content->currencysymbol}
								<!-- {math equation="(x - (x / 100 * z)) + ((x - (x / 100 * z)) / 100 * y)" x=$content->product.price y=$content->tax z=$content->product.memberdiscount format="%.2f"} -->
								<!-- <strike class="text-muted">{$content->currencysymbol}{math equation="x + (x / 100 * y)" x=$content->product.price y=$content->tax format="%.2f"}</strike> -->
								{if $content->currency eq "JPY"}
									{math equation="(x - (x / 100 * z))" x=$content->product.price z=$content->product.memberdiscount|string_format:"%d"}
									<strike class="text-muted">{$content->currencysymbol}{$content->product.price|string_format:"%d"}</strike>
								{else}
									{math equation="(x - (x / 100 * z))" x=$content->product.price z=$content->product.memberdiscount format="%.2f"}
									<strike class="text-muted">{$content->currencysymbol}{$content->product.price  format="%.2f"}</strike>
								{/if}
							{elseif $content->memberdiscount gt 0}
								{$content->currencysymbol}
								<!-- {math equation="(x - (x / 100 * z)) + ((x - (x / 100 * z)) / 100 * y)" x=$content->product.price y=$content->tax z=$content->memberdiscount format="%.2f"} -->
								<!-- <strike class="text-muted">{$content->currencysymbol}{math equation="x + (x / 100 * y)" x=$content->product.price y=$content->tax format="%.2f"}</strike> -->
								{if $content->currency eq "JPY"}
									{math equation="(x - (x / 100 * z))" x=$content->product.price z=$content->memberdiscount|string_format:"%d"}
									<strike class="text-muted">{$content->currencysymbol}{$content->product.price|string_format:"%d"}</strike>
								{else}
									{math equation="(x - (x / 100 * z))" x=$content->product.price z=$content->memberdiscount format="%.2f"}
									<strike class="text-muted">{$content->currencysymbol}{$content->product.price format="%.2f"}</strike>
								{/if}
							{else}
								{if $content->currency eq "JPY"}
									{$content->currencysymbol}{$content->product.price|string_format:"%d"}
								{else}
									{$content->currencysymbol}{$content->product.price format="%.2f"}
								{/if}
								<!-- {math equation="x + (x / 100 * y)" x=$content->product.price y=$content->tax format="%.2f"} -->
							{/if}
						{else}
							{if $content->currency eq "JPY"}
								{$content->currencysymbol}{math equation="x + (x / 100 * y)" x=$content->product.price y=$content->tax|string_format:"%d"}
							{else}
								{$content->currencysymbol}{math equation="x + (x / 100 * y)" x=$content->product.price y=$content->tax format="%.2f"}
							{/if}
						{/if}
					{/if}
					</h5>

					{if $content->tax gt 0 && $content->forceaddtax && !$content->product.taxexempt }

						<p class="tax-holder mb-0 text-muted font-italic">
						{if $member}
							{if $content->product.memberdiscount eq 0 && $content->product.memberdiscount neq ''}
								{if $content->currency eq "JPY"}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x / 100 * y)" x=$content->product.price y=$content->tax|string_format:"%d"}
								{else}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x / 100 * y)" x=$content->product.price y=$content->tax format="%.2f"}
								{/if}
							
								<!-- {if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{$content->product.price} -->
							{elseif $content->product.memberdiscount gt 0}

								{if $content->currency eq "JPY"}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x - (x /100 * y)) * (z / 100)" x=$content->product.price z=$content->tax y=$content->product.memberdiscount|string_format:"%d"}
								{else}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x - (x /100 * y)) * (z / 100)" x=$content->product.price z=$content->tax y=$content->product.memberdiscount format="%.2f"}
								{/if}
								
							{elseif $content->memberdiscount gt 0}

								{if $content->currency eq "JPY"}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x - (x /100 * y)) * (z / 100)" x=$content->product.price z=$content->tax y=$content->memberdiscount|string_format:"%d"}
								{else}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x - (x /100 * y)) * (z / 100)" x=$content->product.price z=$content->tax y=$content->memberdiscount format="%.2f"}
								{/if}
							{else}
								{if $content->currency eq "JPY"}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{$content->product.price|string_format:"%d"}
								{else}
									{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{$content->product.price format="%.2f"}
								{/if}
							{/if}
						{else}
							{if $content->currency eq "JPY"}
								{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x /100 * y)" x=$content->product.price y=$content->tax|string_format:"%d"}
							{else}
								{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x /100 * y)" x=$content->product.price y=$content->tax format="%.2f"}
							{/if}
						{/if}
						</p>
					{/if}
						
					<p>
						{if $content->stocklevels neq true || $content->product.type eq 'd' || $content->product.stock eq ''}
							<span style="color:#00AA00">Available</span>
						{elseif $content->product.stock neq 0 }
							<span style="color:#00AA00"><strong>{$content->product.stock}</strong> in stock</span>
						{else}
							<span  style="color:#DD0000">Out of stock</span>
						{/if}
					</p>
		
					<p class="add-to-cart-preview-buttons mb-0" style="margin-left: -.25rem;">
						{if $content->product.taxexempt || !$content->forceaddtax}
							{if $member}
								{if $content->product.memberdiscount eq 0 && $content->product.memberdiscount neq ''}
									{$content->form.addtocart.html}
								{elseif $content->product.memberdiscount gt 0}
									{$content->form.addtocart.html}
								{elseif $content->memberdiscount gt 0}
									{$content->form.addtocart.html}
								{else}
									{$content->form.addtocart.html}
								{/if}
							{else}
								{$content->form.addtocart.html}
							{/if}
						{else}
							{if $member}
								{if $content->product.memberdiscount eq 0 && $content->product.memberdiscount neq ''}
									{$content->form.addtocart.html}
								{elseif $content->product.memberdiscount gt 0}
									{$content->form.addtocart.html}
								{elseif $content->memberdiscount gt 0}
									{$content->form.addtocart.html}
								{else}
									{$content->form.addtocart.html}
								{/if}
							{else}
								{$content->form.addtocart.html}
							{/if}
						{/if}

						{if $content->haspreview}
							{$content->form.preview.html}
						{/if}
					</p>
				</form>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-4">
				<h5>Description</h5>
				{if $content->product.teaser}
					<p class="product-teaser">
						{$content->product.teaser}
					</p>
				{/if}
				
				{if $content->product.description}
					<p class="product-description">
						{$content->product.description}
					</p>
				{/if}
			</div>
			
			<div class="col-lg-12 mt-3 text-right">
				{if $content->fromcategory eq 'addproduct' || $content->fromcategory eq 'edit' || $content->fromcategory eq $dispatcher.store}
					<a href="/{$dispatcher.store}" class="btn btn-primary" role="button">Back to products</a>
				{else}
					<a href="/{$dispatcher.store}/{$content->fromcategory}" class="btn btn-primary" role="button">Back to products</a>
				{/if}
			</div>
		</div>
	</div>
{/if}	




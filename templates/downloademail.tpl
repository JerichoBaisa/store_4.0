<html>
<body>
{literal}
<style>
#shoppingcart {
  border-collapse: collapse;
  border-right: 1px solid black;
  border-bottom: 1px solid black;
}
#shoppingcart th {
  border: 1px solid black;
  padding: 2px;
}
#shoppingcart td {
  border-left: 1px solid black;
  border-top: 1px solid black;
  padding: 2px;
}
</style>
{/literal}
<p><b>Your digital purchases from {$content->domain}</b></p>

<table id="shoppingcart" cellpadding="2" cellspacing="2" width="60%">
<tr>
	<td>Product</td>
	<td>Download Link</td>
</tr>
{foreach from=$content->downloads item=download}
	<tr><td>{$download.product}</td><td><a href="{$download.url}">Click to Download</a></td></tr>
{/foreach}
</table>
<p>Download links are active for 24 hours from time of purchase and can be downloaded twice within that time.</p>
</body>
</html>

<br><br>
<h3>Sorry, the download code is invalid.</h3>
<br>
<p>Common reasons for this include:
<ul>
	<li>The download has been accessed too many times</li>
	<li>The download code has expired</li>
</ul>
<br>
<a href="/{$dispatcher.store}/"><h3>Return to the store</h3></a>


{if $smarty.const.SITE_NAME eq 'youniverse'}
    <p>Note: Download may not work If you're using an apple device.</p>
{/if}
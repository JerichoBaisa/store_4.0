<div class="col-lg-12">
	{if $content->message}
		{foreach from=$content->message item=reg_message}
			<div class="alert alert-success" role="alert">
				{$reg_message}
			</div>
		{/foreach}
	{/if}
	
	<h1 class="text-center mb-4">{if $content->store->checkoutmessage}{$content->store->checkoutmessage}{else}Thank you for your order.{/if}</h1>
	<h4 class="text-center mb-5">A confirmation email has been sent to you.</h4>
	
	{if $content->VendorTxCode}
		<p class="text-center mb-3">
			Your Transaction ID is <strong>{$content->VendorTxCode}</strong>
		</p>
	{/if}

	<div  class="text-center w-100">
		<a href="/{$dispatcher.store}/" class="btn btn-primary">Return to Store Home</a>
	</div>
</div>

<!--Tracking Code -->
{if isset($settings->tracking_code_store) && $settings->tracking_code_store neq ''}
	{$settings->tracking_code_store}
{/if}

<!--End Tracking Code -->
{if $settings->facebook_pixel_id neq ''}
	<!-- Facebook Event Purchase -->
	{literal}
		<script>{/literal}{$content->fbqPurchase}{literal}</script>
	{/literal}
{/if}

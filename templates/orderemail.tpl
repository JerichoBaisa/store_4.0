<html>
<body>
<p><b>New order from {$content->domain}</b></p>
<br>
<p><b>Address Details</b></p>
<table cellpadding="0" cellspacing="2" border="0" width="60%">
	<tr><td><b>Billing Details</b></td>{if not $content->digital}<td><b>Shipping Details</b></td>{/if}</tr>
	<tr>
		<td>{$content->storedetails.billing_title}&nbsp;{$content->storedetails.billing_name_first}&nbsp;{$content->storedetails.billing_name_last}</td>
		{if not $content->downloadonly}<td>{$content->storedetails.shipping_title}&nbsp;{$content->storedetails.shipping_name_first}&nbsp;{$content->storedetails.shipping_name_last}</td>{/if}
	</tr>
	<tr><td>{$content->storedetails.billing_address}</td>{if not $content->downloadonly}<td>{$content->storedetails.shipping_address}</td>{/if}</tr>
	<tr><td>{$content->storedetails.billing_city}</td>{if not $content->downloadonly}<td>{$content->storedetails.shipping_city}</td>{/if}</tr>
	<tr><td>{$content->storedetails.billing_zip}</td>{if not $content->downloadonly}<td>{$content->storedetails.shipping_zip}</td>{/if}</tr>
	<tr><td>{$content->storedetails.billing_state}</td>{if not $content->downloadonly}<td>{$content->storedetails.shipping_state}</td>{/if}</tr>
	<tr><td>{$content->storedetails.billing_country}</td>{if not $content->downloadonly}<td>{$content->storedetails.shipping_country}</td>{/if}</tr>
	<tr><td>{$content->storedetails.billing_phone}</td>{if not $content->downloadonly}<td>{$content->storedetails.shipping_phone}</td>{/if}</tr>
	<tr><td>{$content->storedetails.billing_email}</td>{if not $content->downloadonly}<td></td>{/if}</tr>
</table>

<p><b>Order Summary</b></p>
{include file='staticcart.tpl'}
<br><br>
</body>
</html>

<html>
<body>
{literal}
<style>
#shoppingcart {
  border-collapse: collapse;
  border-right: 1px solid black;
  border-bottom: 1px solid black;
}
#shoppingcart th {
  border: 1px solid black;
  padding: 2px;
}
#shoppingcart td {
  border-left: 1px solid black;
  border-top: 1px solid black;
  padding: 2px;
}
</style>
{/literal}
<p><b>Your digital purchases from {$domain}</b></p>

<table id="shoppingcart" cellpadding="2" cellspacing="2" width="60%">
<tr>
	<td>Product</td>
	<td>Download Link</td>
</tr>
{$content_downloads}
</table>
<p>{$downloadnotice}</p>
</body>
</html>
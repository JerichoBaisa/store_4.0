
<div class="col-md-12 text-left">
	<p>If you are sure, press Delete.  Otherwise, press Cancel to be returned to product.</p>
	<form {$content->form.attributes}>
		{$content->form.cancel.html}</td>
		{$content->form.delete.html}
	</form>
</div>
{literal}
<link href="/css/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script>
 $(function(){
	$(' #addcoupon').on('click', function(e) {
		 
		var control_id = $(this).attr("id");

		console.log('control_id=>',control_id);

		$.ajax({
			url:'/store/update-cart',
			data:{
			'value':$("form").serialize()
		},
		type:'POST',
		dataType: "json",
		success: function(json) {
			console.log(json);
			if(typeof json.message != 'undefined') {
				if (json.message != '') {
					$("input[name='coupon_code']").val('');
					$('#discount-container').addClass('d-none')
					alert(json.message);
					return false;
				}				 
			}

			if(typeof json.discount != 'undefined') {
				$('#discount-container').removeClass('d-none')
				$('#discount-percentage').text(json.discount+'%');

			}
			//console.log($('option:selected').attr("id"));
			//id = $(this).attr('id');
			
			// var price = parseInt (cart_price ) * parseInt(qty);
			//var price = parseFloat (cart_price ) * parseFloat(qty);
			//price = parseFloat( price ).toFixed(2);

		console.log('total discount =>',json.discount );
		console.log('total =>',json.total );
		console.log('total tax =>',json.totaltax );
		console.log('total subtotal =>',json.subtotal );
			
			 var subtotal = '-';
			if(typeof json.subtotal != 'undefined') {
				subtotal = parseFloat( json.subtotal ).toFixed(2);
			}
			
			var totaltax = '-';
			if(typeof json.totaltax != 'undefined') {
				totaltax = parseFloat( json.totaltax ).toFixed(2);
			}
			
			var shipping_cost = '-';
			if(typeof json.shippingcost != 'undefined') {
				shipping_cost = parseFloat(json.shippingcost).toFixed(2);
			}
			
			var total = '-';
			if(typeof json.total != 'undefined') {	
				total = parseFloat(json.total).toFixed(2);
			}
			
			//$('#itemtotal-'+id).text(price);
			//$('#cart-subtotal').text(subtotal);
			$('#cart-shipping-fees').text(shipping_cost);
			$('#totaltax').text(totaltax);
			$('#cart-total-tax').text(totaltax);
			$('#cart-total').text(total);
	
			return false;        
		},
		error: function() {
			$( "input[name='updatecart']" ).trigger( "click" );
		}
			
		});
	});

  $('.quantity, select[name=shippingrate]').on('change', function(e) {
      //console.log('quantity  changed ' );    
     
      var control_id = $(this).attr("id");

	  console.log('control_id=>',control_id);
      if (control_id != 'shippingrate') {
        
        var arr = control_id.split('_');
        var id = arr[1];

        var cart_price = $('#cart-price-'+id).text();
        var qty =  $(this).val();

        //console.log('id '+ id );     
        console.log('cart price '+ cart_price); 
        //console.log('quantityinput '+ qty); 
      }
      //    console.log('quantity  changed ' +$("form").serialize());     return false;
      $.ajax({
        url:'/store/update-cart',
        data:{
          'value':$("form").serialize()
        },
        type:'POST',
        dataType: "json",
        success: function(json) {
           console.log('jsonData',json);
          console.log($('option:selected').attr("id"));
          //id = $(this).attr('id');
          
          // var price = parseInt (cart_price ) * parseInt(qty);
		  var price = parseFloat (cart_price ) * parseFloat(qty);
          price = parseFloat( price ).toFixed(2);

		  console.log('price =>',price );
          
          var totaltax = '-';
          if (json.totaltax.length) {
            totaltax = parseFloat( json.totaltax ).toFixed(2);
          }
          
          var subtotal = '-';
          if (json.subtotal.length) {
            subtotal = parseFloat( json.subtotal ).toFixed(2);
          }
          
          var shipping_cost = '-';
          if (json.shippingcost.length) {
            shipping_cost = parseFloat(json.shippingcost).toFixed(2);
          }
          
          var total = '-';
          if (json.total.length) {
            total = parseFloat(json.total).toFixed(2);
          }
          
          $('#itemtotal-'+id).text(price);
          $('#cart-subtotal').text(subtotal);
          $('#cart-shipping-fees').text(shipping_cost);
          $('#totaltax').text(totaltax);
		  $('#cart-total-tax').text(totaltax);
          $('#cart-total').text(total);
 
          return false;        
        },
        error: function() {
          $( "input[name='updatecart']" ).trigger( "click" );
        }
        
      });
    
  });
});
</script>  
{/literal}


<div class="col-lg-12">
	<form {$content->form.attributes}>
		{$content->form.hidden}
		
		{if $content->cartitems|@count}
			<div class="row my-5">

				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 mb-5">
					<h1 class="mb-4">Cart</h1>
					
					
					<div class="row mb-3">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hide-on-mobile-lg">
							<p class="font-weight-bold">
								Item
							</p>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center hide-on-mobile-lg">
							<p class="font-weight-bold">
								Quantity
							</p>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center hide-on-mobile-lg">
							<p class="font-weight-bold">
								Price
							</p>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center hide-on-mobile-lg">
							<p class="font-weight-bold">
								Total
							</p>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center hide-on-mobile-lg">
							<p class="font-weight-bold">
								
							</p>
						</div>
					</div>

					{foreach from=$content->cartitems item=cartitem}
						<div class="row mb-3">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<p class="font-weight-bold mb-0 mt-3 d-none display-block-on-mobile">
									Item
								</p>
								<a href="/{$dispatcher.store}/products/{$cartitem.product.id}">{$cartitem.product.name}</a>	
							</div>
							
							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-lg-center text-md-center">
								<p class="font-weight-bold mb-0 mt-3 d-none display-block-on-mobile">
									Quantity
									{foreach from=$cartitem.quantityinput item=quantityinput}
										{if $cartitem.product.type eq 'd' || $cartitem.product.type eq 'a'}
											<font color='#FF0000' class="store-quatity position-relative">*</font>
										{/if}
									{/foreach}
								</p>
								{foreach from=$cartitem.quantityinput item=quantityinput}
									{$content->form.$quantityinput.html}
									{if $cartitem.product.type eq 'd'}
										<font color='#FF0000' class="store-quatity position-absolute hide-on-mobile-lg">*</font>
										{assign var='downloadnotice' value='1'}
									{elseif $cartitem.product.type eq 'a'}
										<font color='#FF0000' class="store-quatity position-absolute hide-on-mobile-lg">*</font>
										{assign var='articlenotice' value='1'}
									{/if}
								{/foreach}
							</div>

							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-lg-center text-md-center">
								<p class="font-weight-bold mb-0 mt-3 d-none display-block-on-mobile">
									Price
								</p>
								<span>{$content->currencysymbol}</span>								
								<span id="cart-price-{$cartitem.product.id}" class="mb-0">
									{if $content->currency eq "JPY"}
										{$cartitem.product.price|string_format:"%d"}
									{else}
										{$cartitem.product.price format="%.2f"}
									{/if}
								</span>
								
							</div>

							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-lg-center text-md-center">
								<p class="font-weight-bold mb-0 mt-3 d-none display-block-on-mobile">
									Total
								</p>
								<p class="mb-0">
									{$content->currencysymbol}
									<span id="itemtotal-{$cartitem.product.id}">
										{if $content->currency eq "JPY"}
											{math equation="x * y" x=$cartitem.product.price y=$cartitem.quantity|string_format:"%d"}
										{else}
											{math equation="x * y" x=$cartitem.product.price y=$cartitem.quantity format="%.2f"}
										{/if}
									</span>
								</p>
								<!-- <p class="mb-0 text-muted font-italic">
									{if $content->tax gt 0}
										{$content->taxname}
									{/if} 
								</p> -->
							</div>

							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-lg-center text-md-center">
								<p class="font-weight-bold mb-0 mt-3 d-none display-block-on-mobile"></p>
								{foreach from=$cartitem.removebutton item=removebutton}
									<button type="submit" name="{$removebutton}" class="btn btn-primary p-0 btn-custom-link rem-1-2" style="font-size:1.2rem;">
										<i class="fa fa-trash"></i>
									</button>
									{*{$content->form.$removebutton.html}*}
								{/foreach}
							</div>
						</div>
					{/foreach}

				
						
					<div class="row mt-5">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label for="ordernotes" class="ordernotes">{$content->form.ordernotes.label}</label>
								{$content->form.ordernotes.html}
								<small id="emailHelp" class="form-text text-muted text-danger">Max 50 characters</small>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							{$content->form.returntoproducts.html}
							{$content->form.updatecart.html}
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<div id="summary" class="bordered-box">
						<h5 class="mb-4 font-weight-bold">Order Summary</h5>

						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group mb-2">
									{* <h5 class="mb-4 font-weight-bold">Coupon Code</h5> *}
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-2">
											{$content->form.coupon_code.html}  
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
											{$content->form.addcoupon.html}
										</div>
											
									</div>
								</div>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-none" id="discount-container">
						
								<div class="row">
									<div class="col-lg-6 col-md-6">Coupon</div>
									
									<div class="col-lg-6 col-md-6">
										<p class="m-0 font-weight-bold">
											<span class="discount-type font-weight-bold"> </span>
											<span class="amount font-weight-bold" style="color: red;" id="discount-percentage"></span>
										</p>	
									</div>
								</div>
							</div>
						
						</div>

						<div class="row">
							<div class="col-lg-6 col-md-6">
								<p class="m-0">
									Subtotal 
								</p>
							</div>
							<div class="col-lg-6 col-md-6">
								<p class="m-0 font-weight-bold" >
									{$content->currencysymbol} 
									<span id="cart-subtotal"> 
										{if $content->currency eq "JPY"}
											{$content->carttotals.subtotal|string_format:"%d"}
										{else}
											{$content->carttotals.subtotal}
										{/if}</span>
								</p>
							</div>
						</div>
						 
						 
						{if $content->displayshippingrates}
						<hr>
							<div class="row">

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-group mb-2">
										<h5 class="mb-4 font-weight-bold">Shipping Method</h5>
										{$content->form.shippingrate.html}
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-lg-6 col-md-6">Shipping Fees</div>
										
										<div class="col-lg-6 col-md-6">
											<p class="m-0 font-weight-bold">
												<span class="currency font-weight-bold">{$content->currencysymbol}</span>
												<span class="amount font-weight-bold" id="cart-shipping-fees">
													{if $content->currency eq "JPY"}
														{$content->carttotals.shippingcost|string_format:"%d"}	
													{else}
														{$content->carttotals.shippingcost format="%.2f"}
													{/if}
												</span>
											</p>	
										</div>
									</div>
								</div>
							</div>
						{/if}

						{if $content->tax gt 0 && 1==2}
							<hr>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-group mb-2">
										<h5 class="mb-4 font-weight-bold">Tax</h5>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<p class="m-0">
										{if $content->taxname neq ''}
											{$content->taxname}
										{else}
											Tax
										{/if}
									</p>
								</div>
								
								<div class="col-lg-6 col-md-6">
									<p class="m-0 font-weight-bold">
										<span class="currency font-weight-bold">{$content->currencysymbol}</span>
										<span class="amount font-weight-bold" id="cart-total-tax">
											{if $content->carttotals.totaltax}
												{if $content->currency eq "JPY"}
													{$content->carttotals.totaltax|string_format:"%d"}
												{else}
													{$content->carttotals.totaltax format="%.2f"}
												{/if}
											{else}
												-
											{/if}
										</span>
									</p>	
								</div>
							</div>
						{/if}
						 
						<hr>
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<p class="m-0 font-weight-bold">
									Total
								</p>
							</div>
							
							<div class="col-lg-6 col-md-6">
								<p class="m-0 font-weight-bold">
									<span class="currency font-weight-bold">{$content->currencysymbol}</span> 
									<span class="amount font-weight-bold" id="cart-total">
										{if $content->currency eq "JPY"}
											{$content->carttotals.total|string_format:"%d"}
										{else}
											{$content->carttotals.total format="%.2f"}
										{/if}
									</span>
								</p>	
							</div>
							
							<div class="col-lg-12 col-md-12 mb-3">

								{if $downloadnotice || $articlenotice}
									<p class="mb-0">
										{if $downloadnotice}
											<small class="form-text text-danger">
												{$notices}
											</small>
										{/if}

										{if $articlenotice}
											<small class="form-text text-danger">
												*Articles can only be purchased in single quantities.
											</small>
										{/if}
									</p>
								{/if}
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-md-12">
								{$content->form.checkout.html}
							</div>							
						</div>
					</div>
				</div>
			</div>


		{else}
			<h3 class="text-center">Your shopping cart is empty</h3>
			<p class="text-center">{$content->form.returntoproducts.html}</p>
		{/if}

		
	</form>
</div>
{if $settings->facebook_pixel_id neq ''}
{literal}<script>{/literal}fbq('track', 'AddToCart');{literal}</script>{/literal}
{/if}
<div class="col-lg-12">
	<form {$content->form.attributes}>
		
		<div class="row mb-4">
			<div class="col-md-12">
				{if $content->currentcategory}
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb bg-transparent px-0">
							<li class="breadcrumb-item">
								<a href="/{$dispatcher.store}/">All Products</a>
							</li>
							{foreach from=$content->categoryhistory item=category}
								<li class="breadcrumb-item">
									<a href="/{$dispatcher.store}/{$category.id}">{$category.name}</a>
								</li>
							{/foreach}
						</ol>
					</nav>
				{/if}
			</div>
			{if $member->admin == '1' || $member->admin == '3'}
				<div class="col-md-12 text-right">

					{if $settings->store_limit GT $productstoreplanlimit || $settings->store_limit EQ ''}
						<a href="/{$dispatcher.store}/addproduct" class="btn btn-primary" role="button">Add</a>
					{/if}
					<a href="/{$dispatcher.store}/configure/manage_products" class="btn btn-primary my-2" role="button">Manage</a>
					<a href="/{$dispatcher.store}/configure" class="btn btn-primary my-2" role="button">Setup</a>
					<a href="/{$dispatcher.store}/orders" class="btn btn-primary my-2" role="button">Orders</a>
					<a href="/emailmanager" class="btn btn-primary my-2" role="button">Email Manager</a>

				</div>
			{/if}
			<!-- Store Setup Welcome Text   -->
			<div class="col-md-12" id="product-category">
				{if $dispatcher.category && $content->currentcategory}
					<h1>{$content->currentcategory.name}</h1>
					{$content->currentcategory.description}
				{else}
					<h1>{$content->title}</h1>
					{$content->welcometext}
				{/if}
			</div>

			<div class="col-md-12 col-xs-12 text-right">

				{if $content->form.viewcart.html}
				<h6>
					<i class="fa fa-shopping-bag mr-1" aria-hidden="true"></i>{$content->form.viewcart.html}
				</h6>
				{/if}
				{if $content->multicurrencies}
					Shop in {$content->form.currency.html}
				{/if}

			</div>

			{if $content->childcategories|@count || $dispatcher.category} 
				<div class="col-md-12">
					<h6>Browse</h6>
				</div>
				<div class="col-md-6 col-xs-12">
					{$content->form.categoryid.html}
				</div>
			{/if}

		</div>
		
		
		<div class="col-lg-12">
			<div class="card-deck">
				{foreach from=$content->products item=product}
					<!-- <div class="product_column col-md-3 col-sm-6 col-xs-12" > -->
						<div class="card rounded my-4 store-products-column">
							{if $product.hasimage}
								<a href="/{$dispatcher.store}/products/{$product.id}" class="store_product_thumb_container">
									<img class="store_product_thumb rounded-top w-100 card-img-top" src="/custom/{$dispatcher.store}/images/full/{$product.id}_full.jpg">
								</a>
							{else}
								<a href="/{$dispatcher.store}/products/{$product.id}" class="store_product_thumb_container">
									<img class="store_product_thumb rounded-top w-100 card-img-top" src="/custom/store/images/full/11_full.jpg">
								</a>
							{/if}
							<div class="card-body pt-3 pb-0 px-3 text-center">
								<h5 class="card-title">
									<a class="product_title" href="/{$dispatcher.store}/products/{$product.id}">{$product.name}</a>
								</h5>
	
								<!-- Temporarily Hide Teaser because it does not fit in the design -->
								<!-- {if $product.teaser}
									<p class="card-text d-none">
										{$product.teaser}
									</p>
								{/if} -->

								<p class="card-text font-weight-bold mb-0">
									{if $product.taxexempt || !$content->forceaddtax }
										{if $member && $product.memberdiscount gt 0 }
											{if $content->currency eq "JPY"}
												{$content->currencysymbol}{math equation="x - (x / 100 * y)" x=$product.price y=$product.memberdiscount|string_format:"%d"}<br>
											{else}
												{$content->currencysymbol}{math equation="x - (x / 100 * y)" x=$product.price y=$product.memberdiscount format="%.2f"}<br>
											{/if}
											<!-- <strike>{$content->currencysymbol}{$product.price format="%.2f"}</strike> -->
										{else}
											{if $content->currency eq "JPY"}
												<span class="product_price_label">{$content->currencysymbol}{$product.price|string_format:"%d"}</span>
											{else}
												<span class="product_price_label">{$content->currencysymbol}{$product.price format="%.2f"}</span>
											{/if}
										{/if}
									{else}
										{if $member && $product.memberdiscount gt 0}
											{if $content->currency eq "JPY"}
												{$content->currencysymbol}{math equation="(x - (x / 100 * z))" x=$product.price z=$product.memberdiscount|string_format:"%d"}<br>
											{else}
												{$content->currencysymbol}{math equation="(x - (x / 100 * z))" x=$product.price z=$product.memberdiscount format="%.2f"}<br>
											{/if}
											<!-- {$content->currencysymbol}{math equation="(x - (x / 100 * z)) + ((x - (x / 100 * z)) / 100 * y)" x=$product.price y=$content->tax z=$product.memberdiscount format="%.2f"}<br> -->
											<!-- <strike>{$content->currencysymbol}{math equation="x + (x / 100 * y)" x=$product.price y=$content->tax format="%.2f"}</strike> -->
										{else}
											<!-- <span class="product_price_label">{$content->currencysymbol}{math equation="x + (x / 100 * y)" x=$product.price y=$content->tax format="%.2f"}</span> -->
											{if $content->currency eq "JPY"}
												<span class="product_price_label">{$content->currencysymbol}{$product.price|string_format:"%d"}</span>
											{else}
												<span class="product_price_label">{$content->currencysymbol}{$product.price format="%.2f"}</span>
											{/if}
										{/if}
									{/if}
								</p>

								{if $content->tax gt 0 && $content->forceaddtax && !$product.taxexempt}
									<p class="card-text mb-0 text-muted font-italic">
										{if $member && $product.memberdiscount gt 0}
											{if $content->currency eq "JPY"}
												{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x - (x /100 * y)) * (z / 100)" x=$product.price z=$content->tax y=$product.memberdiscount|string_format:"%d"}
											{else}
												{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x - (x /100 * y)) * (z / 100)" x=$product.price z=$content->tax y=$product.memberdiscount format="%.2f"}
											{/if}
										{else}
											{if $content->currency eq "JPY"}
												{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x /100 * y)" x=$product.price y=$content->tax|string_format:"%d"}
											{else}
												{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}: {$content->currencysymbol}{math equation="(x /100 * y)" x=$product.price y=$content->tax format="%.2f"}
											{/if}
										{/if}
									</p>
								{/if}

								<p class="card-text mb-0">
									{if $content->stocklevels neq true || $product.type eq 'd' || $product.stock eq ''}
										<span style="color:#00AA00">Available</span>
									{elseif $product.stock neq 0 }
										<span style="color:#00AA00"><strong>{$product.stock}</strong> in stock</span>
									{else}
										<span  style="color:#DD0000">Out of stock</span>
									{/if}
								</p>
							</div>
	
							<div class="card-footer text-center bg-transparent px-1 py-2 border-top-0">
								
								{if $content->stocklevels neq true || $product.type eq 'd' || $product.stock eq ''}
									{foreach from=$product.addbutton item=addbutton}
										{$content->form.$addbutton.html}
									{/foreach}
								{elseif $product.stock neq 0 }
									{foreach from=$product.addbutton item=addbutton}
										{$content->form.$addbutton.html}
									{/foreach}
								{/if}
	
								{if $product.haspreview}
									{foreach from=$product.previewbutton item=previewbutton}
										{$content->form.$previewbutton.html}
									{/foreach}
								{/if}
							</div>
						</div>
					<!-- </div> -->
				{/foreach}
			</div>
		</div>

		<div class="col-lg-12 product-pagination-holder">
			<ul class="pagination pagination-lg justify-content-center">
				{if $content->page eq "all"}
					<li class="page-item"><a class="page-link" href="?p=all"><strong>View All</strong></a></li>
					{else}
					<li class="page-item"><a  class="page-link" href="?p=all">View All</a></li>
					{/if}
					{if $content->page gt 1}
					<li class="page-item"><a  class="page-link" href="?p={$content->page-1}">Previous</a></li>
					{/if}

				{section name=page start=1 loop=$content->numofpages+1 step=1}
					<li class="page-item">
						<a  class="page-link" href="?p={$smarty.section.page.index}">
							{if ($smarty.section.page.index eq $content->page || ($smarty.section.page.index eq 1 && $content->page eq 0))  && $content->page neq "all" }
								<strong>{$smarty.section.page.index}</strong>
							{else}
								{$smarty.section.page.index}
							{/if}
						</a>
					</li>
				{/section}

				{if $content->page lt $content->numofpages}
					<li class="page-item"><a  class="page-link" href="?p={$content->page+1}">Next</a></li>
					{/if}
			</ul>
		</div>

	</form>
</div>
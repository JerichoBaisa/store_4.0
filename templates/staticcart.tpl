{if $content->cartitems|@count}
	<!-- This is the Original HTML for EMAIL version of this page -->
	{if $content->carttype eq 'STOREEMAIL' || $content->carttype eq 'CUSTOMEREMAIL'}
		<table id="shoppingcart_summary" width="60%" cellpadding="4" cellspacing="2">
	
			<tr class="shoppingcart_summary_header">
				<td style="border-bottom: none;" bgcolor="#dddddd"></td>
				<td style="border-bottom: none;" bgcolor="#dddddd">Product</td>
				<td style="border-bottom: none;" bgcolor="#dddddd">Quantity</td>
				<td style="border-bottom: none;" bgcolor="#dddddd" align="center">Price Each</td>
				<td style="border-bottom: none;" bgcolor="#dddddd" align="center">Total {if $content->tax gt 0}(ex {$content->taxname}){/if}</td>
			</tr>

			{foreach from=$content->cartitems item=cartitem}
				<tr class="shoppingcart_summary_product_row">
					<td class="shoppingcart_summary_image">
						{if $cartitem.product.hasimage}
							{if $content->carttype eq 'STOREEMAIL' || $content->carttype eq 'CUSTOMEREMAIL'}
								<a href="http://{$content->domain}/{$dispatcher.store}/products/{$cartitem.product.id}"><img border="0" src="http://{$content->domain}/custom/store/images/thumb/{$cartitem.product.id}_thumb.jpg"></a>
							{else}
								<a href="/{$dispatcher.store}/products/{$cartitem.product.id}"><img border="0" src="/custom/store/images/thumb/{$cartitem.product.id}_thumb.jpg"></a>
							{/if}
						{/if}
					</td>
					<td class="shoppingcart_summary_name">
						{if $content->carttype eq 'STOREEMAIL' || $content->carttype eq 'CUSTOMEREMAIL'}
							<b><a href="http://{$content->domain}/{$dispatcher.store}/products/{$cartitem.product.id}">{$cartitem.product.name}</a></b>
						{else}
							<b><a href="/{$dispatcher.store}/products/{$cartitem.product.id}">{$cartitem.product.name}</a></b>
						{/if}
					</td>
					<td class="shoppingcart_summary_quantity">
						<p class="store_label">Quantity</p>
						{$cartitem.quantity}
						{if $cartitem.product.type eq 'd'}
							<font color='#FF0000'>*</font>
							{assign var='downloadnotice' value='1'}
						{elseif $cartitem.product.type eq 'a'}
							<font color='#FF0000'>*</font>
							{assign var='articlenotice' value='1'}
						{/if}
					</td>
					<td align="center" class="shoppingcart_summary_currency">
						<p class="store_label">Price</p>
						{$content->currencysymbol}{$cartitem.product.price format="%.2f"}
					</td>
					<td align="right"class="shoppingcart_summary_symbol">
						<p class="store_label">Total <br><span>(ex Florida Sales Tax)</span></p>
						<b>{$content->currencysymbol}{math equation="x * y" x=$cartitem.product.price y=$cartitem.quantity format="%.2f"}</b>
					</td>
				</tr>
			{/foreach}

			{if $content->tax gt 0}
				{assign var='rowspan' value='5'}
			{else}
				{assign var='rowspan' value='4'}
			{/if}

			{if !$content->carttotals.shippingcost}
				{assign var='rowspan' value=$rowspan-2}
			{/if}

			{if $content->carttotals.discount neq 0}
				<tr>
					<td style="border-bottom: none;" colspan="1" ></td>
					<td style="border-bottom: none;" colspan="3">Discount</td>
					<td style="border-bottom: none;" colspan="1" align="right">          
					<b>{$content->carttotals.discount} % <b>
					</td>
					
				</tr>
			{/if}
			
			<tr>
				<td style="border-bottom: none;" colspan="1" rowspan={$rowspan} class="shoppingcart_summary_to_hide"></td>
				<td style="border-bottom: none;" colspan="3">Subtotal</td>
				<td style="border-bottom: none;" colspan="1" align="right">          
				<b>{$content->currencysymbol}{$content->carttotals.subtotal format="%.2f"}<b>
				</td>
			</tr>

			{if $content->carttotals.shippingcost neq 0}
				<tr>
					<td style="border-bottom: none;" colspan="1">
						Shipping Method
					</td>
					<td style="border-bottom: none;" colspan="3"  align="right">
						{$content->shippingratename}
					</td>
				</tr>
				<tr>
					<td style="border-bottom: none;" colspan="3">Shipping Fee</td>
					<td style="border-bottom: none;" colspan="1" align="right">
						{$content->currencysymbol}{$content->carttotals.shippingcost format="%.2f"}
					</td>
				</tr>
			{/if}
			
			{if $content->tax gt 0}
			<tr>
				<td style="border-bottom: none;" colspan="3">{if $content->taxname neq ''}{$content->taxname}{else}Tax{/if}</td>
				<td style="border-bottom: none;" colspan="1" align="right">
					{if $content->carttotals.totaltax}
						{$content->currencysymbol}{$content->carttotals.totaltax format="%.2f"}
					{else}
						-
					{/if}
				</td>
			</tr>
			{/if}
			
			<tr>
				<td style="border-bottom: none;" colspan="3">Total</td>
				<td style="border-bottom: none;" colspan="1" align="right"><b>{$content->currencysymbol}{$content->carttotals.total format="%.2f"}</td>
				
			</tr>
		
		</table>

		{if $downloadnotice}
			<p><font color='#FF0000'>*</font>&nbsp;
			<i>
			{if $content->carttype eq 'CUSTOMEREMAIL'}
				You will receive a second email containing the download link for this product. Purchased products can be downloaded twice and within 24 hours of purchase.
			{elseif $content->carttype eq 'STOREEMAIL' || $content->carttype eq 'STORE'}
				Digital Purchase
			{else}
				Downloads can only be purchased in single quantities. They can be downloaded twice within 24 hours of purchase.
			{/if}	
			</i></p>
		{/if}

		{if $articlenotice}
			<p><font color='#FF0000'>*</font>&nbsp;
			<i>
			{if $content->carttype eq 'CUSTOMEREMAIL'}
				You will receive a second email containing the article link for this product.
			{elseif $content->carttype eq 'STOREEMAIL' || $content->carttype eq 'STORE'}
				Digital Purchase
			{else}
				Articles can only be purchased in single quantities.
			{/if}	
			</i></p>
		{/if}
		<br>
		{if $content->ordernotes neq ''}
			<strong>Order notes:</strong><br>
			{$content->ordernotes}
		{/if}
	{else}
		<!-- This will display to the user if this file is not called to send the Email -->
		{include file='staticcart_bootstrap4.tpl'}
	{/if}
{else}
	<h1 class="text-center">Your shopping cart is empty</h1>
{/if}
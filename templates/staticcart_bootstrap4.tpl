
<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
	{if $content->cartitems|@count}
		<div id="summary" class="bordered-box">
			<h5 class="mb-4 font-weight-bold">Order Summary </h5>
			{if $content->carttotals.discount neq 0}
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<p class="m-0">
							Coupon
						</p>
					</div>
					<div class="col-lg-6 col-md-6">
						<p class="m-0 font-weight-bold">
							{$content->carttotals.discount} % 
						</p>
					</div>
				</div>
			{/if}

			<div class="row">
				<div class="col-lg-6 col-md-6">
					<p class="m-0">
						Subtotal
					</p>
				</div>
				<div class="col-lg-6 col-md-6">
					<p class="m-0 font-weight-bold">
						{if $content->currency eq "JPY"}
							{$content->currencysymbol}{$content->carttotals.subtotal|string_format:"%d"}
						{else}
							{$content->currencysymbol}{$content->carttotals.subtotal format="%.2f"}
						{/if}
					</p>
				</div>
			</div>

			{if $content->carttotals.shippingcost neq 0}
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<p class="m-0">
							Shipping Method
						</p>
					</div>
					<div class="col-lg-6 col-md-6">
						<p class="m-0 font-weight-bold">
							{$content->shippingratename}
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6 col-md-6">
						<p class="m-0">
							Shipping Fee
						</p>
					</div>
					<div class="col-lg-6 col-md-6">
						<p class="m-0 font-weight-bold">
							{if $content->currency eq "JPY"}
								{$content->currencysymbol}{$content->carttotals.shippingcost|string_format:"%d"}
							{else}
								{$content->currencysymbol}{$content->carttotals.shippingcost format="%.2f"}
							{/if}
						</p>
					</div>
				</div>
			{/if}

			{if $content->tax gt 0}
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<p class="m-0">
							{if $content->taxname neq ''}
								{$content->taxname}
							{else}
								Tax
							{/if}
						</p>
					</div>
					<div class="col-lg-6 col-md-6">
						<p class="m-0 font-weight-bold">
							{if $content->carttotals.totaltax}
								{if $content->currency eq "JPY"}
									{$content->currencysymbol}{$content->carttotals.totaltax|string_format:"%d"}
								{else}
									{$content->currencysymbol}{$content->carttotals.totaltax format="%.2f"}
								{/if}
							{else}
								-
							{/if}
						</p>
					</div>
				</div>
			{/if}
			
			<hr>

			<div class="row mb-3">
				<div class="col-lg-6 col-md-6">
					<p class="m-0">
						Total
					</p>
				</div>
				<div class="col-lg-6 col-md-6">
					<p class="m-0 font-weight-bold">
						{if $content->currency eq "JPY"}
							{$content->currencysymbol} {$content->carttotals.total|string_format:"%d"}
						{else}
							{$content->currencysymbol} {$content->carttotals.total format="%.2f"}
						{/if}
					</p>
				</div>

				{if $downloadnotice || $articlenotice}
					<div class="col-lg-12 col-md-12">
						<p class="mb-0">
							{if $downloadnotice}
								<small class="form-text text-danger">
									{if $content->carttype eq 'STORE'}
										*Digital Purchase
									{else}
										*Downloads can only be purchased in single quantities. They can be downloaded twice within 24 hours of purchase.
									{/if}	
								</small>
							{/if}

							{if $articlenotice}
								<small class="form-text text-danger">
									{if $content->carttype eq 'STORE'}
										*Digital Purchase
									{else}
										*Articles can only be purchased in single quantities.
									{/if}	
								</small>
							{/if}
						</p>
					</div>
				{/if}
			</div>

			{if $content->ordernotes}
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<p class="mb-0 font-weight-bold">
						Order notes:
					</p>
					<p class="mb-0">
						{$content->ordernotes}
					</p>
				</div>
			</div>
			{/if}
		</div>
	{else}
		<h1 class="text-left">Your shopping cart is empty</h1>
	{/if}
</div>
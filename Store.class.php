<?php

require_once('HTML/QuickForm.php');
require_once('HTML/QuickForm/Renderer/Array.php');
require_once('HTML/QuickForm/Renderer/ArraySmarty.php');
require_once(NEURON_ROOT . 'includes/billing/GeneralFunctions.php');

class Store {
    
    public $productshippingrates = null;
    public $applyconversion = null;
    public $message = '';
    public $isOtherCountryStateTax = 0;

    function Store($STORE_ID = 1) {
        global $dispatcher, $member;
        $config = getStoreConfig($STORE_ID);

        foreach ($config as $key => $configvalue) {
            if ($key=='welcometext' || $key=='thankyouemailsubject' || $key=='thankyouemail' || $key == 'checkoutmessage') {
                $this->$key = html_entity_decode($configvalue, ENT_QUOTES);
            }
            else {
                $this->$key = $configvalue;
            }
        }

        if (isset($_SESSION[$dispatcher['store']]['browsecurrency']) && $this->multicurrencies) {
            $this->currencyinuse = $_SESSION[$dispatcher['store']]['browsecurrency'];
            if ($this->currencyinuse != $this->currency) {
                $this->applyconversion = true;
            }
        } else {
            $this->currencyinuse = $this->currency;
        }
        
        include_once NEURON_ROOT . 'includes/neuron/CurrencyFormatter.inc';
        $cc = new CurrencyFormatter();

        $this->currencysymbol = $cc->getSymbol($this->currencyinuse);
        $this->currencysymbol = $this->currencysymbol == "$" && $this->currencyinuse !== 'USD' ? strtoupper($this->currencyinuse) . " " : $this->currencysymbol;
    }

    function configureStore($STORE_ID) {
        global $db;
        global $dispatcher;
        global $settings;

        $currenciestemp = explode("\n", $settings->currencies);
        foreach ($currenciestemp as $currency) {
            $currencies[$currency] = $currency;
        }

        if (isset($_POST['categories'])) {
            unset($_POST['categories']);

            $_SESSION[$dispatcher['store']]['configvalues'] = $configvalues;
            Header("Location:/" . $dispatcher['store'] . "/configure/categories");
            die();
        } else if (isset($_POST['shippingrates'])) {
            unset($_POST['shippingrates']);

            $_SESSION[$dispatcher['store']]['configvalues'] = $configvalues;

            Header("Location:/" . $dispatcher['store'] . "/configure/shippingrates");
            die();
        }

        //set up initial store values for default category, shipping rates etc.
        $formpath = '/' . $dispatcher['store'] . '/configure';
        $form = new HTML_QuickForm('form', 'post', $formpath, null, TRUE);

        $form->addElement('text', 'title', 'Store Title', array('size' => '60', 'class' => 'form-control'));
        $form->addElement('textarea', 'welcometext', 'Welcome Text', array('cols' => '60', 'rows' => '6', 'class' => 'editor_simple'));
        $form->addElement('select', 'state', 'State (if in USA)', statesList(), array('class' => 'form-control'));
        $form->addElement('select', 'country', 'Country', countriesList(), array('class' => 'form-control', 'id' => 'countrieslist'));
        $form->addElement('select', 'currency', 'Default Currency', $currencies, array('class' => 'form-control'));
        $form->addElement('select', 'shippingcharge', 'Shipping Method', array('PC' => 'Per item', 'PB' => 'Fixed charge per basket'), array('class' => 'form-control'));
        $form->addElement('select', 'excludeshippingcosttax', 'Exclude Shipping', array('1' => 'Yes', '0' => 'No'), array('class' => 'form-control'));
        $form->addElement('text', 'tax', 'Tax Rate', array('size' => '4', 'class' => 'form-control'));
        $form->addElement('text', 'otherstatetax', 'Non-resident sales tax', array('size' => '4', 'class' => 'form-control', 'id'=> 'otherstatetax'));
        $form->addElement('select', 'forceaddtax', 'When to calculate tax', array('1' => 'Before Checkout', '0' => 'After Checkout'), array('class' => 'form-control'));
        $form->addElement('text', 'taxname', 'Name of Tax (ie: VAT, Sales Tax)', array('size' => '30', 'class' => 'form-control'));
        $form->addElement('text', 'orderemail', 'Email address to send orders to', array('size' => '60', 'class' => 'form-control'));
        $form->addElement('text', 'fromemail', 'Email address that emails send from', array('size' => '60', 'class' => 'form-control'));
        $form->addElement('text', 'thankyouemailsubject', 'Thank you Email Subject', array('size' => '60', 'class' => 'form-control'));
        $form->addElement('textarea', 'thankyouemail', 'Thank you Email Text', array('cols' => '60', 'rows' => '6', 'class' => 'form-control'));
        $form->addElement('textarea', 'checkoutmessage', 'Thank you Page Text', array('cols' => '60', 'rows' => '6', 'class' => 'form-control'));
        $form->addElement('checkbox', 'stocklevels', 'Enable Stock Levels', array('class' => 'testclass'));
        $form->addElement('text', 'memberdiscount', 'Default Member Discount', array('size' => '3', 'class' => 'form-control'));
        $form->addElement('checkbox', 'multicurrencies', 'Enable multiple currencies', null, array('onclick' => 'toggleCurrencyRounding();'));
        $form->addElement('select', 'currencyrounding', 'Round currency conversion', array('0' => 'Do not round conversion',
            '1' => 'Round up to nearest 0.25 unit',
            '2' => 'Round up to nearest 0.50 unit',
            '3' => 'Round up to nearest 1.00 unit'));
        $form->addElement('text', 'productspp', 'Products per page', array('size' => '3', 'class' => 'form-control'));
        $form->addElement('text', 'minimum_cart_total', 'Minimum shopping cart total', array('size' => '3', 'class' => 'form-control'));
        $form->addElement('select', 'enabled', 'Store Status', array('1' => 'Open', '0' => 'Closed'), array('class' => 'form-control'));
        //$form->addElement('select','weightunit','Unit of Weight',array('KG'=>'KG','LB'=>'LB'));

        $form->addElement('text', 'productlimitdays', 'Digital product download link validity (Days)', array('class' => 'form-control'));
        $form->addElement('text', 'downloadlimit', 'Download Limit', array( 'class' => 'form-control'));

        $form->addElement('submit', 'categories', 'Configure Product Categories', array('class' => 'btn btn-primary'));
        $form->addElement('checkbox', 'showcategories', 'Show subcategories above products');
        $form->addElement('submit', 'shippingrates', 'Configure Shipping Rates', array('class' => 'btn btn-primary'));
        $form->addElement('submit', 'submit', 'Save Store Configuration', array('id'=>'save-store-config','class' => 'btn btn-primary'));

        //validation rules - have to be server side so we can catch the categories / shipping rate buttons without a complete form
        $form->addRule('country', 'Please select a country', 'required', null);
        $form->addRule('shippingcharge', 'Please select a shipping charge method', 'required', null);
        $form->addRule('orderemail', 'Please enter a valid email address for orders', 'email', null);
        $form->addRule('orderemail', 'Please enter a valid email address for orders', 'required', null);
        $form->addRule('fromemail', 'Please enter a valid email address for orders', 'email', null);
        $form->addRule('fromemail', 'Please enter a valid email address for orders', 'required', null);
        //$form->addRule('weightunit','Please select a Unit of Weight to use','required', null,'client');

        if (isset($_SESSION[$dispatcher['store']]['configvalues'])) {
            $form->setDefaults($_SESSION[$dispatcher['store']]['configvalues']);
        } else {
           
            $productlimitdays = (empty($settings->product_validity_days) ? 1 : $settings->product_validity_days);
            $downloadlimit = (empty($settings->product_download_limit) ? 2 : $settings->product_download_limit);
            
            $form->setDefaults(array('title' => $this->title, 'welcometext' => $this->welcometext, 'state' => $this->state,
                'country' => $this->country, 'currency' => $this->currency, 'shippingcharge' => $this->shippingcharge, 'tax' => $this->tax, 'forceaddtax' => $this->forceaddtax,
                'taxname' => $this->taxname, 'orderemail' => $this->orderemail, 'fromemail' => $this->fromemail, 'thankyouemailsubject' => $this->thankyouemailsubject,
                'thankyouemail' => $this->thankyouemail, 'stocklevels' => $this->stocklevels, 'memberdiscount' => $this->memberdiscount,
                'multicurrencies' => $this->multicurrencies, 'currencyrounding' => $this->currencyrounding, 'productspp' => $this->productspp,
                'showcategories' => $this->showcategories, 'enabled' => $this->enabled, 'minimum_cart_total' => $this->minimum_cart_total,
                'productlimitdays' => $productlimitdays, 'downloadlimit' => $downloadlimit, 'checkoutmessage' => $this->checkoutmessage, 
                'excludeshippingcosttax' => $this->excludeshippingcosttax, 'otherstatetax' => $this->otherstatetax));
        }

        $this->minimum_cart_total = (!is_numeric($this->minimum_cart_total) ? 0 : $this->minimum_cart_total);
        $this->form = $form;

        if ($this->form->validate() && isset($_POST['submit'])) {
            unset($_POST['submit']);

            foreach ($_POST as $key => $value) {
                if ($key=='welcometext') {
                    $storeconfig[$key] = htmlentities($value, ENT_QUOTES);
                }
                else {
                    if ($key =='downloadlimit' || $key =='productlimitdays') {
                        if($key =='productlimitdays') {
                            $val = (!is_numeric($value) ? $productlimitdays : $value);
                        } else {
                            $val = (!is_numeric($value) ? $downloadlimit : $value);
                        }
                        $storeconfiglimit[$key] = filter_var($val, FILTER_SANITIZE_STRING);
                    } else {
                        $storeconfig[$key] = filter_var($value, FILTER_SANITIZE_STRING);
                    }
                }
            }

            if (!isset($_POST['stocklevels']))
                $storeconfig['stocklevels'] = 0;
            if (!isset($_POST['multicurrencies']))
                $storeconfig['multicurrencies'] = 0;
            if (!isset($_POST['showcategories']))
                $storeconfig['showcategories'] = 0;
            
            //set default limit
            if (SITE_NAME == 'loveprayteach' || SITE_NAME == 'oscarg') {
                if(!isset($_POST['productlimitdays'])) 
                    $storeconfiglimit['productlimitdays'] = 90;
                if(!isset($_POST['downloadlimit']))
                    $storeconfiglimit['downloadlimit'] = 5;
            } else if (SITE_NAME == 'youniverse') {
                if(!isset($_POST['productlimitdays']))
                    $storeconfiglimit['productlimitdays'] = 2;
                if(!isset($_POST['downloadlimit']))
                    $storeconfiglimit['downloadlimit'] = 5;
            } else {
                if(!isset($_POST['productlimitdays']))
                    $storeconfiglimit['productlimitdays'] = 1;
                if(!isset($_POST['downloadlimit']))
                    $storeconfiglimit['downloadlimit'] = 2;
            }

            if (getStoreConfig($STORE_ID)) {
                $this->checkMinCartFieldExist();

                $sql = "UPDATE store SET ";

                foreach ($storeconfig as $field => $value) {
                    $sql .= $field . " = '" . $value . "', ";
                }

                $sql .= " configured='1' WHERE id = '" . $STORE_ID . "'";

                $sqlvalidity = sprintf("REPLACE INTO neuron_settings (`name`, `value`) VALUES ('product_validity_days','%s')", $storeconfiglimit['productlimitdays']);
                $sqldownloadlimit = sprintf("REPLACE INTO neuron_settings (`name`, `value`) VALUES ('product_download_limit','%s')", $storeconfiglimit['downloadlimit']);
        
            } else {
                $this->checkMinCartFieldExist();

                $sql = sprintf("INSERT INTO store (id, title, welcometext, state, country, currency, shippingcharge, tax, forceaddtax,
											taxname, orderemail, fromemail, thankyouemailsubject, thankyouemail, weightunit, stocklevels,
											memberdiscount, multicurrencies, currencyrounding, productspp, showcategories, enabled, configured, minimum_cart_total, checkoutmessage, excludeshippingcosttax, otherstatetax)
									VALUES ('%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%d',
											'%s', '%s', '%s', '%s', '%s', NULL, '%d', '%s', '%d', '%d', '%d', '%d','%d', '%d', '%d', '%s', '%s')", $STORE_ID, $storeconfig['title'], $storeconfig['welcometext'], $storeconfig['state'], $storeconfig['country'], $storeconfig['currency'], $storeconfig['shippingcharge'], $storeconfig['tax'], $storeconfig['forceaddtax'], $storeconfig['taxname'], $storeconfig['orderemail'], $storeconfig['fromemail'], $storeconfig['thankyouemailsubject'], $storeconfig['thankyouemail'], $storeconfig['stocklevels'], $storeconfig['memberdiscount'], $storeconfig['multicurrencies'], $storeconfig['currencyrounding'], $storeconfig['productspp'], $storeconfig['showcategories'], $storeconfig['enabled'], 1, $storeconfig['minimum_cart_total'], $storeconfig['checkoutmessage'], $storeconfig['excludeshippingcosttax'], $storeconfig['otherstatetax']);
            
                $sqlvalidity = sprintf("REPLACE INTO neuron_settings (`name`, `value`) VALUES ('product_validity_days','%s')", $storeconfiglimit['productlimitdays']);
                $sqldownloadlimit = sprintf("REPLACE INTO neuron_settings (`name`, `value`) VALUES ('product_download_limit','%s')", $storeconfiglimit['downloadlimit']);
            }

            if ($db->Execute($sql) && $db->Execute($sqlvalidity)  && $db->Execute($sqldownloadlimit)) {
                unset($_SESSION[$dispatcher['store']]['configvalues']);
                // Header("Location:/" . $dispatcher['store'] . "/");  
                $this->message = "Store configuration successfully save."; 
            } else {
                $this->message = "Saving store configuration failed.  Please contact support@subhub.com";
            }
        } 
        $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
        $this->form->accept($renderer);
        $this->form = $renderer->toArray();
        $this->template = 'setupstore.tpl';
    }

    function configureShippingRates($STORE_ID) {
        global $db;
        global $dispatcher;

        if (isset($_POST['cancel'])) {
            Header("Location:/" . $dispatcher['store'] . "/configure/");
            die();
        }

        //set up initial store values for default category, shipping rates etc.
        $formpath = '/' . $dispatcher['store'] . '/configure/shippingrates';
        $form = new HTML_QuickForm('form', 'post', $formpath, null, TRUE);
        $form->addElement('submit', 'addshippingrate', 'Add New Shipping Rate');
        $form->addElement('submit', 'submit', 'Save Shipping Rates');
        $form->addElement('submit', 'cancel', 'Cancel');

        if (isset($_SESSION['store']['shippingrates'])) {
            $shippingrates = $_SESSION['store']['shippingrates'];
        } else {
            $shippingrates = getShippingRates();
        }

        $shipping_rate_tdkey = '';
        $shipping_rate_tdVal = '';
        
        foreach ($_POST as $key => $value) {
            $postvar = explode("_", $key);
            if ($postvar[0] == "removeshippingrate") {
                //if it's a rate in the database
                if (array_key_exists($postvar[1], $shippingrates) && isset($shippingrates[$postvar[1]]['id'])) {
                    //flag the rate for deletion from the db
                    $shippingrates[$postvar[1]]['delete'] = 1;
                } else {
                    //remove it from the session vars
                    unset($shippingrates[$postvar[1]]);
                }

                unset($_POST['removeshippingrate_' . $postvar[1]]);
            } else if ($postvar[0] == "shipping") {
                $shippingrates[$postvar[2]] = array('id' => $_POST['shipping_id_' . $postvar[2]],
                    'name' => $_POST['shipping_name_' . $postvar[2]],
                    'defaultitemcost' => $_POST['shipping_defaultitemcost_' . $postvar[2]],
                    'basketcost' => $_POST['shipping_basketcost_' . $postvar[2]],
                    'percentdiscount' => $_POST['shipping_percentdiscount_' . $postvar[2]]);
            }
        }

        if (isset($_POST['addshippingrate'])) {
            $shippingrates[] = array('id' => null, 'name' => '', 'defaultitemcost' => '', 'basketcost' => '', 'percentdiscount' => '');
        }


        $_SESSION['store']['shippingrates'] = $shippingrates;

        //generate the shipping rates form from the session variables
        if (isset($_SESSION['store']['shippingrates'])) {
            $this->shippingratesholder = array();

            foreach ($_SESSION['store']['shippingrates'] as $key => $value) {
                //check to see if the rate is flagged for deletion
                if (!isset($value['delete'])) {
                    //check to see if we need to preserve the actual db record id for an update
                    if (isset($value['id'])) {
                        $form->addElement('hidden', 'shipping_id_' . $key, $value['id']);
                    }
                    $form->addElement('text', 'shipping_name_' . $key, null, array('size' => '25', 'class' => 'form-control'));

                    if ($this->shippingcharge === 'PB') {
                        $form->addElement('text', 'shipping_basketcost_' . $key, null, array('id'=> 'shipping_basketcost_' . $key,'size' => '6', 'class' => 'form-control'));
                        $shipping_rate_tdkey = 'shipping_basketcost_';
                        $shipping_rate_tdVal = 'basketcost';
                        $this->shippingratesholder[$key] = array('shipping_name_' . $key, $shipping_rate_tdkey . $key, 'removeshippingrate_' . $key);
                    } else if ($this->shippingcharge === 'PC') {
                        $form->addElement('text', 'shipping_defaultitemcost_' . $key, null, array('id'=> 'shipping_defaultitemcost_' . $key, 'size' => '6', 'class' => 'form-control'));
                        $shipping_rate_tdkey = 'shipping_defaultitemcost_';
                        $shipping_rate_tdVal = 'defaultitemcost';
                        $form->addElement('text', 'shipping_percentdiscount_' . $key, null, array('id'=>'shipping_percentdiscount_' . $key, 'size' => '5', 'class' => 'form-control', 'autocomplete' => 'off'));
                        $this->shippingratesholder[$key] = array('shipping_name_' . $key, $shipping_rate_tdkey . $key, 'shipping_percentdiscount_' . $key, 'removeshippingrate_' . $key);
                    }

                    
                    $form->addElement('submit', 'removeshippingrate_' . $key, 'Remove');

                    $form->addRule('shipping_name_' . $key, 'Enter a value', 'required', null, 'client');
                    //$form->addRule('shipping_defaultitemcost_'.$key, 'Enter a value', 'required', null, 'client');
                    //$form->addRule('shipping_basketcost_'.$key, 'Enter a value', 'required', null, 'client');
                    //$form->addRule('shipping_percentdiscount_'.$key, 'Enter a value', 'required', null, 'client');

                    $form->setDefaults(array('shipping_name_' . $key => $value['name'],
                        $shipping_rate_tdkey . $key => $value[$shipping_rate_tdVal],
                        'shipping_percentdiscount_' . $key => $value['percentdiscount']));

                   
                }
            }
        }

        $this->form = $form;

        if ($this->form->validate() && isset($_POST['submit'])) {
            unset($_POST['submit']);

            foreach ($_SESSION['store']['shippingrates'] as $key => $shippingrate) {
                if (isset($shippingrate['delete'])) {
                    //delete the rate
                    $shippingratesql = sprintf("DELETE FROM store_shipping_rates WHERE id='%d'", $shippingrate['id']);
                    unset($_SESSION['store']['shippingrates'][$key]);

                    //delete the product rates for the shipping rate
                    $deleteshippingratesql = sprintf("DELETE FROM store_shipping_rates_product WHERE shippingrateid='%d'", $shippingrate['id']);
                    $db->Execute($deleteshippingratesql);
                } else if (isset($shippingrate['id'])) {
                    //ask about updating product defaults ? - FIXME

                    $productsql = sprintf("UPDATE store_shipping_rates_product SET cost='%s' WHERE shippingrateid='%d'", $shippingrate['defaultitemcost'], $shippingrate['id']);

                    $db->Execute($productsql);

                    //update the rate
                    $shippingratesql = sprintf("UPDATE store_shipping_rates SET name = '%s', defaultitemcost = '%s', basketcost = '%s',percentdiscount = '%s'
												WHERE id = '%d'", $shippingrate['name'], $shippingrate['defaultitemcost'], $shippingrate['basketcost'], $shippingrate['percentdiscount'], $shippingrate['id']);
                } else {
                    //add the rate
                    $shippingratesql = sprintf("INSERT INTO store_shipping_rates (id,name,defaultitemcost,basketcost,percentdiscount)
									VALUES (NULL,'%s','%s','%s','%s')", $shippingrate['name'], $shippingrate['defaultitemcost'], $shippingrate['basketcost'], $shippingrate['percentdiscount']);
                }

                $shippingratesqlresult = $db->Execute($shippingratesql);
                $newshippingrateid = $db->Insert_ID();

                //if we're dealing with a new shipping rate - add the new rate to existing products
                if (!isset($shippingrate['delete']) && !isset($shippingrate['id'])) {
                    $products = getProducts($STORE_ID, null);

                    foreach ($products as $product) {
                        if ($product['type'] == 'p') {
                            $newshippingratesql = sprintf("INSERT INTO store_shipping_rates_product(id, productid, shippingrateid, cost) VALUES
														(NULL, '%d','%d','%s')", $product['id'], $newshippingrateid, $shippingrate['defaultitemcost']);

                            $db->Execute($newshippingratesql);
                        }
                    }
                }
            }

            if ($shippingratesqlresult) {
                unset($_SESSION['store']['shippingrates']);
                Header("Location:/" . $dispatcher['store'] . "/configure");
                die();
            } else {
                $this->message = "Saving shipping rate configuration failed.  Please contact support@subhub.com";
                $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
                $this->form->accept($renderer);
                $this->form = $renderer->toArray();
                $this->template = 'setupshippingrates.tpl';
            }
        } else {
            $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            $this->form->accept($renderer);
            $this->form = $renderer->toArray();
            $this->template = 'setupshippingrates.tpl';
        }
    }

    function reorderProducts($products, $category = 0) {
        global $db;
        global $fp;

        #$fp->log($products);

        $sql = sprintf("DELETE FROM store_products_categories WHERE category_id = %s", $category);
        $db->Execute($sql);

        $pa = explode(',', $products);
        //$fp->log($category);
        #$fp->log($pa);
        #die();
        foreach ($pa as $k => $p) {
            if ($p) {
                $sql = sprintf("INSERT INTO store_products_categories (product_id,category_id,store_products_categories.order) VALUES (%s,%s,%s)", $p, $category, $k);
                #$fp->log($sql);
                #$fp->log($p);
                $db->Execute($sql);
            }
        }
    }

    function manageProducts($STORE_ID, $category = null, $search = null) {
        global $db;
        global $dispatcher;
        global $tpl;

        if ($category != null) {
            $this->currentCat = $category;
        }

        $categories = getCategories();
        foreach ($categories as $k => $cat) {
            $categories[$k]['name'] = spaceCategoryName($cat);
            #vd::dump(getCategoryLevel($cat));
        }

        #vd::dump($categories);
        #die();

        $this->categories = $categories;

        $sql = "SELECT id, name, type, visibility, price FROM store_products WHERE discontinued != 1";
        if ($category) {
            $sql .= sprintf(" AND categoryid = %s", $category);
        }
        if ($search) {
            $sql .= sprintf(" AND name LIKE '%%%s%%'", $search);
        }
        #vd::dump($sql);
        $sql .= " ORDER BY id DESC";
        $res = $db->getAll($sql);

        if (!$category) {
            $category = 0;
        }


        if ($res) {
            foreach ($res as $k => $r) {
                $prods[$k]['id'] = $r['id'];
                $prods[$k]['name'] = $r['name'];
                switch ($r['type']) {
                    case 'p':
                        $prods[$k]['type'] = 'Physical';
                        break;
                    case 'd':
                        $prods[$k]['type'] = 'Digital';
                        break;
                    case 'a':
                        $prods[$k]['type'] = 'Article';
                        break;
                }
                switch ($r['visibility']) {
                    case 'PUBLIC':
                        $prods[$k]['visibility'] = 'Everyone';
                        break;
                    case 'MEMBERS':
                        $prods[$k]['visibility'] = 'Members';
                        break;
                    case 'PRIVATE':
                        $prods[$k]['visibility'] = 'Administrator';
                        break;
                }
                $prods[$k]['price'] = $r['price'];
            }
        }

        $sql2 = sprintf("SELECT * FROM store_products_categories WHERE category_id = %s ORDER BY store_products_categories.order", $category);
        $res2 = $db->getAll($sql2);
        if ($res2 && $prods) {
            /* foreach($res2 AS $k=>$o) {
              foreach($prods as $k2=>$p) {
              if($p['id'] == $o['product_id']) {
              $this->products[$k] = $p;
              break;
              }
              }
              } */
            foreach ($prods as $k2 => $p) {
                $prods[$k2]['order'] = count($prods) + 1;
                foreach ($res2 AS $k => $o) {
                    if ($p['id'] == $o['product_id']) {
                        $prods[$k2]['order'] = $o['order'];
                    }
                }
            }

            $this->products = array_sort($prods, "order", "SORT_ASC");
        } else {
            $this->products = $prods;
        }
        #vd::dump(print_r($this, true));

        $tpl->assign_by_ref('content', $this);
        $this->template = 'manageproducts.tpl';
    }

    function configureCategories($STORE_ID) {
        global $db;
        global $dispatcher;

        if (isset($_POST['cancel'])) {
            Header("Location:/" . $dispatcher['store'] . "/configure/");
            die();
        }

        //set up initial store values for default category, shipping rates etc.
        $formpath = '/' . $dispatcher['store'] . '/configure/categories';
        $form = new HTML_QuickForm('form', 'post', $formpath, null, TRUE);
        $form->addElement('submit', 'addcategory', 'Add New Category');
        $form->addElement('submit', 'submit', 'Save Categories');
        $form->addElement('submit', 'cancel', 'Cancel');

        if (isset($_SESSION['store']['categories'])) {
            $categories = $_SESSION['store']['categories'];
        } else {
            $categories = getCategories();
        }

        foreach ($_POST as $key => $value) {
            $postvar = explode("_", $key);
            if ($postvar[0] == "removecategory") {
                //if it's a rate in the database
                if (array_key_exists($postvar[1], $categories) && isset($categories[$postvar[1]]['id'])) {

                    //flag the rate for deletion from the db
                    $categories[$postvar[1]]['delete'] = 1;

                    //check to see if any products are in the category
                    if (count(getProductsByCategory($STORE_ID, $postvar[1], true))) {
                        $categories[$postvar[1]]['productsincategory'] = 1;
                    }
                } else {
                    //remove it from the session vars
                    unset($categories[$postvar[1]]);
                }

                unset($_POST['removecategory_' . $postvar[1]]);
            } else if ($postvar[0] == "category") {
                $categories[$postvar[2]] = array('id' => $_POST['category_id_' . $postvar[2]],
                    'name' => $_POST['category_name_' . $postvar[2]],
                    'description' => $_POST['category_description_' . $postvar[2]],
                    'visibility' => $_POST['category_visibility_' . $postvar[2]],
                    'parent' => $_POST['category_parent_' . $postvar[2]]);
            }
        }


        if (isset($_POST['addcategory'])) {
            $categories[] = array('id' => null, 'name' => '', 'description' => '', 'visibility' => '', 'parent' => '');
        }

        $_SESSION['store']['categories'] = $categories;

        //build category dropdown options
        $categoriesarray = array('0' => 'None');

        foreach (getCategories() as $category) {
            $categoriesarray[$category['id']] = $category['name'];
        }

        //generate the shipping rates form from the session variables
        if (isset($_SESSION['store']['categories'])) {
            $this->categoriesholder = array();


            foreach ($_SESSION['store']['categories'] as $key => $value) {

                //check to see if the rate is flagged for deletion
                if (!isset($value['delete'])) {
                    $categoriesflat = $categoriesarray;
                    //check to see if we need to preserve the actual db record id for an update
                    if (isset($value['id'])) {
                        $form->addElement('hidden', 'category_id_' . $key, $value['id']);
                        unset($categoriesflat[$value['id']]);
                    }

                    $form->addElement('text', 'category_name_' . $key, null, array('size' => '20', 'class' => 'form-control'));
                    $form->addElement('textarea', 'category_description_' . $key, null, array('cols' => '20', 'rows' => '2', 'class' => 'form-control'));
                    $form->addElement('select', 'category_visibility_' . $key, null, array('PUBLIC' => 'Everyone', 'MEMBERS' => 'Members only', 'PRIVATE' => 'Administrators', 'class' => 'form-control'));
                    $form->addElement('select', 'category_parent_' . $key, null, $categoriesflat, array('class' => 'form-control'));
                    $form->addElement('submit', 'removecategory_' . $key, 'Remove');

                    $form->addRule('category_name_' . $key, 'Enter a value', 'required', null, 'client');
                    $form->addRule('category_visibility_' . $key, 'Enter a value', 'required', null, 'client');

                    $form->setDefaults(array('category_name_' . $key => $value['name'],
                        'category_description_' . $key => $value['description'],
                        'category_visibility_' . $key => $value['visibility'],
                        'category_parent_' . $key => $value['parent']));

                    $this->categoriesholder[$key] = array('category_name_' . $key, 'category_description_' . $key, 'category_visibility_' . $key, 'category_parent_' . $key, 'removecategory_' . $key);
                }
            }
        }

        $this->form = $form;

        if ($this->form->validate() && isset($_POST['submit'])) {
            unset($_POST['submit']);

            foreach ($_SESSION['store']['categories'] as $key => $category) {
                if (isset($category['delete'])) {
                    //Display page to reassign products if they are in the category to be delete - FIXME
                    //delete the category
                    $categorysql = sprintf("DELETE FROM store_categories WHERE id='%d'", $category['id']);
                    unset($_SESSION['store']['categories'][$key]);

                    //move products in the deleted category to uncategorised
                    $productsql = sprintf("UPDATE store_products SET categoryid='0' WHERE categoryid='%d'", $category['id']);
                    $db->Execute($productsql);
                } else if (isset($category['id'])) {
                    //update the category
                    $categorysql = sprintf("UPDATE store_categories SET name = %s, description = %s, visibility = '%s',parent = '%d'
											WHERE id = '%d'", $db->qstr($category['name']), $db->qstr($category['description']), $category['visibility'], $category['parent'], $category['id']);
                    //update products visibility for the category being updated

                    $productsql = sprintf("UPDATE store_products set visibility = '%s' WHERE categoryid = '%d'", $category['visibility'], $category['id']);
                    $db->Execute($productsql);
                } else {
                    //add the category
                    $categorysql = sprintf("INSERT INTO store_categories (id,name,description,visibility,parent)
											VALUES (NULL, %s, %s,'%s','%d')", $db->qstr($category['name']), $db->qstr($category['description']), $category['visibility'], $category['parent']);
                }

                $categorysqlresult = $db->Execute($categorysql);
            }

            if ($categorysqlresult) {
                unset($_SESSION['store']['categories']);
                Header("Location:/" . $dispatcher['store'] . "/configure");
            } else {
                $this->message = "Saving category configuration failed.  Please contact support@subhub.com";
                $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
                $this->form->accept($renderer);
                $this->form = $renderer->toArray();
                $this->template = 'setupcategories.tpl';
            }
        } else {
            $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            $this->form->accept($renderer);
            $this->form = $renderer->toArray();
            $this->template = 'setupcategories.tpl';
        }
    }

    function showProducts($STORE_ID, $categoryid, $page) {
        global $dispatcher, $settings, $member;
        $DEFAULT_QUANTITY = 1;
        $SHOW_PRODUCTS_IN_SUBCATEGORIES = 1;

        if (isset($_POST['categoryid']) && $_POST['categoryid'] != $dispatcher['category']) {
            Header("Location:/" . $dispatcher['store'] . "/" . $_POST['categoryid']);
            die();
        } else if (isset($_POST['currency']) && $_POST['currency'] != $this->currencyinuse) {
            $_SESSION[$dispatcher['store']]['browsecurrency'] = $_POST['currency'];
            Header("Location:/" . $dispatcher['store'] . '/' . $dispatcher['category']);
            die();
        }

        foreach ($_POST as $key => $value) {
            $postexp = explode("_", $key);
        }

        $currencyexp = explode("\n", $settings->currencies);

        foreach ($currencyexp as $currency) {
            $currencies[$currency] = $currency;
        }

        $formpath = '/' . $dispatcher['store'] . '/' . $dispatcher['category'];

        $form = new HTML_QuickForm('form', 'POST', $formpath, null, TRUE);
        $this->categories = getCategories();
        $this->childcategories = getChildCategories($STORE_ID, $categoryid);

        $patharray = null;

        $this->categoryhistory = getCategoryHistory($categoryid, $patharray);


        $categoryarray = null;

        $categorytree = getSubCategoriesByCategory(0, 'ARRAY', $categoryarray);

        $categoryflat = null;

        $categoriesfordisplay = categoriesToDisplayArray($categorytree, 0, $categoryflat);

        unset($categoriesfordisplay[0]);

        if ($this->categories) {
            foreach ($this->categories as $key => $category) {
                $categorynames[$category['id']] = $category['name'];
                if ($category['id'] == $categoryid) {
                    $this->currentcategory = $this->categories[$key];
                }
            }

            //indent the subcategories for the drop down
            $categoriesflat = array();
            foreach ($categoriesfordisplay as $key => $depthcount) {
                for ($i = 1; $i < $depthcount; $i++) {
                    $spacing .= "&nbsp;&nbsp;&nbsp;&nbsp;";
                }

                $categoriesflat[$key] = $spacing . $categorynames[$key];
                unset($spacing);
            }

            $categoriesflat = array('' => "All Products") + $categoriesflat;
        } else {
            $categoriesflat = array('' => "All Products");
        }

        $form->addElement('select', 'currency', 'Browse in', $currencies, array('onchange' => 'form.submit()', 'class' => 'form-control'));
        $form->addElement('select', 'categoryid', 'Category', $categoriesflat, array('onchange' => 'form.submit()', "class" => "form-control"));
        //$form->addElement('submit','gocategories','Go');
        $form->addElement('submit', 'viewcart', 'My Cart', array("class" => "btn btn-primary btn-custom-link", "role" => "button"));

        $dispactherStore = isset($dispatcher['store']) ? $dispatcher['store'] : '';
        if(isset($_SESSION[$dispactherStore])) {
            $form->setDefaults(array('currency' => $_SESSION[$dispactherStore]['browsecurrency']));
        }

        if (isset($this->currentcategory) || !$categoryid) {
            //we've got permission to view this category (or we're viewing all products)
            if ($categoryid) {
                $products = getProductsByCategory($STORE_ID, $categoryid, $SHOW_PRODUCTS_IN_SUBCATEGORIES);
                // vd::dump($products);
            } else {
                $products = getProducts($STORE_ID, null);
            }

            //make pagination info available to templates
            if (!$page)
                $page = 1;
            $this->page = $page;
            $this->numofpages = ceil(count($products) / $this->productspp);
            $this->totalproducts = count($products);
            $start = intval($page - 1) * $this->productspp;
            $this->start = $start;
            if ($products) {
                if ($page != "all") {
                    if ($start) {
                        $products = array_slice($products, $start, $this->productspp, true);
                    } else {
                        $products = array_slice($products, 0, $this->productspp, true);
                    }
                }

                foreach ($products as $key => $product) {

                    if (array_key_exists('description',  $product)) {
                        $products[$key]['description'] = htmlspecialchars_decode($product['description'], ENT_QUOTES); 
                    }
                    
                    if (array_key_exists('teaser',  $product)) {
                        $products[$key]['teaser'] = htmlspecialchars_decode($product['teaser'], ENT_QUOTES); 
                    }
                                        
                    if (checkProductHasPreview($product['id'])) {
                        $form->addElement('submit', 'preview_' . $product['id'], 'Preview', array("role" => "button", "class" => "btn btn-primary"));
                        $products[$key]['haspreview'] = 1;
                        $products[$key]['previewbutton'] = array('preview_' . $product['id'], array());
                    }

                    if ($this->applyconversion)
                        $products[$key]['price'] = $this->currencyconvert($product['price']);

                    if ($product['memberdiscount'] == '' || floatval($product['memberdiscount']) == 0) {
                        if ($this->memberdiscount) {
                            $products[$key]['memberdiscount'] = $this->memberdiscount;
                        } else {
                            $products[$key]['memberdiscount'] = 0;
                        }
                    }  
                                      
                    $form->addElement('submit', 'addtocart_' . $product['id'], 'Add to Cart', array('class' => 'btn btn-primary m-1', 'role' => 'button'));
                    $products[$key]['addbutton'] = array('addtocart_' . $product['id']);
                }
            } else {
                $products = array();
            }
            $form->setDefaults(array('categoryid' => $categoryid));
            $this->products = $products;
        }


        $this->form = $form;
        #echo 14;
        if ($this->form->validate()) {

            foreach ($_POST as $key => $value) {
                $keyexp = explode("_", $key);
                if ($keyexp[0] == 'preview') {
                    getPreview($keyexp[1]);
                } else if ($keyexp[0] == 'addtocart') {
                    $this->addToCart($keyexp[1], $DEFAULT_QUANTITY);
                }
            }

            Header("Location:/" . $dispatcher['store'] . "/cart");
        } else {
            #echo 15;
            $renderer = &new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            #echo 16;
            $this->form->accept($renderer);
            #echo 17;
            $this->form = $renderer->toArray();
            #echo 18;
            $this->template = 'showproducts.tpl';
        }
    }

    function addToCart($productid) {
        global $dispatcher;
        $product = getProduct($productid);

        if (isset($_SESSION[$dispatcher['store']]['cartitems'])) {
            $cartitems = $_SESSION[$dispatcher['store']]['cartitems'];
        } else {
            $cartitems = array();
        }

        if (!array_key_exists($productid, $cartitems) || $product['type'] != 'p') {
            $cartitems[$productid] = array('id' => $productid, 'type' => $product['type'], 'quantity' => '1', 'name' => $product['name']);
        } else {
            $cartitems[$productid]['quantity'] ++;
        }

        $_SESSION[$dispatcher['store']]['cartitems'] = $cartitems;
    }

    function ajaxShowCart() {
        global $dispatcher, $member;
        $data = array();
        $message = '';
        parse_str($_POST['value'], $data);

        if (!empty($data['coupon_code'])) {
            $message = $this->checkPromoCodeStatus($data['coupon_code']);
            if (!empty($message)) {
                unset($_SESSION[$dispatcher['store']]['coupon_code']);
                return ['message' => $message];
            }
        }

        if (isset($_SESSION[$dispatcher['store']]['cartitems']) && empty($message)) {
            //check for remove / update button presses
            foreach ($data as $key => $value) {

                $keyexp = explode("_", $key);
                if ($keyexp[0] == 'quantityinput') {
                    if ($value < 1) {
                        unset($_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]);
                    } else if ($_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]['type'] == 'p') {
                        $_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]['quantity'] = $value;
                    }
                }
            }

            //add quantity input and remove button to cart items
            foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
                //find out if any of the products are physical ones
                $cartitems[$key]['product'] = getProduct($key);

                if ($this->applyconversion)
                    $cartitems[$key]['product']['price'] = $this->currencyConvert($cartitems[$key]['product']['price']);
                if ($cartitems[$key]['product']['memberdiscount'] == '' || floatval($cartitems[$key]['product']['memberdiscount']) == 0) {
                    if ($this->memberdiscount) {
                        $cartitems[$key]['product']['memberdiscount'] = $this->memberdiscount;
                    } else {
                        $cartitems[$key]['product']['memberdiscount'] = 0;
                    }
                }
                if ($member)
                    $cartitems[$key]['product']['price'] = sprintf("%.2f", $cartitems[$key]['product']['price'] - ($cartitems[$key]['product']['price'] / 100 * $cartitems[$key]['product']['memberdiscount']));

                $cartitems[$key]['quantity'] = $cartitem['quantity'];

                if ($cartitem['type'] == 'p') {
                    $this->displayshippingrates = true;
                }
            }
        }

        $_SESSION[$dispatcher['store']]['coupon_code'] = $data['coupon_code'];
        
        //get the totals
        // Orig Code $this->carttotals = $this->calculateCartTotals($data['shippingrate'], $this->forceaddtax);

        // $this->forceaddtax set to true to show all data in order summary
        $this->carttotals = $this->calculateCartTotals($data['shippingrate'], 0);
        $this->cartitems = $cartitems;

        //$cartitems['carttotals'] = $this->carttotals;
        //save the order notes
        $_SESSION[$dispatcher['store']]['cart']['ordernotes'] = $data['ordernotes'];
        

        if (isset($data['shippingrate'])) {
            $_SESSION[$dispatcher['store']]['cart']['shippingrateid'] = $data['shippingrate'];
        }
        return $this->carttotals;
    }

    function showCart() {
        global $dispatcher, $member, $settings, $tpl2;

        $formpath = '/' . $dispatcher['store'] . '/cart';
        $form = new HTML_QuickForm('form', 'POST', $formpath, null, TRUE);

        $form->addElement('text', 'coupon_code', 'Coupon', array('class' => 'form-control', 'placeholder'=> 'Coupon Code', 'value'=>$_SESSION[$dispatcher['store']]['coupon_code']));
        $form->addElement('button', 'addcoupon', 'Apply', array('class' => 'btn btn-primary', 'role' => 'button', 'id'=>'addcoupon'));
        $form->addElement('textarea', 'ordernotes', 'Order notes', array('rows' => '2', 'class' => 'form-control'));

        //check if there's a coupon code in POST
        if (array_key_exists('coupon_code', $_POST) && !empty($_POST['coupon_code'])) {
            $ret = $this->checkPromoCodeStatus($_POST['coupon_code']);
            if (!empty($ret)) {
                $message = urlencode($ret);
                Header("Location:/" . $dispatcher['store'] . "/cart?message=" . $message);
                exit;
            }
        } else {
            if (isset($_SESSION[$dispatcher['action']]['messages']) && stripos($_SESSION[$dispatcher['action']]['messages'], 'Coupon code') !== false) {
                unset($_SESSION[$dispatcher['action']]['messages']);
            }
        }

        if (isset($_SESSION[$dispatcher['store']]['cartitems'])) {
            //check for remove / update button presses
            foreach ($_POST as $key => $value) {

                $keyexp = explode("_", $key);
                if ($keyexp[0] == 'remove') {
                    unset($_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]);
                    $itemremoved = true;
                } else if ($keyexp[0] == 'quantityinput') {
                    if ($value < 1) {
                        unset($_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]);
                    } else if ($_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]['type'] == 'p') {
                        $_SESSION[$dispatcher['store']]['cartitems'][$keyexp[1]]['quantity'] = $value;
                    }
                }
            }

            //add quantity input and remove button to cart items
            $itemType='';
            foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
                //find out if any of the products are physical ones
                $cartitems[$key]['product'] = getProduct($key);
                $itemType = $cartitem['type'];
                if ($this->applyconversion)
                    $cartitems[$key]['product']['price'] = $this->currencyConvert($cartitems[$key]['product']['price']);
                if ($cartitems[$key]['product']['memberdiscount'] == ''|| floatval($cartitems[$key]['product']['memberdiscount']) == 0) {
                    if ($this->memberdiscount) {
                        $cartitems[$key]['product']['memberdiscount'] = $this->memberdiscount;
                    } else {
                        $cartitems[$key]['product']['memberdiscount'] = 0;
                    }
                }
                if ($member)
                    $cartitems[$key]['product']['price'] = sprintf("%.2f", $cartitems[$key]['product']['price'] - ($cartitems[$key]['product']['price'] / 100 * $cartitems[$key]['product']['memberdiscount']));

                $cartitems[$key]['quantity'] = $cartitem['quantity'];

                if ($cartitem['type'] == 'p') {
                    $this->displayshippingrates = true;
                }

                for ($i = 1; $i <= 30; $i++) {
                    $qty[$i] = $i;
                }
                if ($cartitem['type'] == 'p') {
                    $form->addElement('select', 'quantityinput_' . $key, 'Quantity', $qty, array('id' => 'quantityinput_' . $key, 'class' => 'quantity form-control small-form-control'));
                } else {
                    $form->addElement('select', 'quantityinput_' . $key, 'Quantity', $qty, array('id' => 'quantityinput_' . $key, 'disabled' => 'true', 'class' => 'quantity form-control small-form-control'));
                    //$form->addElement('text','quantityinput_'.$key,null,array('size'=>'3','disabled'=>'true'));
                }

                if (strpos($_SERVER['HTTP_REFERER'], 'drumchannel.com') === FALSE) {
                    $form->addElement('submit', 'remove_' . $key, 'Remove', array('class' => 'btn btn-sm btn-primary py-2 px-3 small-form-control'));
                }
                //var_dump($cartitem['quantity']);
                //$form->addRule('quantityinput_'.$key, 'Enter a quantity', 'required', null, 'client');
                $form->setDefaults(array('quantityinput_' . $key => $cartitem['quantity']));

                //create the form element holders for smarty
                $cartitems[$key]['quantityinput'] = array('quantityinput_' . $key);

                if (strpos($_SERVER['HTTP_REFERER'], 'drumchannel.com') === FALSE) {
                    $cartitems[$key]['removebutton'] = array('remove_' . $key);
                }
            }
        }

        $notices = '';
        if ($itemType == 'd') {
            $productValidity = (int)(empty($settings->product_validity_days) ? 1 : (int)$settings->product_validity_days);
            $productlimitdays =  $productValidity > 2 ? $productValidity. " Days": ($productValidity * 24). " hours";
            $productLimit = (int)(empty($settings->product_download_limit) ? 1 : (int)$settings->product_download_limit);
    
            if ($productLimit > 3) {
                $productDownloadLimit = $productLimit." times";
            } 
            else {
                $productDownloadLimit = $productLimit == 1 ? "once" : "twice";
            } 
            $notices .= "*Downloads can only be purchased in single quantities. They can be downloaded {$productDownloadLimit} within {$productlimitdays} of purchase.";
            $tpl2->assign('notices', $notices);
        }

        //flatten the shipping rate array for display purposes
        $shippingrates = getShippingRates();
        if (count($shippingrates)) {
            foreach ($shippingrates as $data) {
                $shippingratesfordisplay[$data['id']] = $data['name'];
            }

            $form->addElement('select', 'shippingrate', 'Shipping Method', $shippingratesfordisplay, array('id' => 'shippingrate', 'class' => 'form-control '));
        } else {
            $this->displayshippingrates = false;
        }
        
        $form->addElement('submit', 'updatecart', 'Update Cart', array('class' => 'btn btn-primary my-1', 'role' => 'button'));
        $form->addElement('submit', 'checkout', 'Checkout', array('class' => 'btn btn-primary btn-block', 'role' => 'button'));
        $form->addElement('submit', 'returntoproducts', 'Continue Shopping', array('class' => 'btn btn-primary my-1', 'role' => 'button'));
        $form->setDefaults(array('ordernotes' => $_SESSION[$dispatcher['store']]['cart']['ordernotes'], 'shippingrate' => $_SESSION[$dispatcher['store']]['cart']['shippingrateid']));
        $this->form = $form;

        //get the totals
        //Orig Code $this->carttotals = $this->calculateCartTotals($_POST['shippingrate'], $this->forceaddtax);

        // For Cart $this->forceaddtax set to true to show all data in order summary
        // ORIG $this->carttotals = $this->calculateCartTotals($_POST['shippingrate'], 0);

        $shippingrate_id =  isset($_POST['shippingrate']) ? $_POST['shippingrate'] : $_SESSION[$dispatcher['store']]['cart']['shippingrateid'];
        $matchBillingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == $this->country || $_SESSION[$dispatcher['store']]['shipping_country'] == $this->country) ? 1 : 0;
        $matchCountryAndState = (($_SESSION[$dispatcher['store']]['billing_country'] == $this->country && $_SESSION[$dispatcher['store']]['billing_state'] == $this->state) || ($_SESSION[$dispatcher['store']]['shipping_country'] == $this->country && $_SESSION[$dispatcher['store']]['shipping_state'] == $this->state)) ? 1 : 0;
        $billingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['billing_country'] == "AU" || $_SESSION[$dispatcher['store']]['billing_country'] == "CA"
                            || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "AU" || $_SESSION[$dispatcher['store']]['shipping_country'] == "CA") ? 1 : 0;
        
        $isBackToCart = $_SESSION[$dispatcher['store']]['goback'] == "Back" ? 1 : 0;
        // $_SESSION[$dispatcher['store']]['billing_state'] = $isBackToCart ? '' : $_SESSION[$dispatcher['store']]['billing_state'];
        // $_SESSION[$dispatcher['store']]['billing_country'] = $isBackToCart ? '' : $_SESSION[$dispatcher['store']]['billing_country'];

        if ($billingCountry) {
            if (($matchCountryAndState && !$isBackToCart)) {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 1);
            } else {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 0);
            }
        } else {
            if ($matchBillingCountry && !$isBackToCart) {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 1);
            } else {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 0);
            }
        }
        $this->cartitems = $cartitems;

        //save the order notes
        $_SESSION[$dispatcher['store']]['cart']['ordernotes'] = $_POST['ordernotes'];
        $_SESSION[$dispatcher['store']]['coupon_code'] = $_POST['coupon_code'];

        if (isset($_POST['shippingrate'])) {
            //Jericho
            // check if $shippingrateid value is exist in the database vs array result of $shippingrates
            if(count($shippingrates) && $itemType == 'p') {
                $shippingrateid = $_POST['shippingrate'];
                $shippingrates_exist = false;
                foreach($shippingrates as $k => $val) {
                    if ($val['id'] == $shippingrateid && $shippingrateid != 0) {
                         $shippingrates_exist = true;
                        break;
                    } 
                }
            }
            $_SESSION[$dispatcher['store']]['cart']['shippingrateid'] = $_POST['shippingrate'];
        } else {
            reset($shippingrates);
            $defaultshippingrate = current($shippingrates);
            $_SESSION[$dispatcher['store']]['cart']['shippingrateid'] = $defaultshippingrate['id'];
        }

        //do this now as we'll have updated quantities even if the user has forgotten to click "update cart"
        if (isset($_POST['returntoproducts'])) {
            Header("Location:/" . $dispatcher['store'] . "/");
            die();
        } 
        else if (isset($_POST['checkout'])) {
            $totalproductprice = 0.00; // 
           foreach($cartitems as $k=>$v) { // check cart items if product total price is zero  
               $totalproductprice = floatval($totalproductprice) + floatval($v['product']['price']);
           }
           
            if(count($shippingrates) && $itemType == 'p') {
                if(!$shippingrates_exist) {
                    //Jericho
                   // add this section to avoid manipulation of selected input value
                   // In case they turn off their javascript in the browser, need to check if shipping rate is exist
                   // check if $shippingrateid value is exist in database return to cart
                    $this->message = 'An error has occured with your choice of shipping method. Please select the correct shipping method and try again.';
                } else {
                    Header("Location:/" . $dispatcher['store'] . "/checkout");
                    die();
                }
           } 
           else {
            //$this->processCheckoutSuccess();
            if($totalproductprice <= 0) {
                Header("Location:/" . $dispatcher['store'] . "/checkout/payment"); // EVO-4401   Allow it to accept 100% member discount.
                die();
            } else {
                Header("Location:/" . $dispatcher['store'] . "/checkout");
                die();
            }

           }
        }

        $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
        $this->form->accept($renderer);
        $this->form = $renderer->toArray();
        $this->template = 'cart.tpl';
    }

    function showStaticCart() {
        global $dispatcher, $member, $tpl2;

        //see if we're dealing with a saved cart - NOT NEEDED - VIEW ORDERS DOES THIS
        /* if(isset($_SESSION[$dispatcher['store']]['cart']['checkoutcurrency']))
          {
          //we are - don't perform a currency conversion - just get the currency symbol right
          switch($_SESSION[$dispatcher['store']]['cart']['checkoutcurrency'])
          {
          case 'GBP':
          $this->currencysymbol = "&pound;";
          break;
          case 'USD':
          $this->currencysymbol = "$";
          break;
          case 'EUR':
          $this->currencysymbol = "&euro;";
          break;
          }

          $this->carttotals = $_SESSION[$dispatcher['store']]['cart']['carttotals'];

          }
          else
          { */
        //ORIG $this->carttotals = $this->calculateCartTotals($_SESSION[$dispatcher['store']]['cart']['shippingrateid'], 1);
        $otherstatetax = (float) $this->otherstatetax;
        $includeStateTaxCountry = $_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" ? 1: 0;
        $shippingrate_id =  $_SESSION[$dispatcher['store']]['cart']['shippingrateid'];
        $matchBillingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == $this->country || $_SESSION[$dispatcher['store']]['shipping_country'] == $this->country) ? 1 : 0;
        $matchCountryAndState = (($_SESSION[$dispatcher['store']]['billing_country'] == $this->country && $_SESSION[$dispatcher['store']]['billing_state'] == $this->state) || ($_SESSION[$dispatcher['store']]['shipping_country'] == $this->country && $_SESSION[$dispatcher['store']]['shipping_state'] == $this->state)) ? 1 : 0;
        $billingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['billing_country'] == "AU" || $_SESSION[$dispatcher['store']]['billing_country'] == "CA"
                            || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "AU" || $_SESSION[$dispatcher['store']]['shipping_country'] == "CA") ? 1 : 0;

        $this->isOtherCountryStateTax = (($includeStateTaxCountry && $otherstatetax) && $matchBillingCountry && !$matchCountryAndState ) ? 1 : 0;
        if ($billingCountry) {
            if (($otherstatetax > 0 && $includeStateTaxCountry) || ($matchCountryAndState)) {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 1);
            } else {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 0);
            }
        } else {
            if ($matchBillingCountry) {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 1);
            } else {
                $this->carttotals = $this->calculateCartTotals($shippingrate_id, 0);
            }
        }
        //vd::dump($this->cartotals);


        foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
            $cartitems[$key]['product'] = getProduct($key);

            //if the cart is live and the currency in use is different to the store currency
            if ($this->applyconversion && !isset($_SESSION[$dispatcher['store']]['cart']['checkoutcurrency'])) {
                //perform the currency conversion
                $cartitems[$key]['product']['price'] = $this->currencyConvert($cartitems[$key]['product']['price']);
            }

            if ($member) {
                if ($cartitems[$key]['product']['memberdiscount'] == '' || floatval($cartitems[$key]['product']['memberdiscount']) == 0) {
                    if ($this->memberdiscount) {
                        $cartitems[$key]['product']['memberdiscount'] = $this->memberdiscount;
                    } else {
                        $cartitems[$key]['product']['memberdiscount'] = 0;
                    }
                }
                $cartitems[$key]['product']['price'] = sprintf("%.2f", $cartitems[$key]['product']['price'] - ($cartitems[$key]['product']['price'] / 100 * $cartitems[$key]['product']['memberdiscount']));
            }

            $cartitems[$key]['quantity'] = $cartitem['quantity'];
        }

        $this->cartitems = $cartitems;

        $shippingrate = getShippingRate($_SESSION[$dispatcher['store']]['cart']['shippingrateid']);
        $this->shippingratename = $shippingrate['name'];

        $this->ordernotes = $_SESSION[$dispatcher['store']]['cart']['ordernotes'];
        $tpl2->assign('dispatcher', $dispatcher);
        $tpl2->assign_by_ref('content', $this);
        return $tpl2->fetch('staticcart.tpl');
    }

    function calculateCartTotals($shippingrateid, $forceaddtax) {
        global $dispatcher, $member, $db;

        // Other State Tax US only
        $taxAmountUse = $this->isOtherCountryStateTax ? $this->otherstatetax : $this->tax;
        if (isset($_SESSION[$dispatcher['store']]['cartitems'])) {

            $subtotal = 0;
            $producttax = 0;

            //find out the quantity of shippable items and calculate the product totals
            foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $value) {
                $product = getProduct($key);

                if (isset($this->applyconversion))
                    $product['price'] = $this->currencyConvert($product['price']);
                if ($member) {
                    if ($product['memberdiscount'] == '' || floatval($product['memberdiscount']) == 0) {
                        if ($this->memberdiscount) {
                            $product['memberdiscount'] = $this->memberdiscount;
                        } else {
                            $product['memberdiscount'] = 0;
                        }
                    }
                    $product['price'] = $product['price'] - ($product['price'] / 100 * $product['memberdiscount']);
                }

                if ($product['type'] == 'p')
                    $physicalquantity += $value['quantity'];
                $subtotal += $product['price'] * $value['quantity'];

                //calculate the tax on products for non tax exempt items
                if (!$product['taxexempt']) {
                    // Orig code as of oct 27 2020 $producttax += sprintf("%.2f", (($product['price'] * $value['quantity']) / 100) * $this->tax);
                    $producttax += sprintf("%.2f", (($product['price'] * $value['quantity']) / 100) * $taxAmountUse);
                }
            }

            $shippingrates = array();

            //work out the cost of shipping for each rate
            foreach (getShippingRates() as $shippingkey => $shippingvalue) {
                if ($this->shippingcharge == 'PC') {  //percent discount for multiple items
                    $costforproducts = 0;

                    //work out if we only have one of one item or only one physical item
                    if (isset($physicalquantity) && $physicalquantity == 1) {
                        //find the first (and only) physical product
                        foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
                            //work out the cost for the first (and only) item in the shopping cart
                            if ($cartitem['type'] == 'p')
                                $costforrate = getShippingRateForProductByRateId($key, $shippingvalue['id']);
                        }
                    }
                    //or work out the shipping for multiple items
                    else {
                        foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
                            if ($cartitem['type'] == 'p') {
                                $costforproducts += getShippingRateForProductByRateId($cartitem['id'], $shippingvalue['id']) * $cartitem['quantity'];
                            }
                        }

                        //apply the percentage discount
                        $costforrate = round($costforproducts - ($costforproducts / 100 * $shippingvalue['percentdiscount']), 2);
                    }

                    $shippingrates[$shippingvalue['id']]['cost'] = $costforrate;
                } else {  //per basket
                    if ($physicalquantity > 0) {
                        $shippingrates[$shippingvalue['id']]['cost'] = $shippingvalue['basketcost'];
                    }
                }
            }

            //apply their shipping rate or the default one
            if (isset($shippingrateid)) {
                $shippingcost = $shippingrates[$shippingrateid]['cost'];
            } else {
                reset($shippingrates);
                $shippingrate = current($shippingrates);
                $shippingcost = $shippingrate['cost'];
            }

            if ($this->applyconversion) {
                $shippingcost = $this->currencyConvert($shippingcost);
            }
        }

        //calculate total
        $totals = array();
        if (!empty($_SESSION[$dispatcher['store']]['coupon_code'])) {
            $discount = $this->calcAmountWithPromoCode($_SESSION[$dispatcher['store']]['coupon_code'], $subtotal);
            $totals['discount'] = $discount;
            $totals['subtotal'] = sprintf("%.2f", $subtotal - ($subtotal * $discount) / 100);
            //orig code as oct 27 2020 $producttax = ($totals['subtotal'] * $this->tax) / 100;
            $producttax = ($totals['subtotal'] * $taxAmountUse) / 100;
        } else {
            $totals['subtotal'] = sprintf("%.2f", $subtotal);
        }

        $totals['shippingcost'] = sprintf("%.2f", $shippingcost);

        //adjust the tax rate
        if ($forceaddtax) {
            //orig code as oct 27 2020 $totals['totaltax'] = !$this->excludeshippingcosttax ? sprintf("%.2f", (($shippingcost / 100) * $this->tax) + $producttax) : sprintf("%.2f", $producttax);
            $totals['totaltax'] = (!$this->excludeshippingcosttax && $producttax > 0)? sprintf("%.2f", (($shippingcost / 100) * $taxAmountUse) + $producttax) : sprintf("%.2f", $producttax);
            $totals['total'] = sprintf("%.2f", $totals['subtotal'] + $totals['shippingcost'] + $totals['totaltax']);
        } else if (isset($_SESSION[$dispatcher['store']]['billing_country']) && !empty($_SESSION[$dispatcher['store']]['billing_country'])) {
            if ($this->addTaxForRegion($_SESSION[$dispatcher['store']]['billing_country'], $_SESSION[$dispatcher['store']]['billing_state'])) {
                //orig code as oct 27 2020 $totals['totaltax'] = !$this->excludeshippingcosttax ? sprintf("%.2f", (($shippingcost / 100) * $this->tax) + $producttax) : sprintf("%.2f", $producttax);
                $totals['totaltax'] = (!$this->excludeshippingcosttax && $producttax > 0) ? sprintf("%.2f", (($shippingcost / 100) * $taxAmountUse) + $producttax) : sprintf("%.2f", $producttax);
                $totals['total'] = sprintf("%.2f", $totals['subtotal'] + $totals['shippingcost'] + $totals['totaltax']);
            } else {
                $totals['total'] = sprintf("%.2f", $totals['subtotal'] + $totals['shippingcost']);
            }
        } 
        else {
            $totals['total'] = sprintf("%.2f", $totals['subtotal'] + $totals['shippingcost']);
        }
        if (!isset($totals['totaltax'])) {
            $totals['totaltax'] = 0;
        }
 
        return $totals;
    }
    
    function notifyDev($param, $product) {
       if ($_POST['type'] == 'a' && (!isset($product['articleid']) || $product['articleid'] == 0)) {
            mail ('joan@subhub.com', SITE_NAME .' error occur while trying to add a ppv please check the record', 
                "\n Param ". $param . "\n Product". print_r($product, true));
        }
    }

    function processProductImage($productid) {
        global $dispatcher;
        //check the store image directories exist
        if (!file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'])) {
            //if not, create them
            mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'], 0775);
            mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images', 0775);
            mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full', 0775);
            mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/thumb', 0775);
        }

        require_once 'Image/Transform.php';

        $largeimage = &Image_Transform::factory('GD');
        $largeimage->load($_FILES['image']['tmp_name']);

        if ($largeimage->getImageWidth() > $FULL_SIZE_X) {
            $largeimage->scaleByX($FULL_SIZE_X);
        }
        if ($largeimage->getImageHeight() > $FULL_SIZE_Y) {
            $largeimage->scaleByY($FULL_SIZE_Y);
        }

        $largeimage->save(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full/' . $productid . '_large.jpg', 'jpg');
        unset($largeimage);

        $fullimage = &Image_Transform::factory('GD');
        $fullimage->load($_FILES['image']['tmp_name']);

        if ($fullimage->getImageWidth() > $IMAGE_SIZE_X) {
            $fullimage->scaleByX($IMAGE_SIZE_X);
        }
        
        if ($fullimage->getImageHeight() > $IMAGE_SIZE_Y) {
            $fullimage->scaleByY($IMAGE_SIZE_Y);
        }

        $fullimage->save(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full/' . $productid . '_full.jpg', 'jpg');
        unset($fullimage);

        $thumbnailimage = &Image_Transform::factory('GD');
        $thumbnailimage->load($_FILES['image']['tmp_name']);

        //create the thumbnail
        if ($thumbnailimage->getImageWidth() > $THUMBNAIL_SIZE_X) {
            $thumbnailimage->scaleByX($THUMBNAIL_SIZE_X);
        }
        
        if ($thumbnailimage->getImageHeight() > $THUMBNAIL_SIZE_Y) {
            $thumbnailimage->scaleByY($THUMBNAIL_SIZE_Y);
        }

        $thumbnailimage->save(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/thumb/' . $productid . '_thumb.jpg', 'jpg');
        unset($thumbnailimage);

        //delete the temporary image file
        @unlink($_FILES['image']['tmp_name']);
        
    }
    
    function getProductDetailsFromPOST() {
        foreach ($_POST as $key => $value) {
            $keyexp = explode("_", $key);

            //strip out the shipping rates - they go in a different table
            if ($keyexp[0] == "productshippingrate") {
                $this->productshippingrates[$keyexp[1]] = filter_var($value, FILTER_SANITIZE_STRING);
            } 
            elseif ($keyexp[0] == "description" || $keyexp[0] == "teaser") {
                $product[$key] = htmlentities($value, ENT_QUOTES, "UTF-8");
            } 
            else {
                $product[$key] = filter_var($value, FILTER_SANITIZE_STRING);
            }
        }
            
       

        if (!isset($product['categoryid']) || empty($product['categoryid'])) {
            $product['categoryid'] = 0;
        }

        if (!isset($product['memberdiscount']) || empty($product['memberdiscount']) || (floatval($product['memberdiscount']) == 0)) {
            $product['memberdiscount'] = 0;
        }

        if (!isset($product['teaser']) || empty($product['teaser'])) {
            $product['teaser'] = '';
        }
        return $product;
    }
    
    function addProduct() {
        global $dispatcher, $db, $settings;

        $FULL_SIZE_X = 800;
        $IMAGE_SIZE_X = 400;
        $THUMBNAIL_SIZE_X = 250;

        $FULL_SIZE_Y = 600;
        $IMAGE_SIZE_Y = 500;
        $THUMBNAIL_SIZE_Y = 350;


        if (isset($_POST['cancel'])) {
            Header("Location:/" . $dispatcher['store'] . "/" . $id);
            die();
        }

        $formpath = '/' . $dispatcher['store'] . '/addproduct';
        $form = new HTML_QuickForm('addproductform', 'POST', $formpath, null, TRUE);

        $form->addElement('text', 'name', 'Name', array('size' => '60', 'class' => 'form-control'));
        $form->setMaxFileSize(100 * 1024 * 1024);

        $this->categories = getCategories();

        $categoryarray = null;
        $categorytree = getSubCategoriesByCategory(0, 'ARRAY', $categoryarray);
        $categoryflat = null;
        $categoriesflat = array();
        $categoriesfordisplay = categoriesToDisplayArray($categorytree, 0, $categoryflat);
        unset($categoriesfordisplay[0]);

        if ($this->categories) {
            foreach ($this->categories as $key => $category) {
                $categorynames[$category['id']] = $category['name'];
            }

            //indent the subcategories for the drop down
            $categoriesflat[0] = "None";
            foreach ($categoriesfordisplay as $key => $depthcount) {
                for ($i = 1; $i < $depthcount; $i++) {
                    $spacing .= "&nbsp;&nbsp;&nbsp;&nbsp;";
                }

                $categoriesflat[$key] = $spacing . $categorynames[$key];
                unset($spacing);
            }

            //	$categoriesflat = array(''=>"None") + $categoriesflat;
            //$categoriesflat = array_merge(array(''=>"None")  ,$categoriesflat);
        } else {
            $categoriesflat = array('' => "None");
        }

        foreach ($this->categories as $key => $category) {
            $categorynames[$category['id']] = $category['name'];
        }

        $form->addElement('select', 'categoryid', 'Category', $categoriesflat, array('class' => 'form-control'));

        $typeRadios[] = &HTML_QuickForm::createElement('radio', null, null, 'Physical Product', 'p', array("onclick" => "showProduct();"));
        $typeRadios[] = &HTML_QuickForm::createElement('radio', null, null, 'Download', 'd', array("onclick" => "showDownload();"));
        if ($settings->evo['ppv'] == '1') {
            $typeRadios[] = &HTML_QuickForm::createElement('radio', null, null, 'Article', 'a', array("onclick" => "showArticle();"));
            $ppv_articles = Articles::getPPVArticles();
            if (!$ppv_articles)
                $ppv_articles = array('0' => 'No pay-per-view articles available');
            $form->addElement('select', 'articleid', 'Article', $ppv_articles, array('class' => 'form-control'));
        }
//vd::dump($typeRadios);
        $form->addGroup($typeRadios, 'type', null, '<br />');

        $form->addElement('file', 'downloaddata', 'Download');
        $form->addRule('downloaddata', 'Too big', 'maxfilesize', 100 * 1024 * 1024);
        $form->addElement('file', 'downloadpreview', 'Download Preview');

        $form->addElement('text', 'accesstime', 'Access Time Limit', array('size' => '2', 'class' => 'form-control'));
        $form->addElement('text', 'accesslimit', 'Access Number Limit', array('size' => '2', 'class' => 'form-control'));

        $form->addElement('textarea', 'teaser', 'Teaser', array('cols' => '60', 'rows' => '6', 'class' => 'editor_simple'));
        $form->addElement('textarea', 'description', 'Description', array('cols' => '60', 'rows' => '6', 'class' => 'editor_simple'));

        $form->addElement('text', 'price', 'Price', array('size' => '6', 'class' => 'form-control'));
        $form->addElement('checkbox', 'taxexempt', 'Tax Exempt?');
        $form->addElement('text', 'memberdiscount', 'Member Discount', array('size' => '3', 'class' => 'form-control'));

        $form->addElement('text', 'meta_title', 'Title Tag', array('size' => '3', 'class' => 'form-control'));
        $form->addElement('text', 'meta_description', 'Description Tag ', array('size' => '3', 'class' => 'form-control'));

        //if the store is set to charge a percent discount, give the shipping rate input boxes
        if ($this->shippingcharge == 'PC') {
            $shippingrates = getShippingRates();
            $shippingvalues = array();
            foreach ($shippingrates as $key => $shippingrate) {
                $form->addElement('text', 'productshippingrate_' . $shippingrate['id'], $shippingrate['name'], array('size' => '6', 'class' => 'form-control'));
                $this->shippingratesholder[$key] = array('productshippingrate_' . $shippingrate['id']);
                $shippingvalues['productshippingrate_' . $shippingrate['id']] = $shippingrate['defaultitemcost'];
            }
        }

        $form->addElement('file', 'image', 'Product Image');
        $form->addElement('text', 'stock', 'Number in Stock', array('size' => '6', 'class' => 'form-control'));
        $form->addElement('select', 'visibility', 'Viewable by', array('PUBLIC' => 'Everyone', 'PRIVATE' => 'Administrators', 'MEMBERS' => 'Members Only'), array('class' => 'form-control'));
        $form->addElement('submit', 'cancel', 'Cancel');
        $form->addElement('submit', 'submit', 'Add Product', array('class' => 'btn btn-small btn-primary'));

        //validation rules
        $form->addRule('name', 'Enter a name', 'required', null, 'client');
        $form->addRule('price', 'Enter a price for the product', 'required', null, 'client');
        $form->addRule('image', 'Must be a image', 'mimetype', array('image/jpeg', 'image/gif', 'image/jpg', 'image/png',
            'image/pjpeg', 'image/x-png'));

        //defaults
        $form->setDefaults($shippingvalues);
        $form->setDefaults(array("type" => "p", "tax" => $this->tax, "price" => "0.00", "accesstime" => "0", "accesslimit" => "0"));

        $this->form = $form;
        
        //($_POST['type'] == 'd') ? $downloadisvalid = $_FILES['downloaddata']['size'] : $downloadisvalid = 1;

        if ($this->form->validate()) {
            
            $downloadisvalid = 1;
            if ($_POST['type'] == 'd') {
                $downloadisvalid = ($_FILES['downloaddata']['name'] != '') ? 1 : 0;
            } 
                      
            $hasimage = 0;
            if ($_FILES['image']['size']) {
                $hasimage = 1;
            } 
            
            $product = $this->getProductDetailsFromPOST();

            $product['meta_title'] = htmlspecialchars($product['meta_title'], ENT_QUOTES);
            $product['meta_description'] = htmlspecialchars($product['meta_description'], ENT_QUOTES);

            $sql = sprintf("INSERT INTO store_products(storeid, categoryid, name, type, teaser, description, price,taxexempt, hasimage, visibility, weight, stock, memberdiscount, discontinued, meta_title, meta_description) "
                . "VALUES ('%d','%d','%s','%s','%s','%s','%s',%d,'%s','%s',NULL,%d,'%s','0', '%s', '%s')", 
                $this->id, $product['categoryid'], $product['name'], $product['type'], $product['teaser'], $product['description'], sprintf("%.2f", $product['price']), $product['taxexempt'], $hasimage, $product['visibility'], $product['stock'], $product['memberdiscount'], $product['meta_title'], $product['meta_description']);
             $db->Execute($sql);
            $productsqlresult = $productid = $db->Insert_ID();

            if (!$productsqlresult) {
                $this->notifyDev($sql, $product);
            }

            $sql = "SELECT * FROM store_products_categories WHERE category_id = 0";

            $catorders = $db->getAll($sql);
            if ($catorders) {
                $sql = "UPDATE store_products_categories SET store_products_categories.order = store_products_categories.order + 1 WHERE category_id = 0";
                $db->Execute($sql);
                $sql = sprintf("INSERT INTO store_products_categories (product_id,category_id,store_products_categories.order) VALUES (%s,%s,%s)", $productid, 0, 1);

                $db->Execute($sql);
                if ($product['categoryid']) {
                    //Check to see if this category has already been sorted
                    $sql = sprintf("SELECT * FROM store_products_categories WHERE category_id = %s", $product['categoryid']);
                    $catsort = $db->getAll($sql);
                    if ($catsort) {
                        $sql = sprintf("UPDATE store_products_categories SET store_products_categories.order = store_products_categories.order + 1 WHERE category_id = %s", $product['categoryid']);
                        $db->Execute($sql);
                        $sql = sprintf("INSERT INTO store_products_categories (product_id,category_id,store_products_categories.order) VALUES (%s,%s,%s)", $productid, $product['categoryid'], 1);
                        $db->Execute($sql);
                    }
                }
            }

            if ($hasimage) {
                $this->processProductImage($productid);
            }

            $shippingratesqlresult = 1;
            if (count($this->productshippingrates) && $product['type'] == 'p') {
                foreach ($this->productshippingrates as $rateid => $cost) {
                    
                    $shippingratesql = sprintf("INSERT INTO store_shipping_rates_product(productid, shippingrateid, cost)
                        VALUES('%d','%d','%s')", $productid, $rateid, $cost);
                    
                    $db->Execute($shippingratesql);
                    
                    $shippingratesqlresult = $db->Insert_ID();
                    
                    if (!$shippingratesqlresult) {
                        $this->notifyDev($shippingratesql, $product);
                    }
                }
            } 
            
            $downloadsqlresult = 1;
            if ($_POST['type'] == 'd' && $productsqlresult) {
                if (!file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/storefiles/')) {
                    //if not, create it
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/storefiles', 0775);
                }
                $uploaddir = dirname($_SERVER['SCRIPT_FILENAME']) . '/storefiles/';

                if ($_FILES['downloadpreview']['size']) {
                    $downloadpreiviewfile = $uploaddir . $productid . '_preview';
                    $downloadpreviewsize = $db->qstr($_FILES['downloadpreview']['size']);
                    $downloadpreviewtype = $db->qstr($_FILES['downloadpreview']['type']);
                    $downloadpreviewname = $db->qstr($_FILES['downloadpreview']['name']);
                    if (move_uploaded_file($_FILES['downloadpreview']['tmp_name'], $downloadpreiviewfile)) {
                        $ddp_success = 1;
                        $extensions = explode('/',$downloadpreviewtype);  
                        if($extensions) { 
                            $ext = str_replace("'","",$extensions[1]);                   
                            if($this->isImage(trim($ext))) {
                                //make a copy of image file to display in preview
                                copy($downloadpreiviewfile, dirname($_SERVER['SCRIPT_FILENAME']). '/custom/' . 'store' . '/images/full/' . $productid  . '_fullpreview.jpg');
                            }
                        }
                    } else {
                        $ddp_success = 0;
                    }
                } 
                else {
                    $downloadpreviewsize = "'". $downloadpreviewsize ."'";
                    $downloadpreviewtype = "'". $downloadpreviewtype ."'";
                    $downloadpreviewname = "'". $downloadpreviewname ."'"; 
                }

                $downloaddatafile = $uploaddir . $productid;
                $downloaddatasize = $db->qstr($_FILES['downloaddata']['size']);
                $downloaddatatype = $db->qstr($_FILES['downloaddata']['type']);
                $downloaddataname = $db->qstr($_FILES['downloaddata']['name']);
                if (move_uploaded_file($_FILES['downloaddata']['tmp_name'], $downloaddatafile)) {
                    $dd_success = 1;
                } else {
                    $dd_success = 0;
                }

                @unlink($_FILES['downloadpreview']['tmp_name']);
                @unlink($_FILES['downloaddata']['tmp_name']);


                $downloadsql = sprintf("INSERT INTO store_downloads (productid, preview, previewsize,previewtype, "
                    . "previewname,data, datasize, datatype, dataname) "
                    . "VALUES('%d',  'FS', %s, %s, %s, 'FS', %s, %s, %s)", 
                    $productid, $downloadpreviewsize, $downloadpreviewtype, $downloadpreviewname, $downloaddatasize, $downloaddatatype, $downloaddataname);

                $db->Execute($downloadsql);
                
                $downloadsqlresult = $db->Insert_ID();
                if (!$productsqlresult) {
                    $this->notifyDev($downloadsql, $product);
                }
                //vd::dump(ADODB_Pear_Error());
            } 

            $articlesqlresult = 1;
            if ($_POST['type'] == 'a' && $productsqlresult) {
                $articlesql = sprintf("INSERT INTO store_articles 
                    (productid, articleid, accesstime, accesslimit) VALUES
                    ('%d', '%s', '%d','%d')", 
                    $productid, 
                    $product['articleid'], 
                    $product['accesstime'], 
                    $product['accesslimit']);

                $db->Execute($articlesql);
                $articlesqlresult = $db->Insert_ID();
                
                if (!$articlesqlresult) {
                    $this->notifyDev($articlesql, $product);
                }
                //vd::dump(ADODB_Pear_Error());
            }
            
            if ($_POST['type'] == 'a' && $productsqlresult && $articlesqlresult) {
                $success = true;
            }
            elseif ($_POST['type'] == 'd' && $productsqlresult && $downloadsqlresult) {
                $success = true;
            }
            elseif ($_POST['type'] == 'p' && $productsqlresult && $shippingratesqlresult) {
                $success = true;
            }
            else {
                $this->message = "Adding product failed.  Please contact support@subhub.com";
                $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
                $this->form->accept($renderer);
                $this->form = $renderer->toArray();
                $this->template = 'addproduct.tpl';
            }
            
            Header("Location:/" . $dispatcher['store'] . "/products/" . $productid); exit;
            
        } elseif ($this->form->validate() && !$downloadisvalid) {
            $this->message = "Invalid file for download.";
            $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            $this->form->accept($renderer);
            $this->form = $renderer->toArray();
            $this->template = 'addproduct.tpl';
        } else {
            $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            $this->form->accept($renderer);
            $this->form = $renderer->toArray();
            $this->template = 'addproduct.tpl';
        }
    }

    function editProduct($product) {
        global $dispatcher, $db, $settings;

        $FULL_SIZE_X = 800;
        $IMAGE_SIZE_X = 400;
        $THUMBNAIL_SIZE_X = 250;

        $FULL_SIZE_Y = 600;
        $IMAGE_SIZE_Y = 500;
        $THUMBNAIL_SIZE_Y = 350;

        if (isset($_POST['cancel'])) {
            Header("Location:/" . $dispatcher['store'] . "/products/" . $dispatcher['product']);
            die();
        } else if (isset($_POST['configure'])) {
            Header("Location:/" . $dispatcher['store'] . "/configure/" . $id);
            die();
        } else if (isset($_POST['deletepreview'])) {
            deleteDownloadPreview($dispatcher['product']);
        } else if (isset($_POST['deleteimage'])) {
            deleteProductImage($dispatcher['product']);
            $product['hasimage'] = 0;
        }

        $formpath = '/' . $dispatcher['store'] . "/products/" . $dispatcher['product'] . '/edit';
        $form = new HTML_QuickForm('form', 'POST', $formpath, null, TRUE);

        $form->addElement('text', 'name', 'Name', array('size' => '60', 'class' => 'form-control'));

        $this->categories = getCategories();

        $categoryarray = null;
        $categorytree = getSubCategoriesByCategory(0, 'ARRAY', $categoryarray);
        $categoriesflat = array();
        $categoriesfordisplay = categoriesToDisplayArray($categorytree, 0, $categoryflat);
        unset($categoriesfordisplay[0]);

        if ($this->categories) {
            $categoriesflat[''] = "None";
            foreach ($this->categories as $key => $category) {
                $categorynames[$category['id']] = $category['name'];
            }

            //indent the subcategories for the drop down
            foreach ($categoriesfordisplay as $key => $depthcount) {
                for ($i = 1; $i < $depthcount; $i++) {
                    $spacing .= "&nbsp;&nbsp;&nbsp;&nbsp;";
                }

                $categoriesflat[$key] = $spacing . $categorynames[$key];
                unset($spacing);
            }
            // this bombs out when categoriesflat is not what you expect
            //$categoriesflat = array(''=>"None") + $categoriesflat;
            //$categoriesflat = array_merge(array(''=>"None")  ,$categoriesflat);
        } else {
            $categoriesflat = array('' => "None");
        }
        //vd::dump($categoriesflat);

        $form->addElement('select', 'categoryid', 'Category', $categoriesflat, array('class' => 'form-control'));

        $typeRadios[] = &HTML_QuickForm::createElement('radio', null, null, 'Physical Product', 'p', array("onclick" => "showProduct();"));
        $typeRadios[] = &HTML_QuickForm::createElement('radio', null, null, 'Download', 'd', array("onclick" => "showDownload();"));
        if ($settings->usesppv == '1') {
            $typeRadios[] = &HTML_QuickForm::createElement('radio', null, null, 'Article', 'a', array("onclick" => "showArticle();"));
            $ppv_articles = Articles::getPPVArticles();
            if (!$ppv_articles)
                $ppv_articles = array('0' => 'No pay-per-view articles available');
            $form->addElement('select', 'articleid', 'Article', $ppv_articles, array('class' => 'form-control'));
        }
        $form->addGroup($typeRadios, 'type', null, '<br />');

        $form->addElement('file', 'downloaddata', 'Download');
        $form->addElement('file', 'downloadpreview', 'Download Preview');

        $form->addElement('text', 'accesstime', 'Access Time Limit', array('size' => '2', 'class' => 'form-control'));
        $form->addElement('text', 'accesslimit', 'Access Number Limit', array('size' => '2', 'class' => 'form-control'));

        $form->addElement('textarea', 'teaser', 'Teaser', array('cols' => '60', 'rows' => '6', 'class' => 'editor_simple'));
        $form->addElement('textarea', 'description', 'Description', array('cols' => '60', 'rows' => '6', 'class' => 'editor_simple'));

        $form->addElement('text', 'price', 'Price', array('size' => '6', 'class' => 'form-control'));
        $form->addElement('checkbox', 'taxexempt', 'Tax Exempt?');
        $form->addElement('text', 'memberdiscount', 'Member Discount', array('size' => '3', 'class' => 'form-control'));

        $form->addElement('text', 'meta_title', 'Title Tag', array('size' => '3', 'class' => 'form-control'));
        $form->addElement('text', 'meta_description', 'Description Tag ', array('size' => '3', 'class' => 'form-control'));

        //if the store is set to charge a percent discount, get the shipping rate values for the product
        if ($this->shippingcharge == 'PC') {
            $shippingrates = getShippingRatesForProduct($product['id']);
            $shippingvalues = array();
            foreach ($shippingrates as $key => $shippingrate) {
                $form->addElement('text', 'productshippingrate_' . $shippingrate['shippingrateid'], $shippingrate['name'], array('size' => '6', 'class' => 'form-control'));
                $this->shippingratesholder[$key] = array('productshippingrate_' . $shippingrate['shippingrateid']);
                $shippingvalues['productshippingrate_' . $shippingrate['shippingrateid']] = $shippingrate['cost'];
            }
        }

        $form->addElement('file', 'image', 'Product Image');
        $form->addElement('text', 'stock', 'Number in Stock', array('size' => '6', 'class' => 'form-control'));
        $form->addElement('select', 'visibility', 'Viewable by', array('PUBLIC' => 'Everyone', 'PRIVATE' => 'Administrators', 'MEMBERS' => 'Members Only'), array('class' => 'form-control'));
        //$form->addElement('submit','configure','No categories found. Click to add some');
        $form->addElement('submit', 'deletepreview', 'Delete', array('class' => 'btn btn-danger'));
        $form->addElement('submit', 'deleteimage', 'Delete', array("class" => "btn btn-danger", "onclick" => "confirmDeleteProductImage()"));
        $form->addElement('submit', 'cancel', 'Cancel', array("class" => "btn btn-primary"));
        $form->addElement('submit', 'submit', 'Update Product', array("class" => "btn btn-success"));

        //validation rules
        $form->addRule('name', 'Enter a name', 'required', null);
        $form->addRule('price', 'Enter a price for the product', 'required', null);
        $form->addRule('image', 'Must be a image', 'mimetype', array('image/jpeg', 'image/gif', 'image/jpg', 'image/png',
            'image/pjpeg', 'image/x-png'));
        
        
        if (array_key_exists('description', $product)) {
            $product['description'] = htmlspecialchars_decode($product['description'], ENT_QUOTES); 
        }
        
        if (array_key_exists('teaser', $product)) {
            $product['teaser'] = htmlspecialchars_decode($product['teaser'], ENT_QUOTES); 
        }
        
        //defaults
        $form->setDefaults($shippingvalues);
        $form->setDefaults($product);
        $this->product = $product;

        if ($product['type'] == 'a') {
            $article = getArticleForProduct($product['id']);
            if (!empty($article)) {
                $form->setDefaults(array('articleid' => $article['articleid'], 'accesstime' => $article['accesstime'],
                    'accesslimit' => $article['accesslimit']));
            } else {
                $form->setDefaults(array('articleid' => $_POST['articleid'], 'accesstime' => $_POST['accesstime'],
                    'accesslimit' => $_POST['accesslimit']));
            }
        }


        $this->form = $form;

        //Check download is valid
        if ($_POST['type'] == 'd') {
           $downloadisvalid = (checkProductHasDownload($product['id'])) ? 1 :  $_FILES['downloaddata']['size'];
        } else {
            $downloadisvalid = 1;
        }

        if ($this->form->validate() && isset($_POST['submit'])) {
            foreach ($_POST as $key => $value) {
                $keyexp = explode("_", $key);

                //strip out the shipping rates - they go in a different table
                if ($keyexp[0] == "productshippingrate") {
                    $productshippingrates[$keyexp[1]] = $value;
                } elseif ($keyexp[0] == "description" || $keyexp[0] == "teaser") {
                    $product[$key] = htmlspecialchars_decode($value, ENT_QUOTES);
                } else {
                    $product[$key] = filter_var($value, FILTER_SANITIZE_STRIPPED);
                }
            }

            if (!isset($_POST['taxexempt']))
                $product['taxexempt'] = 0;

            //check if we have new image files
            if ($_FILES['image']['size']) {
                //check the store image directories exist
                if (!file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'])) {
                    //if not, create them
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'], 0775);
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images', 0775);
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full', 0775);
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/thumb', 0775);
                }

                require_once 'Image/Transform.php';

                $largeimage = &Image_Transform::factory('GD');
                $largeimage->load($_FILES['image']['tmp_name']);

                if ($largeimage->getImageWidth() > $FULL_SIZE_X)
                    $largeimage->scaleByX($FULL_SIZE_X);
                if ($largeimage->getImageHeight() > $FULL_SIZE_Y)
                    $largeimage->scaleByY($FULL_SIZE_Y);

                $largeimage->save(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full/' . $product['id'] . '_large.jpg', 'jpg');
                unset($largeimage);

                $fullimage = &Image_Transform::factory('GD');
                $fullimage->load($_FILES['image']['tmp_name']);

                if ($fullimage->getImageWidth() > $IMAGE_SIZE_X)
                    $fullimage->scaleByX($IMAGE_SIZE_X);
                if ($fullimage->getImageHeight() > $IMAGE_SIZE_Y)
                    $fullimage->scaleByY($IMAGE_SIZE_Y);

                $fullimage->save(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full/' . $product['id'] . '_full.jpg', 'jpg');
                unset($fullimage);

                $thumbnailimage = &Image_Transform::factory('GD');
                $thumbnailimage->load($_FILES['image']['tmp_name']);

                //create the thumbnail
                if ($thumbnailimage->getImageWidth() > $THUMBNAIL_SIZE_X)
                    $thumbnailimage->scaleByX($THUMBNAIL_SIZE_X);
                if ($thumbnailimage->getImageHeight() > $THUMBNAIL_SIZE_Y)
                    $thumbnailimage->scaleByY($THUMBNAIL_SIZE_Y);

                $thumbnailimage->save(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/thumb/' . $product['id'] . '_thumb.jpg', 'jpg');
                unset($thumbnailimage);

                $hasimage = 1;

                //delete the temporary image file
                @unlink($_FILES['image']['tmp_name']);
            }
            //or if we have existing ones
            else if (file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full/' . $product['id'] . '_full.jpg')) {
                $hasimage = 1;
            }
            //check if existing in store_articles
            $article = getArticleForProduct($product['id']);
            if (empty($article)) {
                //insert to store_articles
                $sql = sprintf("INSERT INTO store_articles(productid, articleid, accesstime, accesslimit) "
                        . "VALUES (%d,'%s',%d,%d)", $product['id'], $_POST['articleid'], $_POST['accesstime'], $_POST['accesslimit']);
                $db->Execute($sql);
            }
            $product['meta_title'] = htmlspecialchars($product['meta_title'], ENT_QUOTES);
            $product['meta_description'] = htmlspecialchars($product['meta_description'], ENT_QUOTES);

            $sql = sprintf("UPDATE store_products SET
                            categoryid=%d,
                            name='%s',
                            type='%s',
                            teaser=%s,
                            description=%s,
                            price='%s',
                            taxexempt=%d,
                            hasimage='%s',
                            visibility='%s',
                            stock=%d,
                            memberdiscount='%s',
                            meta_title='%s',
                            meta_description='%s'
                        WHERE id=%d", 
                    $product['categoryid'], 
                    $product['name'], 
                    $product['type'], 
                    $db->qstr($product['teaser']), 
                    $db->qstr($product['description']), 
                    sprintf("%.2f", $product['price']), 
                    $product['taxexempt'], 
                    $hasimage, 
                    $product['visibility'], 
                    $product['stock'], 
                    $product['memberdiscount'], 
                    $product['meta_title'], 
                    $product['meta_description'],
                    $product['id']
                );
            
            $db->Execute($sql);
     
            if (count($productshippingrates) && $product['type'] != 'd') {
                foreach ($productshippingrates as $rateid => $cost) {
                    $shippingratesql = sprintf("UPDATE store_shipping_rates_product SET cost='%s' 
                        WHERE productid='%d' AND shippingrateid='%d'", $cost, $product['id'], $rateid);
                    $shippingratesqlresult = $db->Execute($shippingratesql);
                }
            } else {
                //force the shippingrates sql check to be successful if there are no shipping rates to add
                $shippingratesqlresult = 1;
            }

            if ($_POST['type'] == 'd') {
                if (!file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/storefiles/')) {
                    //if not, create it
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']) . '/storefiles', 0775);
                }
                $uploaddir = dirname($_SERVER['SCRIPT_FILENAME']) . '/storefiles/';

                if ($_FILES['downloadpreview']['size']) {
                    $downloadpreiviewfile = $uploaddir . $product['id'] . '_preview';
                    $downloadpreviewsize = $db->qstr($_FILES['downloadpreview']['size']);
                    $downloadpreviewtype = $db->qstr($_FILES['downloadpreview']['type']);
                    $downloadpreviewname = $db->qstr($_FILES['downloadpreview']['name']);
                    if (move_uploaded_file($_FILES['downloadpreview']['tmp_name'], $downloadpreiviewfile)) {
                        $previewsql = sprintf("UPDATE store_downloads SET preview='FS',previewsize=%s,previewtype=%s,previewname = %s WHERE productid=%d", $downloadpreviewsize, $downloadpreviewtype, $downloadpreviewname, $product['id']);
                        $previewsqlresult = $db->Execute($previewsql);
                        $extensions = explode('/',$downloadpreviewtype);  
                        if($extensions) { 
                            $ext = str_replace("'","",$extensions[1]);                   
                            if($this->isImage(trim($ext))) {
                                //make a copy of image file to display in preview
                                copy($downloadpreiviewfile, dirname($_SERVER['SCRIPT_FILENAME']). '/custom/' . 'store' . '/images/full/' . $product['id'] . '_fullpreview.jpg');
                            }
                        }
                    } else {
                        $previewsqlresult = 0;
                    }
                } 
                else {
                    $previewsqlresult = 1;
                    $downloadpreviewsize = "'". $downloadpreviewsize ."'";
                    $downloadpreviewtype = "'". $downloadpreviewtype ."'";
                    $downloadpreviewname = "'". $downloadpreviewname ."'";
                }

                if ($_FILES['downloaddata']['size'] && $downloadisvalid) {
                    $downloaddatafile = $uploaddir . $product['id'];
                    $downloaddatasize = $db->qstr($_FILES['downloaddata']['size']);
                    $downloaddatatype = $db->qstr($_FILES['downloaddata']['type']);
                    $downloaddataname = $db->qstr($_FILES['downloaddata']['name']);
                    if (move_uploaded_file($_FILES['downloaddata']['tmp_name'], $downloaddatafile)) {

                        $dres = $db->getOne("select id from store_downloads where productid=" . $product['id']);
                        if ($dres) {
                            $downloadsql = sprintf("UPDATE store_downloads SET data='FS', datasize=%s, datatype=%s, dataname=%s WHERE productid=%d", $downloaddatasize, $downloaddatatype, $downloaddataname, $product['id']);
                            $downloadsqlresult = $db->Execute($downloadsql);
                        } 
                        else {
                           $downloadsql = sprintf("INSERT INTO store_downloads (
                                id, productid, 
                                preview, 
                                previewsize,
                                previewtype, 
                                previewname,
                                data, 
                                datasize, 
                                datatype, 
                                dataname) VALUES
                                (NULL, '%d',  'FS', %s, %s, %s, 'FS', %s, %s, %s)", 
                                $product['id'], 
                                $downloadpreviewsize, 
                                $downloadpreviewtype, 
                                $downloadpreviewname, 
                                $downloaddatasize, 
                                $downloaddatatype, 
                                $downloaddataname);
                            $downloadsqlresult = $db->Execute($downloadsql);
                        }
                    } else {
                        $downloadsqlresult = 0;
                    }
                } else {
                    $downloadsqlresult = 1;
                }
                @unlink($_FILES['downloadpreview']['tmp_name']);
                @unlink($_FILES['downloaddata']['tmp_name']);
            } else {
                //force the download sql check to be successful if there it's a physical product
                $downloadsqlresult = 1;
                $previewsqlresult = 1;
            }

            if ($_POST['type'] == 'a') {
                $articlesql = sprintf("UPDATE store_articles 
                    SET articleid='%s', 
                        accesstime='%d',
                        accesslimit='%d'
                    WHERE productid='%d'", 
                        $product['articleid'], 
                        $product['accesstime'], 
                        $product['accesslimit'], 
                        $product['id']
                    );

                $articlesqlresult = $db->Execute($articlesql);
                //vd::dump(ADODB_Pear_Error());
            } else {
                //force the article sql check to be successful if it's not an article
                $articlesqlresult = 1;
            }


            if ($previewsqlresult && $downloadsqlresult && $shippingratesqlresult && $articlesqlresult) {
                Header("Location:/" . $dispatcher['store'] . "/products/" . $product['id']);
            } 
            else {
                $this->message = "Updating product failed.  Please contact support@subhub.com";
                $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
                $this->form->accept($renderer);
                $this->form = $renderer->toArray();
                $this->template = 'editproduct.tpl';
            }
        } else {
            $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            $this->form->accept($renderer);
            $this->form = $renderer->toArray();
            $this->template = 'editproduct.tpl';
        }
    }

    function showProduct($product) {
        global $dispatcher;

        if ($product) {
            if (isset($_POST['preview'])) {
                getPreview($product['id']);
            }

            if ($this->applyconversion)
                $product['price'] = $this->currencyConvert($product['price']);

            $formpath = '/' . $dispatcher['store'] . "/products/" . $dispatcher['product'];
            $form = new HTML_QuickForm('form', 'POST', $formpath, null, TRUE);

            if (checkProductHasPreview($product['id']) && $product['type'] === 'd') { //for download ($product['type'] === 'd')
                $form->addElement('submit', 'preview', 'Preview Download', array("class" => "btn btn-primary  my-1"));
                $this->haspreview = true;
            }
//$content->product.type eq 'd' || $content->product.stock eq ''
            if (($this->stocklevels == 1 && $product['stock'] > 0) || $this->stocklevels == 0 || $product['type'] == 'd' || $product['type'] == '') {
                $form->addElement('submit', 'addtocart', 'Add to cart', array('class' => 'btn btn-primary m-1', 'role' => 'button'));
                $form->addElement('submit', 'backtoproducts', 'Back to products');
            }
            if (file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/custom/' . $dispatcher['store'] . '/images/full/' . $product['id'] . '_large.jpg')) {
                $product['haslargeimage'] = 1;
            }

            $product['description'] = htmlspecialchars_decode($product['description'], ENT_QUOTES); 
            $product['teaser'] = htmlspecialchars_decode($product['teaser'], ENT_QUOTES); 

            $product['meta_title'] = htmlspecialchars_decode($product['meta_title'], ENT_QUOTES); 
            $product['meta_description'] = htmlspecialchars_decode($product['meta_description'], ENT_QUOTES); 
            
            $this->product = $product;
            $this->title = $product['name'];
            
            $lastlinkexp = explode("/", $_SESSION['dispatcher'][0]);

            if (end($lastlinkexp) == "delete" || end($lastlinkexp) == "edit") {
                $this->fromcategory = $product['categoryid'];
            } else {
                $this->fromcategory = end($lastlinkexp);
            }

            $patharray = null;
            $this->categoryhistory = getCategoryHistory($product['categoryid'], $patharray);

            $this->template = 'showproduct.tpl';

            $this->form = $form;

            if ($this->form->validate() && isset($_POST['addtocart'])) {
                $this->addtocart($dispatcher['product']);
                Header("Location:/" . $dispatcher['store'] . "/cart");
            } else {
                $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
                $this->form->accept($renderer);
                $this->form = $renderer->toArray();
                $this->template = 'showproduct.tpl';
            }
        } else {
            $this->template = 'invalidproduct.tpl';
        }
    }

    function deleteProducts($products) {
        global $db, $dispatcher;
        #vd::dump($products);
        #die();
        $in = implode(",", $products);
        $sql = sprintf("UPDATE store_products SET discontinued=1 WHERE id IN(%s);", $in);
        $db->Execute($sql);
        
        $sql2 = sprintf("DELETE FROM store_articles WHERE productid IN (%s)", $in);
        $db->Execute($sql2);
        
        Header("Location:/" . $dispatcher['store'] . "/");
    }

    function deleteProduct($product) {
        global $db;
        global $dispatcher;

        if (isset($_POST['cancel'])) {
            Header("Location:/" . $dispatcher['store'] . "/products/" . $product['id']);
            die();
        }

        $formpath = '/' . $dispatcher['store'] . '/products/' . $product['id'] . '/delete';
        $form = new HTML_QuickForm('form', 'post', $formpath, null, TRUE);

        $form->addElement('submit', 'delete', 'Delete', array('class' => 'btn btn-primary', 'role' => 'button'));
        $form->addElement('submit', 'cancel', 'Cancel', array('class' => 'btn btn-primary', 'role' => 'button'));

        $this->form = $form;

        if ($this->form->validate() && isset($_POST['delete'])) {
            //discontinue the product
            $sql = sprintf("UPDATE store_products set discontinued='1' WHERE id='%d'", $product['id']);

            if ($db->Execute($sql)) {
                
                $sql2 = sprintf("DELETE FROM store_articles WHERE productid='%d'", $product['id']);
                $db->Execute($sql2);
                
                Header("Location:/" . $dispatcher['store'] . "/");
            }
        } else {
            $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
            $this->form->accept($renderer);
            $this->form = $renderer->toArray();
            $this->template = 'deleteproduct.tpl';
        }
    }

    function processCheckout($paid = FALSE) {
        global $dispatcher, $member, $db, $settings;
        // $fpp = fopen(DOCUMENT_ROOT . "/storelog.txt", "a");
        // $ds = print_r($dispatcher, true);
        // $ms = print_r($member, true);
        // $dbs = print_r($db, true);
        // fputs($fpp, $ds . " \n\n==========\n\n" . $ms . "\n\n===========\n\n" . $dbs);

        require_once(NEURON_ROOT . 'includes/mandrill/Mandrill.php');

        if ($paid) {
            //fputs($fpp, 'paid... ');

            $orderid = $_SESSION[$dispatcher['store']]['orderid'];
            if (!$orderid) {
                $orderid = $_SESSION['store']['orderid'];
            }
            //$ssn = print_r($_SESSION, true);
            //fputs($fpp, 'order id: ' . $orderid . "\n\nSESSION:\n\n" . $ssn . "================\n\n");
            $idx = 0;
            $storestate = $_SESSION[$dispatcher['store']];
            foreach ($storestate['cartitems'] as $key => $cartitem) {
                if ($cartitem['type'] == 'p') {
                    $orderhasphysicalproducts = true;

                    //if stock level control is turned on
                    if ($this->stocklevels)
                        reduceStockForProduct($cartitem['id'], $cartitem['quantity']);
                }
                else if ($cartitem['type'] == 'd') {
                    $downloadcodes[$cartitem['id']] = activateDownload($orderid, $cartitem['id']);
                    $senddownloademail = true;
                } else if ($cartitem['type'] == 'a') {
                    $articlecodes[$cartitem['id']] = activateArticle($orderid, $cartitem['id']);
                    $sendarticleemail = true;
                }
                if ($cartitem['name']) {
                    if ($idx != 0) {
                        $_SESSION['idev_cart']['items_purchased'] .= ", ";
                    }
                    $_SESSION['idev_cart']['items_purchased'] .= $cartitem['name'];
                    $idx++;
                }
            }
            
            //fputs($fpp, "reduced stock levels.... ");
            if ($orderhasphysicalproducts) {
                $orderstatus = 'NEW';
            } else {
                $orderstatus = 'COMPLETED';
            }

            $sql = sprintf("UPDATE store_orders SET status='%s' WHERE id='%d'", $orderstatus, $orderid);
            $db->Execute($sql);
            //fputs($fpp,"\n\nSQL: ".$sql."\n\n");
            $_SESSION['idev_cart']['orderid'] = $orderid;
            $customeremail = $storestate['billing_email'];
             
            // Product price is Zero start
            if ( empty($customeremail) &&  floatval($_SESSION['idev_cart']['amount']) == 0 ) {
                $customeremail = $member->email;
                $_SESSION[$dispatcher['store']]['cart']['checkoutcurrency'] = $this->currencyinuse;
                $storestate = $_SESSION[$dispatcher['store']];
                $storestate['carttotals'] = $this->calculateCartTotals($storestate['cart']['shippingrateid'], false);
                foreach ($storestate['cartitems'] as $key => $cartitem) {
                    //pull in the additional product info to save with the order
                    $storestate['cartitems'][$key]['product'] = getProduct($key);
                    if ($member) {
                        if ($storestate['cartitems'][$key]['product']['memberdiscount'] == '' || floatval($storestate['cartitems'][$key]['product']['memberdiscount']) == 0) {
                            if ($this->memberdiscount) {
                                $storestate['cartitems'][$key]['product']['memberdiscount'] = $this->memberdiscount;
                            } else {
                                $storestate['cartitems'][$key]['product']['memberdiscount'] = 0;
                            }
                        }
                        $storestate['cartitems'][$key]['product']['price'] = sprintf("%.2f", $storestate['cartitems'][$key]['product']['price'] - ($storestate['cartitems'][$key]['product']['price'] / 100 * $storestate['cartitems'][$key]['product']['memberdiscount']));
                    }
                }

                if (!isset($_SESSION[$dispatcher['store']]['orderid'])) {
                    $storestate['billing_title'] = $member->title;
                    $storestate['billing_name_first'] = $member->name_first;
                    $storestate['billing_name_last'] = $member->name_last;
                    $storestate['billing_address'] = $member->address1;
                    $storestate['billing_city'] = $member->city;
                    $storestate['billing_state'] = $member->state;
                    $storestate['billing_zip'] = $member->postcode;
                    $storestate['billing_country'] = $member->country;
                    $storestate['billing_phone'] = $member->phonenumber;
                    $storestate['billing_email'] = $member->email;
                    $sql = sprintf("INSERT INTO store_orders (id,memberid, orderdata, placed, status) VALUES (NULL,'%d', %s, NOW(), '%s')", $member->id, $db->qstr(serialize($storestate)), $orderstatus);
                    $db->Execute($sql);
                }

                //Product price is Zero end

            }

            //fputs($fpp, "\n\n Call sendOrderEmailToCustomer " . $customeremail);

            $this->sendOrderEmailToCustomer($customeremail);

            if ($senddownloademail) {
                $this->sendDownloadEmailToCustomer($customeremail, $downloadcodes);
            }
                
            if ($sendarticleemail) {
                $this->sendArticleEmailToCustomer($customeremail, $articlecodes);
            } 

            $this->sendOrderEmailToStore();

            error_log("\n Store idev_affiliate_code : ".$settings->idev_affiliate_code);
        
            if ($settings->idev_affiliate_code) {
                require_once(NEURON_ROOT . "extensions/idevaffiliate/iDev.class.php");
	            //error_log("\n Store SESSION idev_cart : ".print_r($_SESSION['idev_cart'], true));
                //error_log("\n Store remote addr : ". $_SERVER['REMOTE_ADDR']. " CF ADDR : ". $_SERVER['HTTP_CF_CONNECTING_IP'] . "CLient IP " .$_SERVER['HTTP_CLIENT_IP']);
                iDev::submit($_SESSION['idev_cart']['amount'], $orderid, $_SESSION['idev_cart']['ip'], $customeremail, $_SESSION['idev_cart']['items_purchased'], $_SESSION['idev_cart']['couponcode]']);
                unset($_SESSION['idev_cart']);
            }

           //record the redemption for audit or tracking of redemption codes use in store
           $couponcode = $_SESSION['store']['coupon_code'];
           if($couponcode || !empty($couponcode)) {
                $ccode = $this->getPromoCodeValue($couponcode);
                if (is_array($ccode) && array_key_exists('id', $ccode)) {
                    $total = (int)$ccode['redeemed'] + 1;
                    $db->execute(sprintf("UPDATE neuron_coupons SET redeemed = %d where id = %d", $total, $ccode['id']));

                    $sql = sprintf("INSERT INTO `neuron_coupons_extended` (coupon_id, member_id, date_redeemed) VALUES (%d, %d, NOW())",$ccode['id'], $member->id);
                    $db->execute($sql);
                } 
            }

            unset($_SESSION[$dispatcher['store']]);

            
            if (is_null($_SESSION[$dispatcher['action']]['payment_method_id']) && empty($_SESSION[$dispatcher['action']]['payment_method_id'])) {
                if (floatval($_SESSION['idev_cart']['amount']) == 0) {
                    Header("Location:/" . $dispatcher['store'] . "/checkout/complete");
                } else {
                    return true;
                }
            }  
            return false;
        }
        else {
            //set session variables for static cart display
            $_SESSION[$dispatcher['store']]['cart']['checkoutcurrency'] = $this->currencyinuse;

            $storestate = $_SESSION[$dispatcher['store']];
            $storestate['carttotals'] = $this->calculateCartTotalsBySessionData();

            foreach ($storestate['cartitems'] as $key => $cartitem) {
                //pull in the additional product info to save with the order
                $storestate['cartitems'][$key]['product'] = getProduct($key);
                if ($member) {
                    if ($storestate['cartitems'][$key]['product']['memberdiscount'] == '' || floatval($storestate['cartitems'][$key]['product']['memberdiscount']) == 0) {
                        if ($this->memberdiscount) {
                            $storestate['cartitems'][$key]['product']['memberdiscount'] = $this->memberdiscount;
                        } else {
                            $storestate['cartitems'][$key]['product']['memberdiscount'] = 0;
                        }
                    }
                    $storestate['cartitems'][$key]['product']['price'] = sprintf("%.2f", $storestate['cartitems'][$key]['product']['price'] - ($storestate['cartitems'][$key]['product']['price'] / 100 * $storestate['cartitems'][$key]['product']['memberdiscount']));
                }
            }

            if (!isset($_SESSION[$dispatcher['store']]['orderid'])) {
                $orderstatus = 'CANCELLED';

                $sql = sprintf("INSERT INTO store_orders (id,memberid, orderdata, placed, status) VALUES (NULL,'%d', %s, NOW(), '%s')", $member->id, $db->qstr(serialize($storestate)), $orderstatus);
                $db->Execute($sql);
                $iid = $db->Insert_ID();
                $_SESSION[$dispatcher['store']]['orderid'] = $iid;
                $_SESSION['store']['orderid'] = $iid;
                #vd::dump($sql);
                #vd::dump($_SESSION);
            } else {
                //update 
                $orderdata = serialize($storestate);
                $sql = sprintf("UPDATE store_orders set memberid=%d, orderdata=%s WHERE id=%d", 
                        $member->id, 
                        $db->qstr($orderdata), 
                        $_SESSION[$dispatcher['store']]['orderid']);
                $db->Execute($sql);
            }
        }
    }

    function addTaxForRegion($country, $state) {
        // THIS CODE WILL ADD TAX WHEN STORE AND BUYER ARE BOTH IN US AND NA STATE - FIXME
        if (($country == 'GB' && $this->country == 'GB') || ($country == 'CA' && $this->country == 'CA') || ($country == 'AU' && $this->country == 'AU') || (($country == 'US' && $this->country == 'US') && ($state == $this->state))) {
            //the store and the customer is in the UK or the store and the customer is in the US and in the same state - add tax
            return true;
        }

        return false;
    }



    function exportOrders($STORE_ID) {
        global $db;
        global $dispatcher;

        $orders = getOrders($STORE_ID);

        $csv = NULL;
        $colnames = $db->MetaColumns('store_orders', true);

        foreach ($colnames as $key => $value) {
            //stupid fix for excel bug - change ID to ORDERID
            if ($key == 'ID') {
                $key = 'ORDERID';
            }
                
            $columnheaders[$key] = $key;
        }
        //vd::dump($colnames);
        $csv = substr($csv, 0, -1) . "\n";

        $order = end($orders);
        $orderdetails = unserialize($order['orderdata']);

        foreach ($orderdetails as $key => $orderdetail) {
            $orderdetaildata[$key] = $key;
        }

        $orderdatalocation = array_search("ORDERDATA", array_keys($colnames));
        array_splice($columnheaders, $orderdatalocation, 1, $orderdetaildata);
        //vd::dump($colnames);
        //vd::dump($orderdetaildata);
        vd::dump($columnheaders);
        die();

        foreach ($orders as $order) {

            //csv .= '"'.implode('","', str_replace('"', '""', $order))."\"\n";
        }

        // header("Content-type: application/vnd.ms-excel");
        // header("Content-disposition: csv; filename=store_orders-" . date("Y-m-d") . ".csv; size=" . strlen($csv));
        // echo $csv;
        // exit;
    }

    function sendOrderEmailToCustomer($customeremail) {
        global $dispatcher, $tpl2, $member, $settings;

        $domain = $settings->nonsecure_url;
        //Orig  $this->carttotals = $this->calculateCartTotals($_SESSION['store']['cart']['shippingrateid'], false);
        $this->carttotals = $this->calculateCartTotalsBySessionData();

        foreach ($_SESSION['store']['cartitems'] as $key => $cartitem) {
            $cartitems[$key]['product'] = getProduct($key);

            if ($member) {

                if ($cartitems[$key]['product']['memberdiscount'] == '' || floatval($cartitems[$key]['product']['memberdiscount']) == 0) {
                    if ($this->memberdiscount) {
                        $cartitems[$key]['product']['memberdiscount'] = $this->memberdiscount;
                    } else {
                        $cartitems[$key]['product']['memberdiscount'] = 0;
                    }
                }
                $cartitems[$key]['product']['price'] = sprintf("%.2f", $cartitems[$key]['product']['price'] - ($cartitems[$key]['product']['price'] / 100 * $cartitems[$key]['product']['memberdiscount']));
            }

            $cartitems[$key]['quantity'] = $cartitem['quantity'];
        }

        $this->cartitems = $cartitems;

        $this->storedetails = $_SESSION['store'];
        $this->domain = $domain;
        $this->carttype = 'CUSTOMEREMAIL';
        if ($this->isOrderDigital())
            $this->digital = true;

        $shippingrate = getShippingRate($_SESSION['store']['cart']['shippingrateid']);
        $this->shippingratename = $shippingrate['name'];
        $this->ordernotes = $_SESSION['store']['cart']['ordernotes'];

        $to = $customeremail;

        $subject = $this->thankyouemailsubject;

        $headers = "From: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "Reply-To: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if (!$this->digital) {
            $shipping_details = "<td><b>Shipping Details</b></td>";


            $shipping_phone = $shipping_country = $shipping_state = $shipping_zip = $shipping_city = $shipping_address = $download_only = '';
            if (!$this->downloadonly) {
                $download_only = "<td>{$this->storedetails['shipping_title']}&nbsp;{$this->storedetails['shipping_name_first']}&nbsp;{$this->storedetails['shipping_name_last']}</td>";
                $shipping_address = $this->storedetails['shipping_address'];
                $shipping_city = $this->storedetails['shipping_city'];
                $shipping_zip = $this->storedetails['shipping_zip'];
                $shipping_state = $this->storedetails['shipping_state'];
                $shipping_country = $this->storedetails['shipping_country'];
                $shipping_phone = $this->storedetails['shipping_phone'];
            }
        }
        if (count($this->cartitems)) {

            $rowspan = 4;
            if ($this->tax > 0) {
                $tax = "(ex {$this->taxname})";
                $rowspan = 5;
                $taxname = (!empty($this->taxname)) ? $this->taxname : 'Tax';
                $totaltax = '-';
                if ($this->carttotals['totaltax']) {
                    $totaltax = $this->currencysymbol . round($this->carttotals['totaltax'], 2);
                }
                $total_tax = ' <tr>
          <td style="border-bottom: none;" colspan="3">' . $taxname . '</td>
          <td style="border-bottom: none;" colspan="1" align="right">
            ' . $totaltax . '
          </td>
        </tr>';
            }
            if (!$this->carttotals['shippingcost']) {
                $rowspan = $rowspan - 2;
            }

            $cart_discount = '';
            if ($this->carttotals['discount'] != 0) {
                $cart_discount = '<tr>
            <td style="border-bottom: none;" colspan="1" ></td>
            <td style="border-bottom: none;" colspan="3">Discount</td>
            <td style="border-bottom: none;" colspan="1" align="right">          
              <b>' . $this->carttotals['discount'] . ' %<b>
            </td>
         </tr>';
            }

            $cart_subtotal = $this->currencysymbol . round($this->carttotals['subtotal'], 2);

            if ($this->carttotals['shippingcost'] != 0) {
                $shippingcost = round($this->carttotals['shippingcost'], 2);
                $cart_shipping .= '<tr>
          <td style="border-bottom: none;" colspan="1">
            Shipping Method
          </td>
          <td style="border-bottom: none;" colspan="3"  align="right">
            ' . $this->shippingratename . '
          </td>
        </tr>
        <tr>
          <td style="border-bottom: none;" colspan="3">Shipping Charge</td>
          <td style="border-bottom: none;" colspan="1" align="right">
            ' . $this->currencysymbol . ' ' . $shippingcost . '
          </td>
        </tr>';
            }

            $required_symbol = "<font color='#FF0000'>*</font>";
            $downloadnotice = $articlenotice = 0;
            foreach ($this->cartitems as $cartitem) {
                $cart_price = $this->currencysymbol . round($cartitem['product']['price'], 2);
                if ($cartitem['product']['type'] == 'd') {
                    $downloadnotice = '1';
                } elseif ($cartitem['product']['type'] == 'a') {
                    $articlenotice = '1';
                }
                $items_total = $this->currencysymbol . round(($cartitem['product']['price'] * $cartitem['quantity']), 2);

                if ($this->carttype == 'STOREEMAIL' || $this->carttype == 'CUSTOMEREMAIL') {
                    $cart_width = '80%';
                    $product_image = ($cartitem['product']['hasimage']) ? '<a href="http://' . $this->domain . '/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '"><img style="width: 80px; height: auto;" border="0" src="http://' . $this->domain . '/custom/store/images/thumb/' . $cartitem['product']['id'] . '_thumb.jpg"></a>' : '';
                    $product_name = '<b><a href="http://' . $this->domain . '/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '">' . $cartitem['product']['name'] . '</a></b>';
                } else {
                    $cart_width = '100%';
                    $product_image = ($cartitem['product']['hasimage']) ? '<a href="/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '"><img style="width: 80px; height: auto;" border="0" src="/custom/store/images/thumb/' . $cartitem['product']['id'] . '_thumb.jpg"></a>' : "";
                    $product_name = '<b><a href="/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '">' . $cartitem['product']['name'] . '</a></b>';
                }
                $cart_items .= "
        <tr>
          <td style='width: 10%;'>{$product_image}</td>
          <td style='width: 50%;'>{$product_name}</td>
          <td style='width: 10%;'>{$cartitem['quantity']} {$required_symbol}</td>
          <td align='center' style='width: 10%;'>{$cart_price}</td>
          <td align='right' style='width: 20%;'><b>{$items_total}</b></td>
        </tr>";
            }

            $cart_total = $this->currencysymbol . round($this->carttotals['total'], 2);
            $notices = '';
            if ($downloadnotice) {
                $productValidity = (int)(empty($settings->product_validity_days) ? 1 : (int)$settings->product_validity_days);
                $productlimitdays =  $productValidity > 2 ? $productValidity. " Days": ($productValidity * 24). " hours";
                $productLimit = (int)(empty($settings->product_download_limit) ? 1 : (int)$settings->product_download_limit);
        
                if ($productLimit > 3) {
                    $productDownloadLimit = $productLimit." times";
                } 
                else {
                    $productDownloadLimit = $productLimit == 1 ? "once" : "twice";
                } 

                $notices .= "<p><font color='#FF0000'>*</font>&nbsp;<i>";
                if ($this->carttype == 'CUSTOMEREMAIL') {
                    $notices .= "You will receive a second email containing the download link for this product. Purchased products can be downloaded {$productDownloadLimit} and within {$productlimitdays} of purchase.";
                } elseif ($this->carttype == 'STOREEMAIL' || $this->carttype == 'STORE') {
                    $notices .= "Digital Purchase";
                } else {
                    $notices .= "Downloads can only be purchased in single quantities. They can be downloaded {$productDownloadLimit} within {$productlimitdays} of purchase.";
                }
                $notices .= "</i></p>";
            }
            if ($articlenotice) {
                $notices .= "<p><font color='#FF0000'>*</font>&nbsp;<i>";
                if ($this->carttype == 'CUSTOMEREMAIL') {
                    $notices .= "You will receive a second email containing the article link for this product.";
                } elseif ($this->carttype == 'STOREEMAIL' || $this->carttype == 'STORE') {
                    $notices .= "Digital Purchase";
                } else {
                    $notices .= "Articles can only be purchased in single quantities.";
                }
                $notices .= "</i></p>";
            }
        }

        if (!empty($this->ordernotes)) {
            $ordernotes = "<strong>Order notes:</strong><br>";
            $ordernotes .= $this->ordernotes;
        }

        if (floatval($this->carttotals['total']) > 0 ) {       
            $tpl2->assign('shipping_details', $shipping_details);
            $tpl2->assign('shipping_address', $shipping_address);
            $tpl2->assign('shipping_city', $shipping_city);
            $tpl2->assign('shipping_zip', $shipping_zip);
            $tpl2->assign('shipping_state', $shipping_state);
            $tpl2->assign('shipping_country', $shipping_country);
            $tpl2->assign('shipping_phone', $shipping_phone);
        } 
        else {
            $tpl2->assign('member', $member);
        }

        $tpl2->assign('download_only',$download_only);
        $tpl2->assign('domain', $domain);

        $tpl2->assign('tax', $tax);
        $tpl2->assign('total_tax', $total_tax);

        $tpl2->assign('rowspan', $rowspan);
        $tpl2->assign('cart_subtotal', $cart_subtotal);
        $tpl2->assign('cart_shipping', $cart_shipping);
        $tpl2->assign('cart_total', $cart_total);
        $tpl2->assign('cart_width', $cart_width);
        $tpl2->assign('cart_items', $cart_items);
        $tpl2->assign('cart_discount', $cart_discount);

        $tpl2->assign('notices', $notices);
        $tpl2->assign('order_notes', $order_notes);

        $tpl2->assign_by_ref('content', $this);
        
        $message = $tpl2->fetch('send_order_email_to_customer.tpl');

        $aHeaders['from'] = strip_tags($this->fromemail);
        $aHeaders['to'] = $to;
        $aHeaders['type'] = ($this->digital) ? 'digital' : 'physical';
        $aHeaders['subject'] = $subject;
        $aHeaders['message'] = $message;

        $this->store_email_manager($aHeaders);

        $mandrill = new Mandrill(MANDRILL_APIKEY);
        $message = $this->buildStoreMessage($message, $subject, $to, $aHeaders['from']);
        $template_content = array(array('name' => '', 'content' => ''));
        try {
            $result = $mandrill->messages->sendTemplate('store-order-customer-email', $template_content, $message, false, '', null);
            //mail('joan@subhub.com', SITE_NAME.' store order email a mandrill result ', print_r($result, true) . print_r($message, true));
        } catch (Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            mail('joan@subhub.com', SITE_NAME . ' store order email mandrill error occurred: ', get_class($e) . ' - ' . $e->getMessage());
        }
        //store-order-customer-email  
        //mail($to, $subject, $message, $headers);
    }

    function store_email_manager($headers) {
        global $db, $tpl2;
        $sql = sprintf("INSERT INTO store_email_manager (type, recipient, sender, date_sent, subject, message) "
                . "VALUES ('%s', '%s','%s', NOW(), '%s', %s)", $headers['type'], $headers['to'], $headers['from'], $headers['subject'], 
                $db->qstr($headers['message']));
        $db->execute($sql);
    }

    function sendDownloadEmailToCustomer($customeremail, $downloadcodes) {
        global $dispatcher, $tpl2, $settings;
        $to = $customeremail;

        $subject = $this->thankyouemailsubject . " - Digital Purchase Details";

        $headers = "From: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "Reply-To: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $domain = $settings->nonsecure_url;
        $BASEURL = sprintf("http://%s/%s/download/", $domain, $dispatcher['store']);

        foreach ($downloadcodes as $key => $downloadcode) {
            $downloadurl = $BASEURL . $downloadcode;
            $product = getProduct($key);
            $downloads[] = array('product' => $product['name'], 'url' => sprintf("%s", $downloadurl));
        }

        $this->downloads = $downloads;
        $content_downloads = '';
        foreach ($downloads as $download) {
            $content_downloads .= "<tr><td>" . $download['product'] . "</td><td><a href='" . $download['url'] . "'>Click to Download</a></td></tr>";
        }

        $productValidity = (int)(empty($settings->product_validity_days) ? 1 : (int)$settings->product_validity_days);
        $productlimitdays =  $productValidity > 2 ? $productValidity. " Days": ($productValidity * 24). " hours";
        $productLimit = (int)(empty($settings->product_download_limit) ? 1 : (int)$settings->product_download_limit);

        if ($productLimit > 3) {
            $productDownloadLimit = $productLimit." times";
        } 
        else {
            $productDownloadLimit = $productLimit == 1 ? "once" : "twice";
        } 

        $downloadnotice = "Download links are active for {$productlimitdays} from time of purchase and can be downloaded {$productDownloadLimit} within that time.";
        
        $tpl2->assign('downloadnotice', $downloadnotice);
        $tpl2->assign('domain', $this->domain);
        $tpl2->assign('content_downloads', $content_downloads);
        $message = $tpl2->fetch('send_download_email_to_cutomer.tpl');

        $aHeaders['from'] = strip_tags($this->fromemail);
        $aHeaders['to'] = $to;
        $aHeaders['type'] = 'digital';
        $aHeaders['subject'] = $subject;
        $aHeaders['message'] = $message;

        $this->store_email_manager($aHeaders);

        $mandrill = new Mandrill(MANDRILL_APIKEY);
        $message = $this->buildStoreMessage($message, $subject, $to, $aHeaders['from']);
        $template_content = array(array('name' => '', 'content' => ''));
        try {
            $result = $mandrill->messages->sendTemplate('store-order-customer-email', $template_content, $message, false, '', null);
            //mail('joan@subhub.com', SITE_NAME.' store sendDownloadEmailToCustomer mandrill result ', print_r($result, true) . print_r($message, true));
        } catch (Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            mail('joan@subhub.com', SITE_NAME . ' store sendDownloadEmailToCustomer mandrill error occurred: ', get_class($e) . ' - ' . $e->getMessage());
        }

        //mail($to, $subject, $message, $headers);
    }

    function sendArticleEmailToCustomer($customeremail, $articlecodes) {
        global $dispatcher, $tpl2, $settings;
        $to = $customeremail;

        $subject = $this->thankyouemailsubject . " - Digital Purchase Details";

        $headers = "From: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "Reply-To: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $domain = $settings->nonsecure_url;
        $BASEURL = sprintf("http://%s/articles/", $domain);

        $articles = '';
        foreach ($articlecodes as $key => $articlecode) {
            $article = getArticleForProduct($key);
            $articleurl = $BASEURL . $article['articleid'] . "/" . $articlecode;
            $product = getProduct($key);
            // $articles[] = array('product'=>$product['name'],'url'=>sprintf("%s",$articleurl));

            $articles .= '<tr><td>' . $product['name'] . '</td><td><a href="' . sprintf("%s", $articleurl) . '">Click to Access This Article</a></td></tr>';
        }

        $this->articles = $articles;

        $tpl2->assign('domain', $this->domain);
        $tpl2->assign('articles', $articles);
        $message = $tpl2->fetch('send_article_email_to_customer.tpl');

        $aHeaders['from'] = strip_tags($this->fromemail);
        $aHeaders['to'] = $to;
        $aHeaders['type'] = 'digital';
        $aHeaders['subject'] = $subject;
        $aHeaders['message'] = $message;

        $this->store_email_manager($aHeaders);

        $mandrill = new Mandrill(MANDRILL_APIKEY);
        $message = $this->buildStoreMessage($message, $subject, $to, $aHeaders['from']);
        $template_content = array(array('name' => '', 'content' => ''));
        try {
            $result = $mandrill->messages->sendTemplate('store-order-customer-email', $template_content, $message, false, '', null);
            //mail('joan@subhub.com', SITE_NAME.' store sendArticleEmailToCustomer mandrill result ', print_r($result, true) . print_r($message, true));
        } catch (Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            mail('joan@subhub.com', SITE_NAME . ' store sendArticleEmailToCustomer mandrill error occurred: ', get_class($e) . ' - ' . $e->getMessage());
        }

        //mail($to, $subject, $message, $headers);
    }

    function sendOrderEmailToStore() {
        global $dispatcher, $tpl2, $member, $settings, $db;
        if (!empty($this->orderemail)) {
            $to = $this->orderemail;
        } else {
            $to = $db->getOne("select value from neuron_settings where name = 'adminemail'");
        }

        $domain = $settings->nonsecure_url;
        $subject = sprintf("Store Order for %s", $domain);

        $headers = "From: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "Reply-To: " . strip_tags($this->fromemail) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        // Orig $this->carttotals = $this->calculateCartTotals($_SESSION[$dispatcher['store']]['cart']['shippingrateid'], false);
        $this->carttotals = $this->calculateCartTotalsBySessionData();

        foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
            $cartitems[$key]['product'] = getProduct($key);
            if ($member) {
                if ($cartitems[$key]['product']['memberdiscount'] == '' || floatval($cartitems[$key]['product']['memberdiscount']) == 0) {
                    if ($this->memberdiscount) {
                        $cartitems[$key]['product']['memberdiscount'] = $this->memberdiscount;
                    } else {
                        $cartitems[$key]['product']['memberdiscount'] = 0;
                    }
                }
                $cartitems[$key]['product']['price'] = sprintf("%.2f", $cartitems[$key]['product']['price'] - ($cartitems[$key]['product']['price'] / 100 * $cartitems[$key]['product']['memberdiscount']));
            }
            $cartitems[$key]['quantity'] = $cartitem['quantity'];
        }

        $this->cartitems = $cartitems;
        $this->storedetails = $_SESSION[$dispatcher['store']];
        $this->carttype = 'STOREEMAIL';
        $shippingrate = getShippingRate($_SESSION[$dispatcher['store']]['cart']['shippingrateid']);
        $this->shippingratename = $shippingrate['name'];
        $this->ordernotes = $_SESSION[$dispatcher['store']]['cart']['ordernotes'];
        if ($this->isOrderDigital()) {
            $this->digital = true;
        }

        $tpl2->assign_by_ref('content', $this);
        $tpl2->assign('dispatcher', $dispatcher);
        //$message = $tpl2->fetch('orderemail.tpl');

        if (!$this->digital) {
            $shipping_details = "<td><b>Shipping Details</b></td>";


            $shipping_phone = $shipping_country = $shipping_state = $shipping_zip = $shipping_city = $shipping_address = $download_only = '';
            if (!$this->downloadonly) {
                $download_only = "<td>{$this->storedetails['shipping_title']}&nbsp;{$this->storedetails['shipping_name_first']}&nbsp;{$this->storedetails['shipping_name_last']}</td>";
                $shipping_address = $this->storedetails['shipping_address'];
                $shipping_city = $this->storedetails['shipping_city'];
                $shipping_zip = $this->storedetails['shipping_zip'];
                $shipping_state = $this->storedetails['shipping_state'];
                $shipping_country = $this->storedetails['shipping_country'];
                $shipping_phone = $this->storedetails['shipping_phone'];
            }
        }
        if (count($this->cartitems)) {

            $rowspan = 4;
            if ($this->tax > 0) {
                $tax = "(ex {$this->taxname})";
                $rowspan = 5;
                $taxname = (!empty($this->taxname)) ? $this->taxname : 'Tax';
                $totaltax = '-';
                if ($this->carttotals['totaltax']) {
                    $totaltax = $this->currencysymbol . round($this->carttotals['totaltax'], 2);
                }
                $total_tax = ' <tr>
            <td style="border-bottom: none;" colspan="3">' . $taxname . '</td>
            <td style="border-bottom: none;" colspan="1" align="right">
              ' . $totaltax . '
            </td>
          </tr>';
            }
            if (!$this->carttotals['shippingcost']) {
                $rowspan = $rowspan - 2;
            }

            $cart_discount = '';
            if ($this->carttotals['discount'] != 0) {
                $cart_discount = '<tr>
              <td style="border-bottom: none;" colspan="1" ></td>
              <td style="border-bottom: none;" colspan="3">Discount</td>
              <td style="border-bottom: none;" colspan="1" align="right">          
                <b>' . $this->carttotals['discount'] . ' %<b>
              </td>
           </tr>';
            }

            $cart_subtotal = $this->currencysymbol . round($this->carttotals['subtotal'], 2);

            if ($this->carttotals['shippingcost'] != 0) {
                $shippingcost = round($this->carttotals['shippingcost'], 2);
                $cart_shipping .= '<tr>
            <td style="border-bottom: none;" colspan="1">
              Shipping Method
            </td>
            <td style="border-bottom: none;" colspan="3"  align="right">
              ' . $this->shippingratename . '
            </td>
          </tr>
          <tr>
            <td style="border-bottom: none;" colspan="3">Shipping Charge</td>
            <td style="border-bottom: none;" colspan="1" align="right">
              ' . $this->currencysymbol . ' ' . $shippingcost . ' 
            </td>
          </tr>';
            }

            $required_symbol = "<font color='#FF0000'>*</font>";
            $downloadnotice = $articlenotice = 0;
            foreach ($this->cartitems as $cartitem) {
                $cart_price = $this->currencysymbol . round($cartitem['product']['price'], 2);
                if ($cartitem['product']['type'] == 'd') {
                    $downloadnotice = '1';
                } elseif ($cartitem['product']['type'] == 'a') {
                    $articlenotice = '1';
                }
                $items_total = $this->currencysymbol . round(($cartitem['product']['price'] * $cartitem['quantity']), 2);

                if ($this->carttype == 'STOREEMAIL' || $this->carttype == 'CUSTOMEREMAIL') {
                    $cart_width = '60%';
                    $product_image = ($cartitem['product']['hasimage']) ? '<a href="http://' . $this->domain . '/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '"><img style="width: 80px; height: auto;" border="0" src="http://' . $this->domain . '/custom/store/images/thumb/' . $cartitem['product']['id'] . '_thumb.jpg"></a>' : '';
                    $product_name = '<b><a href="http://' . $this->domain . '/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '">' . $cartitem['product']['name'] . '</a></b>';
                } else {
                    $cart_width = '100%';
                    $product_image = ($cartitem['product']['hasimage']) ? '<a href="/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '"><img style="width: 80px; height: auto;" border="0" src="/custom/store/images/thumb/' . $cartitem['product']['id'] . '_thumb.jpg"></a>' : "";
                    $product_name = '<b><a href="/' . $dispatcher['store'] . '/products/' . $cartitem['product']['id'] . '">' . $cartitem['product']['name'] . '</a></b>';
                }
                $cart_items .= "
          <tr>
            <td>{$product_image}</td>
            <td>{$product_name}</td>
            <td>{$cartitem['quantity']} {$required_symbol}</td>
            <td align='center'>{$cart_price}</td>
            <td align='right'><b>{$items_total}</b></td>
          </tr>";
            }

            $cart_total = $this->currencysymbol . round($this->carttotals['total'], 2);
            $notices = '';
            if ($downloadnotice) {
                $productValidity = (int)(empty($settings->product_validity_days) ? 1 : (int)$settings->product_validity_days);
                $productlimitdays =  $productValidity > 2 ? $productValidity. " Days": ($productValidity * 24). " hours";
                $productLimit = (int)(empty($settings->product_download_limit) ? 1 : (int)$settings->product_download_limit);
        
                if ($productLimit > 3) {
                    $productDownloadLimit = $productLimit." times";
                } 
                else {
                    $productDownloadLimit = $productLimit == 1 ? "once" : "twice";
                } 

                $notices .= "<p><font color='#FF0000'>*</font>&nbsp;<i>";
                if ($this->carttype == 'CUSTOMEREMAIL') {
                    $notices .= "You will receive a second email containing the download link for this product. Purchased products can be downloaded {$productDownloadLimit} and within {$productlimitdays} of purchase.";
                } elseif ($this->carttype == 'STOREEMAIL' || $this->carttype == 'STORE') {
                    $notices .= "Digital Purchase";
                } else {
                    $notices .= "Downloads can only be purchased in single quantities. They can be downloaded {$productDownloadLimit} within {$productlimitdays} of purchase.";
                }
                $notices .= "</i></p>";
            }
            if ($articlenotice) {
                $notices .= "<p><font color='#FF0000'>*</font>&nbsp;<i>";
                if ($this->carttype == 'CUSTOMEREMAIL') {
                    $notices .= "You will receive a second email containing the article link for this product.";
                } elseif ($this->carttype == 'STOREEMAIL' || $this->carttype == 'STORE') {
                    $notices .= "Digital Purchase";
                } else {
                    $notices .= "Articles can only be purchased in single quantities.";
                }
                $notices .= "</i></p>";
            }
        }

        if (!empty($this->ordernotes)) {
            $ordernotes = "<strong>Order notes:</strong><br>";
            $ordernotes .= $this->ordernotes;
        }

        if (floatval($this->carttotals['total']) > 0 ) {       
            $tpl2->assign('shipping_details', $shipping_details);
            $tpl2->assign('shipping_address', $shipping_address);
            $tpl2->assign('shipping_city', $shipping_city);
            $tpl2->assign('shipping_zip', $shipping_zip);
            $tpl2->assign('shipping_state', $shipping_state);
            $tpl2->assign('shipping_country', $shipping_country);
            $tpl2->assign('shipping_phone', $shipping_phone);
        } 
        else {
            $tpl2->assign('member', $member);
        }

        $tpl2->assign('download_only',$download_only);
        $tpl2->assign('domain', $domain);

        $tpl2->assign('tax', $tax);
        $tpl2->assign('total_tax', $total_tax);

        $tpl2->assign('rowspan', $rowspan);
        $tpl2->assign('cart_subtotal', $cart_subtotal);
        $tpl2->assign('cart_shipping', $cart_shipping);
        $tpl2->assign('cart_total', $cart_total);
        $tpl2->assign('cart_width', $cart_width);
        $tpl2->assign('cart_items', $cart_items);
        $tpl2->assign('cart_discount', $cart_discount);

        $tpl2->assign('notices', $notices);
        $tpl2->assign('order_notes', $order_notes);

        $tpl2->assign_by_ref('content', $this);
        
        $message = $tpl2->fetch('send_order_email_to_customer.tpl');

        $aHeaders['from'] = strip_tags($this->fromemail);
        $aHeaders['to'] = $to;
        $aHeaders['type'] = ($this->digital) ? 'digital' : 'physical';
        $aHeaders['subject'] = $subject;
        $aHeaders['message'] = $message;

        $this->store_email_manager($aHeaders);

        $mandrill = new Mandrill(MANDRILL_APIKEY);
        $message = $this->buildStoreMessage($message, $subject, $to, $aHeaders['from']);
        $template_content = array(array('name' => '', 'content' => ''));
        try {
            $result = $mandrill->messages->sendTemplate('store-order-customer-email', $template_content, $message, false, '', null);
            //mail('joan@subhub.com', SITE_NAME.' store sendOrderEmailToStore mandrill result ', print_r($result, true) . print_r($message, true));
        } catch (Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            mail('joan@subhub.com', SITE_NAME . ' store sendOrderEmailToStore mandrill error occurred: ', get_class($e) . ' - ' . $e->getMessage());
        }

        //mail($to, $subject, $message, $headers);
        //  mail('joanm@sonarlogic.biz', $subject, $message, $headers);
    }

    function fetchDownload($downloadcode) {
        if (checkDownloadIsValid($downloadcode)) {
            getDownloadData($downloadcode);
        } else {
            $this->template = 'invaliddownload.tpl';
        }
    }

    function getSubscribers($from, $to) {
        global $db;
        $qry = $limit = '';
        if (!empty($from)) {
            $to = empty($to) ? date('Y-m-d') : $to;
            $qry = sprintf(" WHERE a.created >= '%s' AND a.created <= '%s' ", $from, $to);
        } else {
            $limit = ' LIMIT 2000';
        }

        $sql = sprintf("SELECT a.username as id, a.id as memberid, a.title as billing_title, a.name_first as billing_name_first, a.name_last as billing_name_last, c.amount, a.created as placed, a.substatus as status"
                . " FROM neuron_members a"
                . " INNER JOIN neuron_paymentplans b ON a.paymentplan = b.id"
                . " INNER JOIN neuron_paymentplans_currencies c ON b.id = c.paymentplan_id"
                . " %s"
                . " ORDER BY a.created DESC"
                . " %s", $qry, $limit);
//        die($sql);
        $res = $db->getAll($sql);
        return $res;
    }

    function viewOrders($STORE_ID) {
//        error_reporting(E_ALL);
        global $dispatcher, $tpl2;

        $formpath = '/' . $dispatcher['store'] . '/orders/';
        $form = new HTML_QuickForm('form', 'POST', $formpath, null, TRUE);
        $csv_out = '';
        if (isset($_POST)) {
            $date_from = !empty($_POST['date_from']) ? filter_var($_POST['date_from'], FILTER_SANITIZE_STRING) : '';
            $date_to = !empty($_POST['date_to']) ? filter_var($_POST['date_to'], FILTER_SANITIZE_STRING) : '' ;

            $dt = date_create($date_from);
            $from = date_format($dt, 'Y-m-d 00:00:00');

            $dt2 = date_create($date_to);
            $to = date_format($dt2, 'Y-m-d 23:23:23');

            $tpl2->assign('date_from', $date_from);
            $tpl2->assign('date_to', $date_to);
        }

        if (!empty($_POST['export'])) {

            $csv_data = $this->getOrderDatas($STORE_ID, $from, $to);

            $csv_out = 'Order No, Type, Name, Product Name, Amount, Placed, Status, Billing Address, Shipping Address, Notes';
           
            $csv_out .= "\n";
            $csv_out_store = "";
            $csv_out_members = "";

            foreach ($csv_data as $key => $row) {
//                die(var_dump($row['details']['carttotals']['subtotal']));
//                die(var_dump($row['details']['billing_state']));
                if ($row['type'] == 'store') {
                    
                    if ( isset($row['details']['store_amount']) ) {
                        $store_total_amount = $row['details']['store_amount'];
                    } else {
                        $store_total_amount = $row['details']['carttotals']['total'];
                    }
                    
                    if($row['shipping_details']) {
                        $shipping =  $row['shipping_details']['address'] . " " . $row['shipping_details']['city']  . " " . $row['shipping_details']['state']  . " " . $row['shipping_details']['zip']  . " " .  $row['shipping_details']['country']  . " " . $row['shipping_details']['phone'];
                        $billing = "";
                    } else {
                        $billing =  $row['billing_details']['address'] . " " . $row['billing_details']['city']  . " " . $row['billing_details']['state']  . " " . $row['billing_details']['zip']  . " " .  $row['billing_details']['country']  . " " . $row['billing_details']['phone'];
                        $shipping = "";
                    }

                    $product_title = htmlspecialchars_decode($row['product_title'] , ENT_QUOTES); 

                    $csv_out_store .= '"' . $row['id'] . '","' . $row['type'] .  '","' . $row['details']['billing_title'] . " " . $row['details']['billing_name_first'] . " " . $row['details']['billing_name_last'] . '","'  . $product_title . '","'. $store_total_amount . '","' . $row['placed'] . '","' . $row['status'] . '","' . $billing . '","' . $shipping .'","' . $row['details']['cart']['ordernotes'] . '"' ;

                    $csv_out_store .= "\n";
                    
                } elseif ($row['type'] == 'member') {
                   
                        $csv_out_members .= '"' . $row['id'] . '", "' . $row['type'] . '","' . $row['member_details']['billing_title'] . " " . $row['member_details']['billing_name_first'] . " " . $row['member_details']['billing_name_last'] . '","' .''.'","'. $row['amount'] . '","' . $row['placed'] . '","' . $row['status'] . '"';
                        $csv_out_members .= "\n";
                     
                }
            }

            $csv_out .= $csv_out_members . $csv_out_store;

            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=export.csv");

            echo $csv_out;
            die;
        }

        if (isset($_POST['search']) || isset($_POST['submit'])) {

            $orders = getOrders($STORE_ID, $from, $to);
            $members = $this->getSubscribers($from, $to);
            $statuses = array('NEW' => 'New', 'BEING PROCESSED' => 'Being Processed',
                'AWAITING STOCK' => 'Awaiting Stock', 'COMPLETED' => 'Completed',
                'CANCELLED' => 'Cancelled');

            $akey = 0;
            foreach ($orders as $key => $order) {
                #vd::dump($order);
                $this->orders[$akey]['placed'] = tz_load(strtotime($order['placed']), 'Y-m-d H:i:s');
                $form->addElement('select', 'statusfield_' . $order['id'], null, $statuses);

                $this->orders[$akey]['statusfields'] = array('statusfield_' . $order['id']);
                $form->setDefaults(array('statusfield_' . $order['id'] => $order['status']));

                $this->orders[$akey]['details'] = unserialize($order['orderdata']);

                $this->orders[$akey]['id'] = $order['id'];
                $this->orders[$akey]['ip'] = $order['ip'];
                $this->orders[$akey]['memberid'] = $order['memberid'];
                //$this->orders[$akey]['placed'] = $order['placed'];
                $this->orders[$akey]['status'] = $order['status'];
                $this->orders[$akey]['type'] = 'store';

                unset($orders[$akey]['orderdata']);

                if (count($members)) {
                    foreach ($members as $mkey => $mval) {
                        $akey += 1;
                        $this->orders[$akey]['type'] = 'member';
                        $this->orders[$akey]['placed'] = tz_load(strtotime($mval['placed']), 'Y-m-d H:i:s');
                        $this->orders[$akey]['status'] = 'NEW';
                        //$this->orders[$akey]['name'] = $mval['name'];
                        $this->orders[$akey]['id'] = $mval['id'];
                        $this->orders[$akey]['memberid'] = $mval['memberid'];
                        $this->orders[$akey]['amount'] = $mval['amount'];

                        $this->orders[$akey]['member_details'] = array(
                            'billing_title' => $mval['billing_title'],
                            'billing_name_first' => $mval['billing_name_first'],
                            'billing_name_last' => $mval['billing_name_last'],
                            'carttotals' => array('total' => $mval['amount'])
                        );

                        unset($members[$mkey]);
                        break;
                    }
                }
                $akey++;
            }

            $form->addElement('submit', 'submit', 'Save Changes');
        }

        $this->form = $form;

        if ($this->form->validate()) {
            if (isset($_POST['submit'])) {
                foreach ($_POST as $key => $value) {
                    $keyexp = explode("_", $key);
                    if ($keyexp[0] == 'statusfield') {
                        updateOrderStatus($keyexp[1], $value);
                    }
                }
            }
        }

        $renderer = & new HTML_QuickForm_Renderer_ArraySmarty($tpl2);
        $this->form->accept($renderer);
        $this->form = $renderer->toArray();
        $this->template = 'vieworders.tpl';
    }

    function getOrderDatas($STORE_ID, $from, $to) {
        $orders = getOrders($STORE_ID, $from, $to);
        $members = $this->getSubscribers($from, $to);
        $statuses = array('NEW' => 'New', 'BEING PROCESSED' => 'Being Processed',
            'AWAITING STOCK' => 'Awaiting Stock', 'COMPLETED' => 'Completed',
            'CANCELLED' => 'Cancelled');

        $store_orders = '';

        $akey = 0;
        foreach ($orders as $key => $order) {
            #vd::dump($order);
            $store_orders[$akey]['placed'] = tz_load(strtotime($order['placed']), 'Y-m-d H:i:s');

            $store_orders[$akey]['statusfields'] = array('statusfield_' . $order['id']);

            $store_orders[$akey]['details'] = unserialize($order['orderdata']);

            $store_orders[$akey]['id'] = $order['id'];
            $store_orders[$akey]['ip'] = $order['ip'];
            $store_orders[$akey]['memberid'] = $order['memberid'];
            $store_orders[$akey]['status'] = $order['status'];
            $store_orders[$akey]['type'] = 'store';
            
            $cart = $store_orders[$akey]['details']['cartitems'];
            foreach ( $cart as $k => $v) {
                if (!empty($store_orders[$akey]['product_title'])) {
                    $store_orders[$akey]['product_title'] .= ', '. $v['name'];
                } else {
                    $store_orders[$akey]['product_title'] = $v['name'];
                }
                
            }

            unset($store_orders[$akey]['orderdata']);

            if ($store_orders[$akey]['details']['shipping_same'] === '1') {
                $store_orders[$akey]['shipping_details'] = array(
                    'address'=> $store_orders[$akey]['details']['shipping_address'],
                    'city'=> $store_orders[$akey]['details']['shipping_city'],
                    'state'=> $store_orders[$akey]['details']['shipping_state'],
                    'zip'=> $store_orders[$akey]['details']['shipping_zip'],
                    'country'=> $store_orders[$akey]['details']['shipping_country'],
                    'phone'=> $store_orders[$akey]['details']['shipping_phone'],
                );
            } 
            else {
                $store_orders[$akey]['billing_details'] = array(
                    'address'=> $store_orders[$akey]['details']['billing_address'],
                    'city'=> $store_orders[$akey]['details']['billing_city'],
                    'state'=> $store_orders[$akey]['details']['billing_state'],
                    'zip'=> $store_orders[$akey]['details']['billing_zip'],
                    'country'=> $store_orders[$akey]['details']['billing_country'],
                    'phone'=> $store_orders[$akey]['details']['billing_phone'],
                );
            }



            if (count($members)) {
                foreach ($members as $mkey => $mval) {
                    $akey += 1;
                    $store_orders[$akey]['type'] = 'member';
                    $store_orders[$akey]['placed'] = tz_load(strtotime($mval['placed']), 'Y-m-d H:i:s');
                    $store_orders[$akey]['status'] = 'NEW';
                    $store_orders[$akey]['id'] = $mval['id'];
                    $store_orders[$akey]['memberid'] = $mval['memberid'];
                    $store_orders[$akey]['amount'] = $mval['amount'];
                    
                    $store_orders[$akey]['member_details'] = array(
                        'billing_title' => $mval['billing_title'],
                        'billing_name_first' => $mval['billing_name_first'],
                        'billing_name_last' => $mval['billing_name_last'],
                        'carttotals' => array('total' => $mval['amount'])
                    );

                    unset($members[$mkey]);
                    break;
                }
            }
            $akey++;
        }

        return $store_orders;
    }


    function viewOrder($orderid) {
        global $db;

        $order = getOrder($orderid);
        $order['details'] = unserialize($order['orderdata']);
        $this->order = $order;
        foreach ($order['details']['cartitems'] as $key => $cartitem) {
            //fix to get current product data if it wasn't saved with order
            //should always be saved now in case price changes after order is placed.
            if (!isset($cartitem['product'])) {
                $order['details']['cartitems'][$key]['product'] = getProduct($key);
                //$order['details']['cartitems'][$key]['quantity'] = $cartitem['quantity'];
            }
        }

        $this->cartitems = $order['details']['cartitems'];
        $this->carttotals = $order['details']['carttotals'];
        $this->carttype = 'STORE';  // switch to store cart view
        $shippingrate = getShippingRate($order['details']['cart']['shippingrateid']);
        $this->shippingratename = $shippingrate['name'];
        $this->ordernotes = $order['details']['cart']['ordernotes'];

        $this->template = 'vieworder.tpl';
    }

    function isOrderDigital() {
        global $dispatcher;
        $digital = true;

        foreach ($_SESSION[$dispatcher['store']]['cartitems'] as $key => $cartitem) {
            //find out if any of the products are physical ones
            if ($cartitem['type'] == 'p')
                $digital = false;
        }

        return $digital;
    }

    function currencyConvert($price) {
        require_once(NEURON_ROOT . 'includes/billing/CurrencyConverter.class.php');

        $from = $this->currency;
        $to = $this->currencyinuse;
        $decimals = 2;

        $cconverter = new CurrencyConverter();
        $newprice = $cconverter->convert($price, $from, $to, $decimals);

        switch ($this->currencyrounding) {
            case '1':
                $newprice = sprintf('%.2f', (ceil($newprice * 4)) / 4);
                break;
            case '2':
                $newprice = sprintf('%.2f', (ceil($newprice * 2)) / 2);
                break;
            case '3':
                $newprice = sprintf('%.2f', ceil($newprice));
                break;
        }

        return $newprice;
    }

    function checkPromoCodeStatus($promocode, $flag = '') {
        global $db, $dispatcher;

        if (!empty($promocode)) {
            //check if the promocode is valid        
            $today = date('Y-m-d');
            $sql = sprintf("select * from neuron_coupons where BINARY `code` = '%s' and expiration >= '%s' and deleted = 0", $promocode, $today);
            $res = $db->getAll($sql);
            $msg = '';

            if ($res) {
                $redeemed = (int) $res[0]['redeemed'];
                $max_redemptions = (int) $res[0]['max_redemptions'];
                $coupon_value = $res[0]['value'];

                if (!empty($max_redemptions) && $redeemed >= $max_redemptions) {
                    $msg = "Coupon code '{$promocode}' is valid but has reach maximum redemptions.";
                } elseif ($flag == 'amount') {
                    $a = $_SESSION['idev_cart']['amount'];
                    $_SESSION['idev_cart']['amount'] = $a - ($a * $coupon_value) / 100;

                }
            } else {

                $sql = sprintf("select * from neuron_coupons where BINARY `code` = '%s'", $promocode);
                $res = $db->getOne($sql);
                $msg = "Coupon code '{$promocode}' does not exist.";
                if ($res) {
                    $msg = "Coupon code '{$promocode}' exist but is expired or not active";
                }
            }
            $_SESSION[$dispatcher['action']]['messages'] = $msg;
            if (empty($msg)) {
                $_SESSION[$dispatcher['store']]['coupon_code'] = $promocode;
            } else {
                unset($_SESSION[$dispatcher['store']]['coupon_code']);
            }
            return $msg;
        }
    }

    public function calcAmountWithPromoCode($promo_code, $subtotal) {
        global $db;

        $today = date('Y-m-d');
        $sql = sprintf("select * from neuron_coupons where code='%s' and expiration >= '%s' and deleted = 0", $promo_code, $today);
        $res = $db->getAll($sql);
        if ($res) {
            return $coupon_value = (int) $res[0]['value'];
        }
        return 0;
    }

    function checkMinimumCartTotal() {
        global $dispatcher;
        if (!empty($this->minimum_cart_total) && $this->minimum_cart_total > 0) {

            $carttotals = $this->calculateCartTotals($_SESSION[$dispatcher['store']]['cart']['shippingrateid'], 0);
            if ($carttotals['total'] == 0) {
                return false;
            }
            if ($carttotals['total'] < $this->minimum_cart_total) {
                return true;
            }
        }
        return false;
    }

    function buildStoreMessage($content, $subject, $to, $reply_to) {
        global $settings, $member;

        $evohost = 'subhub.com';
        
        $sysFrom = getEmailFromName();
        if (!empty($sysFrom)) {
            $from_name = $from = $sysFrom.'@'.$evohost;
        }
        else {
            $from_name = $from = SITE_NAME.'@'.$evohost;
        }
        
        $message = array(
            'subject' => $subject,
            'from_email' => $from,
            'from_name' => $from_name,
            'to' => array(
                array(
                    'email' => $to,
                    'type' => 'to'
                )
            ),
            'headers' => array('Reply-To' => $reply_to),
            'important' => false,
            'track_opens' => false,
            'track_clicks' => false,
            'auto_text' => null,
            'auto_html' => null,
            'inline_css' => null,
            'url_strip_qs' => null,
            'preserve_recipients' => null,
            'view_content_link' => null,
            'bcc_address' => null,
            'tracking_domain' => null,
            'signing_domain' => null,
            'return_path_domain' => null,
            'merge' => true,
            'global_merge_vars' => array(
                array(
                    'name' => 'CUSTOMER_EMAIL_CONTENT',
                    'content' => $content
                )
            )
        );

        return $message;
    }

    function checkMinCartFieldExist() {
        global $db;
        $sql = sprintf("
      SELECT count(column_name)
      FROM information_schema.COLUMNS 
      WHERE 
          TABLE_SCHEMA = '%s' 
      AND TABLE_NAME = 'store' 
      AND COLUMN_NAME = 'minimum_cart_total'", 'sh_' . SITE_NAME);
        $colCount = $db->getOne($sql);
        if ($colCount < 1) {
            $db->execute("ALTER TABLE `store` ADD `minimum_cart_total` INT(3) NOT NULL DEFAULT '0' AFTER `configured`;");
        }
    }
    
    function GetRecordLimitForProductStorePlan() {
        global $db, $settings;        
        $sql = sprintf("SELECT count(id) as recordcount FROM store_products WHERE discontinued != 1");
        $results = $db->GetAll($sql);
        if ($results) {
            return $results[0]['recordcount'];
        }
    }
    // show file as a link download
    function GetFileDownloads($productid) {
        global $db;
        $sql = "SELECT dataname FROM store_downloads WHERE productid={$productid} AND data > '' ";
        $result = $db->getRow($sql);
        if($result) {
            return $result['dataname'];
        } else {
        return 'No File chosen.';
        }           
    }
    // show file as a link preview
    function GetPreviewDownloads($productid) {
        global $db;
        $sql = "SELECT previewname FROM store_downloads WHERE productid={$productid} AND preview > '' ";
        $result = $db->getRow($sql);
        if($result) {
            return $result['previewname'];
        } else {
            return 'No File chosen.';
        }    
    }

    function isImage($filename) {
        switch (strtolower($filename)) {
            case 'jpg':
            case 'gif':
        case 'png':
            case 'jpeg':
            return true;
        }
        return false;
    }

    function getPromoCodeValue($promo_code) {
        global $db;
        $today = date('Y-m-d');
        $sql = sprintf("SELECT * FROM neuron_coupons WHERE code='%s' AND expiration >= '%s' AND deleted = 0", $promo_code, $today);
        $res = $db->getRow($sql);
        if ($res) {
          return $res;
        }
        return false;
    }

    function calculateCartTotalsBySessionData() {

        global $dispatcher;
        $otherstatetax = (float) $this->otherstatetax;
        $includeStateTaxCountry = $_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" ? 1: 0;
        $shippingrate_id =  $_SESSION[$dispatcher['store']]['cart']['shippingrateid'];
        $matchBillingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == $this->country || $_SESSION[$dispatcher['store']]['shipping_country'] == $this->country) ? 1 : 0;
        $matchCountryAndState = (($_SESSION[$dispatcher['store']]['billing_country'] == $this->country && $_SESSION[$dispatcher['store']]['billing_state'] == $this->state) || ($_SESSION[$dispatcher['store']]['shipping_country'] == $this->country && $_SESSION[$dispatcher['store']]['shipping_state'] == $this->state)) ? 1 : 0;
        $billingCountry = ($_SESSION[$dispatcher['store']]['billing_country'] == "US" || $_SESSION[$dispatcher['store']]['billing_country'] == "AU" || $_SESSION[$dispatcher['store']]['billing_country'] == "CA"
                            || $_SESSION[$dispatcher['store']]['shipping_country'] == "US" || $_SESSION[$dispatcher['store']]['shipping_country'] == "AU" || $_SESSION[$dispatcher['store']]['shipping_country'] == "CA") ? 1 : 0;

        $this->isOtherCountryStateTax = (($includeStateTaxCountry && $otherstatetax) && $matchBillingCountry && !$matchCountryAndState ) ? 1 : 0;
        if ($billingCountry) {
            if (($otherstatetax > 0 && $includeStateTaxCountry) || ($matchCountryAndState)) {
                $carttotals = $this->calculateCartTotals($shippingrate_id, 1);
            } else {
                $carttotals = $this->calculateCartTotals($shippingrate_id, 0);
            }
        } else {
            if ($matchBillingCountry) {
                $carttotals = $this->calculateCartTotals($shippingrate_id, 1);
            } else {
                $carttotals = $this->calculateCartTotals($shippingrate_id, 0);
            }
        }

        return $carttotals;
    }

}
<?php
global $SECURE_URL,$dispatcher,$settings;
$SECURE_URL=true;


if (!empty($_SERVER['HTTPS'])) {
	if ($_SERVER['HTTPS']!='on' && $dispatcher['category']=='checkout' && $dispatcher['product']!='complete') {
		$url = parse_url($_SERVER['REQUEST_URI']);
		$new_secure_url = sprintf("Location:https://%s%s?%s&sess=%s",$settings->secure_url,$url['path'],$url['query'],session_id());
		header($new_secure_url);
		exit();
	}
}

if (!empty($_SERVER['HTTPS'])) {
	if ($_SERVER['HTTPS'] == 'on' && $dispatcher['category']=='checkout' && $dispatcher['product']!='complete'){
		if ($_GET['sess']){
			session_id($_GET['sess']);
			$url = parse_url($_SERVER['REQUEST_URI']);
			header("Location: ".$url['path']);
			exit();
		}

	}
}

if (!empty($_SERVER['HTTPS'])) {
	if ($_SERVER['HTTPS'] != 'on' && $dispatcher['product']=='complete') {

		if ($_GET['sess']){
			session_id($_GET['sess']);
			$url = parse_url($_SERVER['REQUEST_URI']);
			header("Location: ".$url['path']);
			exit();
		}

	}
}
if (!empty($_SERVER['HTTPS'])) {
	if ($_SERVER['HTTPS'] == 'on' && $dispatcher['product']=='complete'){
		$url = parse_url($_SERVER['REQUEST_URI']);
		header(sprintf("Location:http://%s%s?%s&sess=%s",$settings->nonsecure_url,$url['path'],$url['query'],session_id()));
		exit();
	}
}
